#ifndef EXAM_H
#define EXAM_H

#include <QtWidgets/QMainWindow>
#include "ui_exam.h"
#include "Controller.h"
#include "observer.h"

class exam : public QMainWindow,public observer
{
	Q_OBJECT

public:
	exam(Controller &c,Session &s,Programmer p,QWidget *parent = 0);
	~exam();
	void update() override;

private:
	Session &session;
	Programmer programmer;
	Controller &ctrl;
	Ui::examClass ui;

private slots:
	void set();
	void add();
	void remove();
	void revise();
};

#endif // EXAM_H
