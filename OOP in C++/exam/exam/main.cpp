#include "exam.h"
#include <QtWidgets/QApplication>
#include "Controller.h"
#include "observer.h"
#include <assert.h>
#include <iostream>

void test_repository();

int main(int argc, char *argv[])
{
	test_repository();
	QApplication a(argc, argv);
	Session s;
	Repository r{"programmers.txt","files.txt"};
	Controller ctrl{ r };
	std::vector<Programmer> programmers = ctrl.getProgrammers();
	exam* window;
	for (auto p : programmers) {
		window = new exam{ ctrl,s,p };
		window->show();

	}
	return a.exec();
}

void test_repository() {
	Programmer p{"Adi",5};
	Repository r{ "programtest.txt","filetest.txt" };
	assert(r.getFiles().size() == 6);
	assert(r.getProgrammers().size() == 3);
	SourceFile s{ "prog3",0,"creator1","" };
	r.addFile(s);
	assert(r.getFiles().size() == 7);
	assert(r.getFiles()[6] == s);
	r.removeFile(s.getName());
	assert(r.getFiles().size() == 6);
	assert(r.getFiles()[5] != s);
	r.addFile(s);
	r.revise(s.getName(), p);
	assert(r.getFiles()[6].getReviewer() == "Adi");
	assert(r.getFiles()[6].getStatus() == 1);
	assert(p.getRevised() == 6);
	r.removeFile("prog3");
}
