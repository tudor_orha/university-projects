#pragma once
#include "Repository.h"

class Controller {
private:
	Repository repo;
public:
	Controller(Repository r) : repo(r) {}
	~Controller() {}
	void addProgrammer(std::string name, int revised) { repo.addProgrammer(Programmer(name, revised)); }
	void addFile(std::string name, bool status, std::string creator, std::string reviewer) { repo.addFile(SourceFile{ name,status,creator,reviewer }); }
	void removeFile(std::string name) { repo.removeFile(name); }
	void revise(std::string name,Programmer &p) { repo.revise(name, p); }
	std::vector<Programmer> getProgrammers() { return repo.getProgrammers(); }
	std::vector<SourceFile> getFiles() { return repo.getFiles(); }
	bool uniqueFileName(std::string name){
		for (auto f : repo.getFiles()) if (f.getName() == name) return 0;
		return 1;
	}
	std::string getCreatorForFile(std::string name) {
		for (auto f : repo.getFiles()) if (f.getName() == name) return f.getCreator();
		return "";
	}
	void sortControllerByFileName() {
		std::vector<SourceFile> s = repo.getFiles();
		for (int i = 0; i < s.size() - 1; i++)
			for (int j = i+1; j < s.size();j++)
				if (s[i].getName() > s[j].getName()) {
					SourceFile aux = s[i];
					s[i] = s[j];
					s[j] = aux;
				}
		repo.setFiles(s);
	}

};