#include "exam.h"
#include <qmessagebox.h>

exam::exam(Controller &c,Session &s,Programmer p,QWidget *parent)
	: QMainWindow(parent),ctrl(c),session(s),programmer(p)
{ 
	ctrl.sortControllerByFileName();
	ui.setupUi(this);
	connect(ui.addButton,SIGNAL(clicked()),this,SLOT(add()));
	connect(ui.removeButton, SIGNAL(clicked()), this, SLOT(remove()));
	connect(ui.reviseButton, SIGNAL(clicked()), this, SLOT(revise()));
	connect(ui.listWidget, SIGNAL(itemSelectionChanged()), this, SLOT(set()));
	this->setWindowTitle(QString::fromStdString(p.getName()));
	this->ui.label_2->setText(QString::fromStdString(std::to_string(p.getRevised())));
	session.registerObserver(this);
	update();
}

exam::~exam()
{
	session.outObserver(this);
}

void exam::update()
{
	if (ui.listWidget->count() > 0) ui.listWidget->clear();
	for (auto f : ctrl.getFiles()) {
		QString item = QString::fromStdString(f.toString());
		ui.listWidget->addItem(item);
	}
}

void exam::add()
{
	std::string fileName = ui.lineEdit->text().toStdString();
	if ( fileName == "") {
		QMessageBox x;
		x.critical(0, "Error", "Given name is empty!");
		return;
	}
	else if (ctrl.uniqueFileName(fileName) == 0) {
		QMessageBox x;
		x.critical(0, "Error", "Program name already exists!");
		return;
	}
	else {
		ctrl.addFile(fileName, 0, programmer.getName(), "");
		session.notice();
	}
}

void exam::remove()
{
	std::string fileName = ui.lineEdit->text().toStdString();
	ctrl.removeFile(fileName);
	session.notice();
}

void exam::revise()
{
	std::string fileName = ui.lineEdit->text().toStdString();
	ctrl.revise(fileName, programmer);
	this->ui.label_2->setText(QString::fromStdString(std::to_string(programmer.getRevised())));
	session.notice();
}

void exam::set() {
	if (ui.listWidget->count() == 0) return;
	QModelIndexList index = this->ui.listWidget->selectionModel()->selectedIndexes();
	if (index.size() == 0) return;
	int i = index.at(0).row();
	SourceFile f = ctrl.getFiles()[i];
	this->ui.lineEdit->setText(QString::fromStdString(f.getName()));
	if (f.getStatus() == 0 && ctrl.getCreatorForFile(ui.lineEdit->text().toStdString())!=programmer.getName()) this->ui.reviseButton->setFlat(0);
	else this->ui.reviseButton->setFlat(1);
}
