#pragma once
#include "Domain.h"
#include <vector>

class Repository {
private:
	std::vector<Programmer> programmers;
	std::vector<SourceFile> sourceFiles;
	std::string programmersFile;
	std::string sourceFile;
public:
	Repository(std::string prog, std::string source) : programmersFile(prog), sourceFile(source) { loadFromFiles(); }
	~Repository() {}
	void addProgrammer(Programmer p) {
		programmers.push_back(p);
		storeToFiles();
	}
	void removeFile(std::string name);
	void addFile(SourceFile s);
	void revise(std::string name, Programmer &p);
	void loadFromFiles() {
		std::ifstream pf(programmersFile);
		std::ifstream sf(sourceFile);
		Programmer p; SourceFile s;
		while (pf >> p) programmers.push_back(p);
		while (sf >> s) sourceFiles.push_back(s);
	}
	void storeToFiles() {
		std::ofstream pf(programmersFile);
		std::ofstream sf(sourceFile);
		for (auto p : programmers) pf << p.getName() + "," + std::to_string(p.getRevised()) + "\n";
		for (auto s : sourceFiles) sf << s.getName() + "," + std::to_string(s.getStatus()) + "," + s.getCreator() + "," + s.getReviewer() + "\n";
	}
	std::vector<Programmer> getProgrammers() { return programmers; }
	std::vector<SourceFile> getFiles() { return sourceFiles; }
	void setFiles(std::vector<SourceFile> SF) { sourceFiles = SF; }
};