#pragma once
#include <string>
#include <fstream>

class Programmer{
private:
	std::string name;
	int nr_revised;
public:
	Programmer() :name(""), nr_revised(0) {}
	Programmer(std::string name,int nr_rev) :name(name),nr_revised(nr_rev){}
	~Programmer() {}
	std::string getName() { return name; }
	int getRevised() { return nr_revised; }
	void growRevised() { nr_revised++; }
	friend std::istream & operator >>(std::istream& is, Programmer &p) {
		std::string part[2];
		getline(is, part[0], ',');
		getline(is, part[1]);
		p = Programmer(part[0], atoi(part[1].c_str()));
		return is;
	}
	std::string toString() {
		return name + " " + std::to_string(nr_revised);
	}
};

class SourceFile {
private:
	bool status;
	std::string name,creator,reviewer;
public:
	SourceFile() :  name(""), status(0), creator(""), reviewer("") {}
	SourceFile(std::string name, bool status, std::string creator, std::string reviewer) :name(name), status(status), creator(creator), reviewer(reviewer) {}
	~SourceFile() {}
	bool operator==(SourceFile s) {
		return (s.status == status && s.name == name && s.creator == creator && s.reviewer == reviewer);
	}
	bool operator!=(SourceFile s) {
		return 1-(s == *this);
	}
	std::string getName() { return name; }
	bool getStatus() { return status; }
	std::string getCreator() { return creator; }
	std::string getReviewer() { return reviewer; }
	void setReviewer(std::string name) { reviewer = name; }
	void setStatus() { status = 1; }
	friend std::istream & operator >>(std::istream& is, SourceFile &s) {
		std::string part[4];
		getline(is, part[0], ',');
		getline(is, part[1], ',');
		getline(is, part[2], ',');
		getline(is, part[3]);
		s = SourceFile(part[0], atoi(part[1].c_str()), part[2], part[3]);
		return is;
	}
	std::string toString() {
		return name + " " + std::to_string(status) + " " + creator + " " + reviewer;
	}
};