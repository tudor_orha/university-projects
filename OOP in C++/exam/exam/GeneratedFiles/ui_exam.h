/********************************************************************************
** Form generated from reading UI file 'exam.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXAM_H
#define UI_EXAM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_examClass
{
public:
    QWidget *centralWidget;
    QListWidget *listWidget;
    QPushButton *addButton;
    QPushButton *removeButton;
    QPushButton *reviseButton;
    QLineEdit *lineEdit;
    QLabel *label;
    QLabel *label_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *examClass)
    {
        if (examClass->objectName().isEmpty())
            examClass->setObjectName(QStringLiteral("examClass"));
        examClass->resize(600, 400);
        centralWidget = new QWidget(examClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(20, 0, 291, 321));
        addButton = new QPushButton(centralWidget);
        addButton->setObjectName(QStringLiteral("addButton"));
        addButton->setGeometry(QRect(330, 40, 75, 23));
        removeButton = new QPushButton(centralWidget);
        removeButton->setObjectName(QStringLiteral("removeButton"));
        removeButton->setGeometry(QRect(410, 40, 75, 23));
        reviseButton = new QPushButton(centralWidget);
        reviseButton->setObjectName(QStringLiteral("reviseButton"));
        reviseButton->setGeometry(QRect(490, 40, 75, 23));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(330, 10, 113, 20));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(330, 110, 111, 16));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(430, 110, 47, 13));
        examClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(examClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        examClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(examClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        examClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(examClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        examClass->setStatusBar(statusBar);

        retranslateUi(examClass);

        QMetaObject::connectSlotsByName(examClass);
    } // setupUi

    void retranslateUi(QMainWindow *examClass)
    {
        examClass->setWindowTitle(QApplication::translate("examClass", "exam", 0));
        addButton->setText(QApplication::translate("examClass", "Add", 0));
        removeButton->setText(QApplication::translate("examClass", "Remove", 0));
        reviseButton->setText(QApplication::translate("examClass", "Review", 0));
        label->setText(QApplication::translate("examClass", "Nr. of revised files:", 0));
        label_2->setText(QApplication::translate("examClass", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class examClass: public Ui_examClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXAM_H
