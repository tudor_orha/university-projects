#pragma once
#include <vector>

class observer {
public:
	virtual void update() = 0;
	~observer() {}
};

class Session {
private:
	std::vector<observer*> observers;
public:
	~Session() {}
	void registerObserver(observer* obs) { observers.push_back(obs); }
	void outObserver(observer* obs) { observers.erase(std::find(observers.begin(), observers.end(), obs)); }
	void notice() {
		for (auto obs : observers) {
			obs->update();
		}
	}

};