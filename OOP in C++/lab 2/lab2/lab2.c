#include <stdio.h>
#include <assert.h>

/*
Problem statement:
5.a. Print the exponent of a prime number p from the decomposition in prime factors of a given
	 number n (n is a non-null natural number).
  b. Given a vector of numbers, find the longest contiguous subsequence such that any two
	 consecutive elements are relatively prime.
*/

// A data structure that will contain an array, and the length of the array.
typedef struct{
	int elements[1000];
	int length;
}vector;

/*
Input : Two vectors.
Output: Returns 0 if the vectors are not equal, 1 otherwise.
*/
int compare_vectors(vector a, vector b) {
	if (a.length == b.length)
	{
		int equal = 1;
		for (int i = 0; i < a.length; i++)
		{
			if (a.elements[i] != b.elements[i])
			{
				equal = 0;
			}
		}
		return equal;
	}
	else return 0;
}

/*
Input : A positive integer, and a prime number.
Output: Exponent of the prime number from the decomposition in prime factors of the positive integer.
*/
int exponent(int number,int prime)
{
	int exp = 0;
	while (number % prime == 0)
	{
		number = number / prime;
		exp++;
	}
	return exp;
}


// Tests the exponent function.
void test_exponent() {
	assert(exponent(1, 2) == 0);
	assert(exponent(169, 13) == 2);
	assert(exponent(5000, 5) == 4);
}

/*
Input : Two positive integers.
Output: The greatest commond divisor of those 2 integers.
*/
int gcd(int first_number, int second_number)
{
	while (second_number != 0)
	{
		int modulo = first_number % second_number;
		first_number = second_number;
		second_number = modulo;
	}
	return first_number;
}


// Tests the gcd function.
void test_gcd()
{
	assert(gcd(0, 5) == 5);
	assert(gcd(48, 60) == 12);
}

/*
Input : A vector.
Output: A vector that represents the longest contiguous subsequence such that any two consecutive elements are relatively prime
	    If there are 2 subsequences that have the same max_length, the first one will be returned.
*/
vector sequence(vector v)
{
	int len = 1, len_max = 1, position_of_the_last_element_from_sequence = 0;
	vector sequence;
	for (int i = 1; i < v.length; i++)
	{
		if (gcd( v.elements[i - 1], v.elements[i]) == 1)
		{
			len++;
		}
		else
		{
			if (len_max < len)
			{
				len_max = len;
				position_of_the_last_element_from_sequence = i - 1;
			}
			len = 1;
		}
	}
	if (len_max < len)
	{
		len_max = len;
		position_of_the_last_element_from_sequence = v.length-1;
	}
	sequence.length = len_max;
	for (int i = len_max-1; i >= 0; i--)
	{
		sequence.elements[i] = v.elements[position_of_the_last_element_from_sequence--];
	}

	return sequence;
}

// Tests the sequence function
void test_sequence()
{
	vector x = { {1,2,3,4,5 }, 5 };
	vector y = { {1,3,6,7,8},5 };
	vector seq_y = { {6,7,8},3 };
	vector z = { {5,10,7,8,4},5 };
	vector seq_z = { {10,7,8},3 };
	assert(compare_vectors(sequence(x), x));
	assert(compare_vectors(sequence(y), seq_y));
	assert(compare_vectors(sequence(z), seq_z));
}


// Reads the length of an array, and the elements of the array, and returns a vector.
vector read_vector()
{
	vector v;
	printf("vector length=");
	scanf("%d", &v.length);
	for (int i = 0; i < v.length; i++)
	{
		printf("v[%d]=", i);
		scanf("%d", &v.elements[i]);
	}
	return v;
}

// Prints a vector.
void print_vector(vector v)
{
	printf("[");
	for (int i = 0; i < v.length - 1; i++)
	{
		printf("%d, ", v.elements[i]);
	}
	printf("%d", v.elements[v.length - 1]);
	printf("]\n");
}

// Prints the Menu options.
void print_menu()
{
	printf("a. Print the exponent of a prime number p from the decomposition in prime factors of a given number n.\n\n");
	printf("b. Print the longest contiguous subsequence of a vector such that any two consecutive elements are relatively prime.\n\n");
	printf("c. Exit\n");

}

// Executes the a option from the menu.
void case_a()
{
	int n, p;
	printf("n=");
	scanf("%d", &n);
	printf("p=");
	scanf("%d", &p);
	printf("Exponent: %d\n\n",exponent(n, p));
}

// Executes the b option from the menu.
void case_b()
{
	vector v,u;
	v = read_vector();
	u = sequence(v);
	print_vector(u);
	printf("\n");

}

// Runs all the tests.
void tests()
{
	test_exponent();
	test_gcd();
	test_sequence();
}

int main()
{
	//system("cls"); //clears the console
	tests();
	char option[255];
	do
	{
		print_menu();
		printf("Insert option here:");
		scanf("%s",option);
		if (option[1] == NULL) {
			switch (option[0])
			{
			case 'A':
			case 'a':
				case_a();
				break;
			case 'B':
			case 'b':
				case_b();
				break;
			case 'C':
			case 'c':
				printf("Exiting...\n");
				break;
			default:
				printf("That is not a valid option!\n\n");
			}
		}
		else printf("That is not a valid option!\n\n");

	} while (!(option[1] == NULL && (option[0] == 'C' || option[0] == 'c')));
	return 0;
	
}
