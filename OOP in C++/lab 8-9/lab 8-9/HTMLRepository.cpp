#include "HTMLRepository.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>

using namespace std;

void HTMLRepository::storeToFile()
{
	ifstream in(this->fileName);
	string line;
	string file;
	int i = 0;
	while (getline(in,line,'\n')) {
		file = file + line + "\n";
		if (line == "        </tr>") break;
	}
	ofstream out(this->fileName);
	out << file;
	for (Dog d : this->dogs) {
		out << "\t\t<tr>\n";
		out << "\t\t\t<td>" << d.getBreed() << "</td>\n";
		out << "\t\t\t<td>" << d.getName() << "</td>\n";
		out << "\t\t\t<td>" << d.getAge() << "</td>\n";
		out << "\t\t\t<td><a href=" << d.getPhoto() << ">Link</a></td>\n";
		out << "\t\t</tr>\n";
	}
	out << "\t</table>\n";
	out << "</body>\n";
	out << "</html>\n";
}
