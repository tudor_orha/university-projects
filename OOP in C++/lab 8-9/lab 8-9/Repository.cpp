#include "Repository.h"
#include <algorithm>
#include <fstream>
#include <iostream>

using namespace std;

Repository::Repository(string fileName) {
	this->fileName = fileName;
}

Repository::Repository(const Repository& repo)
{
	this->dogs = repo.dogs;
	this->fileName = repo.fileName;
}

Repository Repository::operator=(const Repository& repo)
{
	this->dogs = repo.dogs;
	this->fileName = repo.fileName;
	return *this;
}

void Repository::addDog(Dog d)
{
	this->dogs.push_back(d);
	storeToFile();
}

void Repository::removeDog(Dog d)
{
	this->dogs.erase(std::remove(this->dogs.begin(),this->dogs.end(),d),this->dogs.end());
	storeToFile();
}

void Repository::readFromFile()
{
	Dog d;
	ifstream file(this->fileName);
	if (!file.is_open())
	{
		cout << "Can't open file " << this->fileName << "!\n";
		return;
	}
	while (file >> d) this->dogs.push_back(d);

}

void Repository::storeToFile()
{
	ofstream file(this->fileName);
	for ( Dog dog : this->dogs)
	{
		file << dog;
	}
	file.close();
}

