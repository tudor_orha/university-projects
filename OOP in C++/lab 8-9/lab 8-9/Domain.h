#pragma once
#include <string>

class Dog {
private:
	std::string breed;
	std::string name;
	int age;
	std::string photo;
public:
	//Dog() { this->breed = ""; this->name = ""; this->age = 0; this->photo = ""; }
	Dog() : breed("") , name("") , age(0) , photo("") {}
	Dog(std::string breed, std::string name, int age, std::string photo) : breed(breed), name(name), age(age), photo(photo) {}
	bool operator==(Dog d);
	std::string getBreed();
	std::string getName();
	int getAge();
	std::string getPhoto();
	friend std::istream& operator>>(std::istream& is, Dog& d);
	friend std::ostream& operator<<(std::ostream& os, Dog d);
};