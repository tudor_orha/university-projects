#pragma once
#include "Domain.h"
#include <vector>

class Repository
{
protected:
	std::vector<Dog> dogs;
	std::string fileName;
	unsigned int current;
public:
	Repository(std::string fileName);
	Repository(const Repository &repo);
	Repository operator=(const Repository &repo);
	// adds dog d to the last position
	void addDog(Dog d);
	// removes all dogs d from the repository;
	void removeDog(Dog d);
	std::vector<Dog> getDogs() { return dogs; }
	unsigned long getSize() { return dogs.size(); }
	std::string getFileName() { return fileName; }
	unsigned int getCurrent() { return current; }
	void startDog() { current = 0; }
	void nextDog() { current++; if (current == dogs.size()) current = 0; }
	void readFromFile();
	virtual void storeToFile();
};
