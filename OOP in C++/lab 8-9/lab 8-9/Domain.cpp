#include "Domain.h"
#include <stdlib.h>

using namespace std;
/*
Dog::Dog(string breed, string name, int age, string photo)
{
	this->breed = breed;
	this->name = name;
	this->age = age;
	this->photo = photo;
}*/

bool Dog::operator==(Dog d)
{
	if (this->breed == d.getBreed() && this->name == d.getName()) return 1;
	return 0;
}

string Dog::getName()
{
	return this->name;
}

string Dog::getBreed()
{
	return this->breed;
}

int Dog::getAge()
{
	return this->age;
}

string Dog::getPhoto()
{
	return this->photo;
}

std::istream & operator>>(std::istream &  is, Dog &   d)
{
	string line;
	string partitions[4];
	getline(is, line, ','); partitions[0] = line;
	getline(is, line, ','); partitions[1] = line;
	getline(is, line, ','); partitions[2] = line;
	getline(is, line); partitions[3] = line;
	d = Dog(partitions[0], partitions[1], atoi(partitions[2].c_str()), partitions[3]);
	return is;
}

std::ostream & operator<<(std::ostream & os, Dog d)
{
	os << d.getBreed() << "," << d.getName() << "," << d.getAge() << "," << d.getPhoto() << endl;
	return os;
}
