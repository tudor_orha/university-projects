#pragma once
#include <string>
#include "controller.h"

using namespace std;

class UI
{
private:
	Controller control;

public:
	UI(const Controller& c) : control(c) {}
	void run();

private:
	void printAdminMenu();
	void printUserMenu();
	void adminMode();
	void userMode();
	void add(string use);
	void remove(string use);
	void update();
	void printController();
	int readPositiveInteger();
	void printDog(int position,Dog* dogs);
	string readString(string str);
	int stringIsPositiveInteger(string str);
};