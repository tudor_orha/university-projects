#pragma once
#include <string>

using namespace std;

class Dog {
private:
	string breed;
	string name;
	int age;
	string photo;
public:
	Dog() { this->breed = ""; this->name = ""; this->age = 0; this->photo = ""; }
	Dog(string breed, string name, int age, string photo);
	bool operator==(Dog d);
	string getBreed();
	string getName();
	int getAge();
	string getPhoto();

};