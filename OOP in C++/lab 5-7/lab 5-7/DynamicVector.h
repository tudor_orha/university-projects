#pragma once
//#include "dog.h"

template<typename Dog>
class DynamicVector
{
private:
	Dog *dogs;
	unsigned long size;
	unsigned long capacity;

public:
	DynamicVector(unsigned long capacity = 10);
	DynamicVector(const DynamicVector &v);
	~DynamicVector();
	DynamicVector& operator=(const DynamicVector& v);
	DynamicVector operator+(Dog d);
	DynamicVector operator-(Dog d);
	friend DynamicVector operator+(Dog d, DynamicVector v) {v.add(d); return v;};
	void add(Dog d);
	void remove(Dog d);
	Dog* getAllDogs();
	unsigned long getSize();

private:
	void resize(unsigned long factor = 2);
};


template<typename Dog>
DynamicVector<Dog>::DynamicVector(unsigned long capacity)
{
	this->size = 0;
	this->capacity = capacity;
	this->dogs = new Dog[capacity];
}

template<typename Dog>
DynamicVector<Dog>::DynamicVector(const DynamicVector& v)
{
	this->size = v.size;
	this->capacity = v.capacity;
	this->dogs = new Dog[this->capacity];
	for (unsigned int i = 0; i < this->size; i++)
		this->dogs[i] = v.dogs[i];
}

template<typename Dog>
DynamicVector<Dog>::~DynamicVector()
{
	delete[] this->dogs;
}

template<typename Dog>
DynamicVector<Dog>& DynamicVector<Dog>::operator=(const DynamicVector& v)
{
	if (this == &v)
		return *this;

	this->size = v.size;
	this->capacity = v.capacity;

	delete[] this->dogs;
	this->dogs = new Dog[this->capacity];
	for (unsigned int i = 0; i < this->size; i++)
		this->dogs[i] = v.dogs[i];

	return *this;
}

template<typename Dog>
DynamicVector<Dog> DynamicVector<Dog>::operator+(Dog d)
{
	this->add(d);
	return *this;
}

template<typename Dog>
DynamicVector<Dog> DynamicVector<Dog>::operator-(Dog d)
{
	this->remove(d);
	return *this;
}

template<typename Dog>
void DynamicVector<Dog>::add(Dog d)
{
	if (this->size == this->capacity)
		this->resize();
	this->dogs[this->size] = d;
	this->size++;
}

template<typename Dog>
void DynamicVector<Dog>::remove(Dog d)
{
	int ok = 1, i = 0;
	while (ok && i < this->size)
	{
		if (d == this->dogs[i])
		{
			ok = 0;
			for (int j = i; j < this->size - 1; j++)
			{
				this->dogs[j] = this->dogs[j + 1];
			}
			this->size--;
		}
		i++;
	}

}

template<typename Dog>
void DynamicVector<Dog>::resize(unsigned long factor)
{
	this->capacity *= factor;
	Dog *els = new Dog[this->capacity];
	for (unsigned int i = 0; i < this->size; i++)
		this->dogs[i] = els[i];
	delete[] this->dogs;
	this->dogs = els; //check this line
}

template<typename Dog>
Dog* DynamicVector<Dog>::getAllDogs()
{
	return this->dogs;
}

template<typename Dog>
unsigned long DynamicVector<Dog>::getSize()
{
	return this->size;
}

