#include "repository.h"
#include "ui.h"
#include <iostream>

using namespace std;

template<typename T>
class List {
public:
	struct Node {
		Node* next;
		Node* prev;
		T info;
	};
	class Iterator {
	private:
		Node* current;
	public:
		explicit Iterator(Node* c) {
			current = c;
		}
		T& operator*() const {
			return current->info;
		}
		T* operator->() const {
			return &current->info;
		}
		void operator++() {
			current = current->next;
		}
		void operator--() {
			current = current->prev;
		}
		void operator+=(int n) {
			while (n--) current = current->next;
		}
		bool hasNext() const {
			return current->next != nullptr;
		}
	};
private:
	Node* first;
	Node** pLast;

public:
	List() {
		first = nullptr;
		pLast = &first;
	}
	bool isEmpty() const {
		return first == nullptr;
	}
	void add(T val) {
		*pLast = new Node;
		(*pLast)->next = nullptr;
		(*plast)->prev = nullptr;
		(*pLast)->info = val;
		pLast = &(*pLast)->next;
	}

	Iterator iterator() {
		return Iterator(first);
	}
};


int main()
{	/*
	List<int> list;
	list.add(10);
	list.add(12);
	list.add(42);

	if (!list.isEmpty()) {
		List<int>::Iterator it = list.iterator();
		while (true) {
			*it = *it * 10;
			if (!it.hasNext()) break;
			it += 2;
			it--;
		}
	}

	printf("Begin list\n");
	if (!list.isEmpty()) {
		List<int>::Iterator it = list.iterator();
		while (true) {
			printf("  %d\n", *it);
			if (!it.hasNext()) break;
			++it;
		}
	}
	printf("End list\n");
	*/
	Repository repo("Dogs.txt");
	Controller control(repo);
	UI ui(control);
	ui.run();
}