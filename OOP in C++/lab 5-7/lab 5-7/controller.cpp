#include "controller.h"
#include "dog.h"

void Controller::addDog(string Breed, string Name, int Age, string Photo)
{
	Dog dog(Breed, Name, Age, Photo);
	this->repo.addDog(dog);
}

void Controller::removeDog(string Breed, string Name)
{
	Dog dog(Breed, Name, 0, "");
	this->repo.removeDog(dog);
}

bool Controller::findBreed(string Breed)
{
	Dog* dogs = this->repo.getAllDogs();
	for (unsigned int i = 0; i < this->repo.getSize(); i++)
	{
		if (dogs[i].getBreed() == Breed) return 1;
	}
	return 0;
}

bool Controller::findBreedAndName(string Breed, string Name) {
	Dog* dogs = this->repo.getAllDogs();
	for (unsigned int i = 0; i < this->repo.getSize(); i++)
	{
		if (dogs[i].getBreed() == Breed && dogs[i].getName() == Name) return 1;
	}
	return 0;
}

DynamicVector<Dog> Controller::filterBreedAndAge(string Breed, int Age) {
	DynamicVector<Dog> dogs(3);
	Dog* allDogs = this->repo.getAllDogs();	
	for (unsigned int i = 0; i < this->repo.getSize(); i++)
	{
		if ((Breed == "*" || allDogs[i].getBreed() == Breed) && allDogs[i].getAge() < Age) dogs.add(allDogs[i]);
	}
	return dogs;
}