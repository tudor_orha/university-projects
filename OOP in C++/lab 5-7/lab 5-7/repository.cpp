#include "repository.h"
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

Repository::Repository(string fileName) {
	this->fileName = fileName;
	this->dogs = DynamicVector<Dog>();
	this->readFromFile();
}

Repository::Repository(const Repository& repo)
{
	this->dogs = DynamicVector<Dog>(repo.dogs);
	this->fileName = repo.fileName;
}

Repository Repository::operator=(const Repository& repo)
{
	this->dogs = repo.dogs;
	this->fileName = repo.fileName;
	return *this;
}

void Repository::addDog(Dog d)
{
	this->dogs.add(d);
	storeToFile();
}

void Repository::removeDog(Dog d)
{
	this->dogs.remove(d);
	storeToFile();
}

void Repository::readFromFile()
{
	ifstream file(this->fileName);
	if (!file.is_open())
	{
		cout << "Can't open file!\n"; return;
	}
	string line;
	while (getline(file,line,'|'))
	{
		string partitions[4];
		partitions[0] = line;
		getline(file, line, '|'); partitions[1] = line;
		getline(file, line, '|'); partitions[2] = line;
		getline(file, line); partitions[3] = line;
		Dog x = Dog(partitions[0], partitions[1], stringToInt(partitions[2]), partitions[3]);
		this->dogs.add(x);
	}

}

void Repository::storeToFile()
{
	ofstream file(this->fileName);
	Dog* dogs = this->dogs.getAllDogs();
	for (unsigned int i = 0; i < this->dogs.getSize(); i++)
	{
		file << dogs[i].getBreed() << '|' << dogs[i].getName() << '|' << dogs[i].getAge() << '|' << dogs[i].getPhoto() << "\n";
	}
	file.close();
}

int Repository::stringToInt(string str)
{
	int number = 0;
	for (unsigned int i = 0; i < str.length(); i++) number = number * 10 + str[i] - '0';
	return number;
}