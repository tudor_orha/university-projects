#pragma once
#include "repository.h"
#include <string>


class Controller
{
private:
	Repository repo;
public:
	// Controller class constructor. Input: a Repository type object
	Controller(const Repository& r) : repo(r) {}
	// Adds a Dog object to the controller. Input: Dog attributes
	void addDog(string Breed,string Name,int Age,string Photo);
	// Removes a dog object from the controller. Input: Dog breed and name
	void removeDog(string Breed, string Name);
	// Searches for dogs by a given breed.
	// Input: Breed
	// Output: 1 if a dog is found, 0 otherwise
	bool findBreed(string Breed);
	/*
	Searches for dogs by a given breed and name.
	Input: Breed and Name strings
	Output: 1 if a dog is found with the given Input, 0 otherwise
	*/
	bool findBreedAndName(string Breed, string Name);
	/*
	Input: string Breed, int Age
	Output: A Dynamic vector containing all dogs with the given breed and an age smaller than the one from the input.	
	*/
	DynamicVector<Dog> filterBreedAndAge(string Breed, int Age);
	// Returns all the dog data from the controller
	Dog* getDogs() { return repo.getAllDogs(); }
	// Returns the number of dogs from the controller
	unsigned long getSize() { return repo.getSize(); }
};