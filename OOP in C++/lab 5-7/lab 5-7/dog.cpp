#include "dog.h"

Dog::Dog(string breed, string name, int age, string photo)
{
	this->breed = breed;
	this->name = name;
	this->age = age;
	this->photo = photo;
}

bool Dog::operator==(Dog d)
{
	if (this->breed == d.getBreed() && this->name == d.getName()) return 1;
	return 0;
}

string Dog::getName()
{
	return this->name;
}

string Dog::getBreed()
{
	return this->breed;
}

int Dog::getAge()
{
	return this->age;
}

string Dog::getPhoto()
{
	return this->photo;
}