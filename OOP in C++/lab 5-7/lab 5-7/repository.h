#pragma once
#include <string>
#include "DynamicVector.h"
#include "dog.h"

using namespace std;

class Repository
{
private:
	DynamicVector<Dog> dogs;
	string fileName;
public:
	Repository(string fileName);
	Repository(const Repository& repo);
	Repository operator=(const Repository &repo);
	void addDog(Dog d);
	void removeDog(Dog d);
	Dog* getAllDogs() { return dogs.getAllDogs(); }
	unsigned long getSize() { return dogs.getSize(); }
	string getFileName() { return fileName; }
private:
	void readFromFile();
	void storeToFile();
	int stringToInt(string str);
};