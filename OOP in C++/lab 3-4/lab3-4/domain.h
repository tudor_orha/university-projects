#pragma once

typedef struct {
	char name[255],category[255];
	int quantity, expiration_day, expiration_month, expiration_year;
}food;

char* getFoodName(food);
food createFood(char*,char*,int,int,int,int);
int compareFood(food, food);
//char* getFoodCategory(food);