#pragma once
#include "domain.h"


typedef struct
{
	food* foods;
	int length;
	int capacity;
}dynamicArray;

dynamicArray* createArray(int capacity);
void destroyArray(dynamicArray *d);
void copyArray(dynamicArray* old, dynamicArray* new);
void resize(dynamicArray *d);
void addToArray(dynamicArray *d, food f);
void deleteFromArray(dynamicArray *d, food f);
void updateArray(dynamicArray *d, food old, food new);
void test_dynamicArray();