#include "dynamicArray.h"
#include "domain.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

dynamicArray* createArray(int capacity)
{
	dynamicArray* d = malloc(sizeof(dynamicArray));
	d->capacity = capacity;
	d->length = 0;
	d->foods = malloc(sizeof(food)*capacity);
	return d;
}

void destroyArray(dynamicArray *d)
{
	free(d->foods);
	free(d);
}

void copyArray(dynamicArray* old, dynamicArray* new)
{
	new->capacity = old->capacity;
	new->length = old->length;
	new->foods = realloc(new->foods, sizeof(food) * new->capacity);
	for (int i = 0; i < new->length; i++) new->foods[i] = old->foods[i];
}

void resize(dynamicArray *d)
{
	d->capacity *= 2;
	d->foods = realloc(d->foods, sizeof(food) * d->capacity);
}

void addToArray(dynamicArray *d, food f)
{
	if (d->length == d->capacity) resize(d);
	int ok = 1;
	for (int i = 0; i < d->length; i++)
	{
		if (compareFood(f, d->foods[i])) {
			ok = 0;
			d->foods[i].quantity += f.quantity;
		}
	}
	if (ok == 1)
	{
		d->foods[d->length] = f;
		d->length++;
	}
}

void deleteFromArray(dynamicArray *d, food f)
{
	int ok = 1, i = 0;
	while (ok && i < d->length)
	{
		if (compareFood(f, d->foods[i]) == 1)
		{
			ok = 0;
			for (int j = i; j < d->length - 1; j++)
			{
				d->foods[j] = d->foods[j + 1];
			}
			d->length--;
		}
		i++;
	}
}

void updateArray(dynamicArray *d, food old, food new)
{
	deleteFromArray(d, old);
	addToArray(d, new);
}

void test_dynamicArray()
{
	dynamicArray* d = createArray(2), *d2 = createArray(2);
	food f1 = createFood("Milk", "dairy", 15, 23, 3, 2016);
	food f2 = createFood("Chicken", "meat", 12, 20, 3, 2016);
	food f3 = createFood("Skittles", "sweets", 5, 30, 7, 2018);
	addToArray(d, f1);
	addToArray(d, f2);
	assert(d->capacity == 2);
	addToArray(d, f3);
	assert(d->capacity == 4);
	addToArray(d2, f1);
	addToArray(d2, f3);
	deleteFromArray(d, f2);
	for (int i = 0; i < 2; i++)
	{
		assert(compareFood(d->foods[i], d2->foods[i]) == 1);
	}

}