#include "domain.h"
#include "repository.h"
#include <string.h>
#include <stdlib.h>

// Creates an empty repository
repository* createRepository() {
	repository* foodRepo = malloc(sizeof(repository));
	foodRepo->d = createArray(2);
	return foodRepo;
}

void destroyRepository(repository* r)
{
	if (r == NULL) return;
	destroyArray(r->d);
	free(r);
}

void copyRepository(repository* old, repository* new)
{
	copyArray(old->d, new->d);
}

/*
Input : A repository and a food object.
Output: Adds the food object to the repository, and returns the new repository.
*/
void addToRepository(repository* x,food f)
{
	addToArray(x->d, f);
}

/*
Input : A repository and a food object
Output: Deletes the given food from the repository, and returns the new repository
*/
void deleteFromRepository(repository* x, food f)
{
	deleteFromArray(x->d, f);
}

/*
Input : A repository and 2 food objects
Output: Removes the old food, and adds the new one to the repository. Returns the new repoistory.
*/
void updateRepository(repository* x,food old,food new)
{
	updateArray(x->d, old, new);
}

