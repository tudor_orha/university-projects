#pragma once
#include "dynamicArray.h"
#include "domain.h"

typedef struct {
	//food element[100];
	//int length;
	dynamicArray* d;
}repository;

repository* createRepository();
void destroyRepository(repository* r);
void copyRepository(repository* old, repository* new);
void addToRepository(repository*,food);
void deleteFromRepository(repository*, food);
void updateRepository(repository*, food, food);