#include <stdio.h>
#include "dynamicArray.h"
#include "domain.h"
#include "repository.h"
#include "controller.h"
#include "UI.h"
#include <time.h>
/*
struct _man {
	char name[256];
	int age;
	void(*speak)(char *text);
	void(*eat)(long *foodptr);
	void(*sleep)(int hours);
	//etc
};

void grijesh_speak(char *text)
{
	printf("speaks");
}

void grijesh_eat(long *food)
{
	printf("eats");
}

void grijesh_sleep(int hours)
{
	printf("sleeps");
}

void init_struct(struct _man *man)
{
	if (man == NULL) { man = malloc(sizeof(struct _man)); }
	//strcpy(*man.name, "Grijesh");
	man->age = 25;
	man->speak = grijesh_speak;
	man->eat = grijesh_eat;
	man->sleep = grijesh_sleep;
	//etc
}


//so now in main.. i can tell you to either speak, or eat or sleep.

int main(int argc, char *argv[])
{
	struct _man grijesh;
	init_struct(&grijesh);
	grijesh.speak("Babble Dabble");
	grijesh.sleep(10);
	return 0;
}
*/
/*
The company �Home SmartApps� has decided to design a new intelligent
refrigerator.Besides the hardware, they need a software application to
manage the refrigerator.Each Type of food has a name, a category(may
	be dairy, sweets, meat or fruit), a quantity and an expiration date.
	a.The application must allow adding, deleting and updating a type of
	food.A type of food is uniquely identified by name and category.If a type
	of food that already exists is added, its quantity will be updated(the new
		quantity is added to the existing one).
	b.The application should offer the possibility to display all the types
	of food whose names contain a given string(if the string is empty, all types of food from the
		refrigerator are considered) and they will be shown sorted ascending by their quantities.
	c.The application should be able to display all types of food of a given category(if the category is
		empty, all types of food will be considered) whose expiration dates are close(expire in the
			following given X days).
	d.The application must provide the option to undo and redo the last change.
	*/
int main()
{
	//test_dynamicArray();
	//testController();
	repository repo;
	food a, b, c;
	a = createFood("nmeee", "sweets", 145, 10, 2, 2014);
	c = createFood("name", "dairy", 300000, 1, 2, 2014);
	b = createFood("Secondn", "fruit", 90, 30, 3, 2016);
	repository *r = createRepository();
	controller *co = createController(r);
	add(co, a);
	add(co, b);
	add(co, c);
	run(co);
	destroyController(co);
	destroyRepository(r);
    return 0;
}

