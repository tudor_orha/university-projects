#include <stdio.h>
#include <string.h>
#include "domain.h"

/*
Input: 2 char strings and 4 integers
Output: Returns a food object
*/
food createFood(char* name,char* category,int quantity,int ed,int em,int ey) {
	food f;
	strcpy(f.name, name);
	strcpy(f.category, category);
	f.quantity = quantity;
	f.expiration_day = ed;
	f.expiration_month = em;
	f.expiration_year = ey;
	return f;
}


char* getFoodName(food a)
{
	char b[255];
	int i = 0;
	while (a.name[i] != NULL) b[i] = a.name[i++];
	b[i] = NULL;
	return b;
}

char* getFoodCategory(food a)
{
	char b[255];
	int i = 0;
	while (a.category[i] != NULL) b[i] = a.category[i++];
	b[i] = NULL;
	return b;
}

int getFoodQuantity(food a)
{
	return a.quantity;
}

int compareFood(food a, food b)
{
	if (_stricmp(a.name, b.name) == 0 && _stricmp(a.category, b.category) == 0) return 1;
	return 0;
}
