#pragma once
#include "domain.h"
#include "repository.h"

typedef struct {
	repository* repo;

}controller;

controller* createController(repository*);
void destroyController(controller* c);
void add(controller*, food);
void delete(controller*, food);
void update(controller*, food, food);
void filter(controller* x, controller* filtered, char* string);
void filterByDaysLeft(controller* x, controller* filtered, char* category,int days);
int digits(int);
int findName(controller*, char*);
int findByNameAndCategory(controller*, char*, char*);
void testController();