#include<stdio.h>
#include<string.h>
#include "domain.h"
#include "repository.h"
#include "controller.h"

// Prints the menu
void printMenu()
{
	printf("1. Add food\n");
	printf("2. Delete food\n");
	printf("3. Update food\n");
	printf("4. Print all food\n");
	printf("5. Print all foods that contain a certain string\n");
	printf("6. Print foods that go bad soon!\n");
	printf("7. Undo\n");
	printf("8. Redo\n");
	printf("0. Exit\n");
}

/*
Input :
Output:
*/
void readString(char* name,char* inputMessage)
{
	printf(inputMessage);
	scanf("%s", name);
}

void readCategory(char* category,char* inputMessage,int starCanBreak)
{
	while (1)
	{
		printf(inputMessage);
		scanf("%s", category);
		if (_stricmp(category, "dairy") == 0 || _stricmp(category, "sweets") == 0 ||
			_stricmp(category, "meat") == 0 || _stricmp(category, "fruit") == 0 || (starCanBreak == 1 && _stricmp(category, "*") == 0))
		{
			for (int i = 0; i < strlen(category); i++) {
				category[i] = tolower(category[i]);
			}
			break;
		}
		printf("Category should be dairy, sweets, meat, or fruit!\n");
	}
}

int readInteger(char* inputMessage,char* errorMessage)
{
	while (1)
	{
		char input[255];
		printf("%s", inputMessage);
		scanf("%s", input);
		int ok = 1, sign = 0, nr = 0;
		if (input[0] == '-') sign = 1;
		for (int i = sign; i < strlen(input); i++)
		{
			if (input[i]<'0' || input[i]>'9') ok = 0;
		}
		if (ok)
		{
			for (int i = sign; i < strlen(input); i++) nr = nr * 10 + (input[i] - '0');
			if (sign) nr = 0 - nr;
			return nr;
		}
		printf("%s", errorMessage);
	}
}

int readPositiveInteger(char* inputMessage,char* errorMessage)
{
	int a;
	while (1)
	{
		a = readInteger(inputMessage,errorMessage);
		if (a > 0) return a;
		printf(errorMessage);
	}
}

int readMonth()
{
	int a;
	while (1)
	{
		a = readPositiveInteger("Insert month here:", "Month should be an integer between 1 and 12!\n");
		if (a >= 1 && a <= 12) return a;
		printf("Month should be an integer between 1 and 12!\n");
	}
}

int readDay(int month, int year)
{
	int a;
	int daysInMonth[] = {31,28,31,31,31,31,31,31,31,31,31,31};
	if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) daysInMonth[1] = 29;
	while (1)
	{
		a = readPositiveInteger("Insert day here:","Day must be a positive integer!\n");
		if (a >= 1 && a <= daysInMonth[month-1]) return a;
		printf("The day can take values between 1 and %d!\n",daysInMonth[month-1]);
	}
}

void addOption(controller* x, char* use)
{	
	char name[255], category[255];
	int quantity, expiration_day, expiration_month, expiration_year;
	readString(name, "Insert name here:");
	readCategory(category, "Insert category here:",0);
	quantity = readPositiveInteger("Insert quantity:", "Quantity should be a positive integer!\n");
	expiration_year = readPositiveInteger("Insert expiration year:","Year should be a positive integer!\n");
	expiration_month = readMonth();
	expiration_day = readDay(expiration_month,expiration_year);
	add(x, createFood(name, category, quantity, expiration_day, expiration_month, expiration_year));
	printf("The given food has been %sed!\n\n",use);
}

int deleteOption(controller* x, char* use)
{
	if (x->repo->d->length > 0)
	{
		food y;
		char name[255], category[255];
		printf("Type 'back' when asked for the name if you want to return to the menu!\n");
		while (1)
		{
			readString(name, "Insert name here:");
			if (_stricmp(name, "back") == 0)
			{
				printf("Returned to the menu!\n\n");
				return 1;
			}
			if (findName(x, name) == 0) printf("There is no product with the given name!\n");
			else break;
		}
		while (1)
		{
			readCategory(category, "Insert category here:",0);
			if (findByNameAndCategory(x, name, category) == 0) printf("There is no product with the given combination of name and category!\n");
			else
			{
				y = createFood(name, category, 1, 1, 1, 1);
				delete(x, y);
				if (_stricmp(use, "delete") == 0) printf("The given food has been removed!\n\n");
				break;
			}
		}
	}
	else printf("The repository is empty. There is nothing to %s!\n\n",use);
	return 0;
}

int updateOption(controller *x)
{
	if (deleteOption(x,"update")==1) return 1;
	addOption(x,"updat");
	return 0;
}

void printController(controller* x)
{
	if (x->repo->d->length > 0)
	{
		int maxNameLength = 0;
		for (int i = 0; i < x->repo->d->length; i++)
			if (maxNameLength < strlen(x->repo->d->foods[i].name)) maxNameLength = strlen(x->repo->d->foods[i].name);
		printf("Name\t");
		for (int j = 0; j < maxNameLength / 8; j++) printf("\t");
		printf("Category  Quantity  Exp.Day  Exp.Month  Exp.Year\n");
		for (int i = 0; i < x->repo->d->length; i++)
		{
			printf("%s\t", x->repo->d->foods[i].name);
			for (int j = 0; j < maxNameLength / 8 - strlen(x->repo->d->foods[i].name) / 8; j++) printf("\t");
			printf("%s\t  ", x->repo->d->foods[i].category);
			printf("%d", x->repo->d->foods[i].quantity);
			for (int j = 0; j < 10 - digits(x->repo->d->foods[i].quantity); j++) printf(" ");
			printf("%d", x->repo->d->foods[i].expiration_day);
			for (int j = 0; j < 9 - digits(x->repo->d->foods[i].expiration_day); j++) printf(" ");
			printf("%d", x->repo->d->foods[i].expiration_month);
			for (int j = 0; j < 11 - digits(x->repo->d->foods[i].expiration_month); j++) printf(" ");
			printf("%d\n", x->repo->d->foods[i].expiration_year);
		}
		printf("\n");
	}
	else printf("There is no data to be displayed!\n\n");
}

void findOption(controller* control)
{
	repository* repo = createRepository();
	controller* filtered = createController(repo);
	char string[255];
	readString(string, "Insert string here:");
	filter(control, filtered, string);
	if (filtered->repo->d->length > 0) printController(filtered);
	else printf("There is no data containing the given string!\n\n");
	destroyController(filtered);
	destroyRepository(repo);
}

void expirationFilter(controller* control)
{
	repository* repo = createRepository();
	controller* filtered = createController(repo);
	char category[255];
	int days;
	readCategory(category, "Read category:", 1);
	days = readPositiveInteger("Give number of days:", "Not a positive integer!");
	filterByDaysLeft(control, filtered, category, days);
	if (filtered->repo->d->length > 0) printController(filtered);
	else if (_stricmp(category, "*") != 0) printf("There is no food of the given category going bad in the next %d days!\n\n", days);
		 else printf("There is no food going bad in the next %d days!\n\n", days);
	destroyController(filtered);
	destroyRepository(repo);
}

void undo(controller* control,repository* undo)
{
	free(control);
	control = createController(undo);
	printf("The last operation that changed the data has been undone\n\n");
}

void redo(controller* control,repository* redo)
{
	free(control);
	control = createController(redo);
	printf("The last undo has been redone!\n\n");
}

void run(controller* control)
{
	repository* undorepo = createRepository();
	repository* redorepo = createRepository();
	int lastOperation = 2; // 0 for undo, 1 for rest
	char string[255];
	int keepGoing = 1;
	do{
		rewind(stdin);
		printMenu();
		char option[255];
		readString(option,"Insert option here:");
		if (option[1] == NULL && option[0] != NULL)
		{
			switch (option[0])
			{
			case '1':
				copyRepository(control->repo, undorepo);
				lastOperation = 1;
				addOption(control,"add");
				break;
			case '2':
				copyRepository(control->repo, undorepo);
				lastOperation = 1;
				deleteOption(control,"delete");
				break;
			case '3':
				copyRepository(control->repo, undorepo);
				lastOperation = 1;
				updateOption(control);
				break;
			case '4':
				printController(control);
				break;
			case '5':
				findOption(control);
				break;
			case '6':
				expirationFilter(control);
				break;
			case '7':
				if (lastOperation == 1) {
					lastOperation = 0;
					copyRepository(control->repo, redorepo);
					undo(control, undorepo);
				}
				else printf("The last operation that changed the data can't be undone!\n\n");
				break;
			case '8':
				if (lastOperation == 0) {
					lastOperation = 1;
					copyRepository(control->repo, undorepo);
					redo(control, redorepo);
				}
				else printf("The last operation has to be an undo!\n\n");
				break;
			case '0':
				keepGoing = 0;
				printf("Exiting...\n");
				break;
			default:
				printf("Invalid option! Try again.\n");
			}
		}
		else printf("Invalid option! Try again.\n");

	} while (keepGoing);
	destroyRepository(undorepo);
	destroyRepository(redorepo);
}