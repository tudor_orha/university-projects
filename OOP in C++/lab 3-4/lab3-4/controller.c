#include "controller.h"
#include "repository.h"
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

// Creates a new controller, based on a given repository
controller* createController(repository* x)
{
	controller* c = malloc(sizeof(controller));
	c->repo = x;
	return c;
}

// Deallocates the memory for a given controller.
void destroyController(controller* c)
{
	if (c == NULL) return;
	free(c);
}

/*
Input : A controller and a food object.
Output: Adds the food object to the controller, and returns the new controller.
*/
void add(controller* x, food f)
{
	addToRepository(x->repo, f);
	//x->lastOperation.element[x->lastOperation.length] = 1;
	//x->lastOperation.length++;
	//addToRepository(&(x->lastFood), y);
}

/*
Input : A controller and a food object
Output: Deletes the food object from the contoller, and returns the new controller.
*/
void delete(controller* x, food f)
{
	deleteFromRepository(x->repo, f);
	/*
	deleteFromRepository(&(x->repo), y);
	x->lastOperation.element[x->lastOperation.length] = 2;
	x->lastOperation.length++;
	addToRepository(&(x->lastFood), y);*/
}

/*
Input : A controller and 2 food objects
Output: Removes the old food and adds the new food to the controller, returns the new controller.
*/
void update(controller* x, food old, food new)
{
	updateRepository(x->repo, old, new);
}

/*
Input : An integer.
Output: Returns the number of digits.
*/
int digits(int x)
{
	int nrOfDigits = 0;
	if (x == 0) nrOfDigits++;
	while (x != 0)
	{
		nrOfDigits++;
		x /= 10;
	}
	return nrOfDigits;
}

/*
Input : A controller object and a string
Output: Returns 0 if the given string is not a name in the controller, 1 otherwise.
*/
int findName(controller* x, char* string)
{
	int nameFound = 0;
	for (int i = 0; i < x->repo->d->length; i++)
	{
		if (_stricmp(x->repo->d->foods[i].name, string) == 0) nameFound = 1;
	}
	return nameFound;
}

/*
Input : One controller and 2 strings.
Output: Returns 1 if there is an element with the given name and category into the controller, 0 otherwise.
*/
int findByNameAndCategory(controller* x, char* name, char* category)
{
	int elementFound = 0;
	for (int i = 0; i < x->repo->d->length; i++)
	{
		if (_stricmp(x->repo->d->foods[i].name, name) == 0 && _stricmp(x->repo->d->foods[i].category, category) == 0) elementFound = 1;
	}
	return elementFound;
}

/*
Input : A controller and a string.
Output: Returns a controller that contains all the elements that contain the string as a substring in their name or category.
*/
void filter(controller* x, controller* filtered, char* string)
{
	for (int i = 0; i < x->repo->d->length; i++)
	{
		if (strstr(x->repo->d->foods[i].name, string) != NULL)
		{
			add(filtered, x->repo->d->foods[i]);
			int j = filtered->repo->d->length - 1;
			food auxf = x->repo->d->foods[i];
			int aux = x->repo->d->foods[i].quantity;
			while (j > 0 && aux < filtered->repo->d->foods[j - 1].quantity)
			{
				filtered->repo->d->foods[j] = filtered->repo->d->foods[j - 1];
				j--;
			}
			filtered->repo->d->foods[j] = auxf;
		}
	}
}


void filterByDaysLeft(controller* x,controller* filtered,char* category,int days)
{
	time_t t = time(NULL);
	struct tm current_time = *localtime(&t);
	current_time.tm_mday += days;
	mktime(&t); //fixes time ( no dates like 40 jan )
	for (int i = 0; i < x->repo->d->length; i++)
	{
		if (_stricmp(category, "*") == 0 || _stricmp(x->repo->d->foods[i].category, category) == 0)
		{
			if (x->repo->d->foods[i].expiration_year < current_time.tm_year + 1900) add(filtered, x->repo->d->foods[i]);
			if (x->repo->d->foods[i].expiration_year == current_time.tm_year + 1900 && x->repo->d->foods[i].expiration_month < current_time.tm_mon + 1) add(filtered, x->repo->d->foods[i]);
			if (x->repo->d->foods[i].expiration_year == current_time.tm_year + 1900 && x->repo->d->foods[i].expiration_month == current_time.tm_mon + 1 &&
				x->repo->d->foods[i].expiration_day <= current_time.tm_mday) add(filtered, x->repo->d->foods[i]);
			int j = filtered->repo->d->length - 1;
			food auxf = x->repo->d->foods[i];
			while (j > 0 && _stricmp(auxf.name , filtered->repo->d->foods[j - 1].name) > 0)
			{
				filtered->repo->d->foods[j] = filtered->repo->d->foods[j - 1];
				j--;
			}
			filtered->repo->d->foods[j] = auxf;
		}
	}
}



void testDigits()
{
	int numbers[] = {53,636,1241,22152,32,5,0};
	int results[] = {2,3,4,5,2,1,1};
	for (int i = 0; i < 7; i++)
	{
		assert(digits(numbers[i])==results[i]);
	}

}

void testController()
{
	testDigits();
	repository* repo = createRepository(), *repo2 = createRepository();
	controller* control = createController(repo), *control2 = createController(repo2);
	food f1 = createFood("Milk", "dairy", 15, 23, 3, 2016);
	food f2 = createFood("Chicken", "meat", 12, 20, 3, 2016);
	food f3 = createFood("Skittles", "sweets", 5, 30, 7, 2018);
	add(control, f1);
	add(control, f2);
	add(control, f3);
	assert(findName(control, "Milk") == 1);
	assert(findName(control, "Mfsal") == 0);
	assert(findByNameAndCategory(control, "Milk", "dairy") == 1);
	assert(findByNameAndCategory(control, "Milk", "meat") == 0);
	add(control2, f1);
	add(control2, f3);
	delete(control, f2);
	for (int i = 0; i < 2; i++)
	{
		assert(compareFood(control->repo->d->foods[i], control2->repo->d->foods[i]) == 1);
	}
}
