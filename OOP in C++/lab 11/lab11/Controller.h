#pragma once
#include "Repository.h"

class Controller
{
private:
	Repository repo;
public:
	// Controller class constructor. Input: a Repository type object
	Controller(const Repository& r) : repo(r) {};
	// Adds a Dog object to the controller. Input: Dog attributes
	void addDog(std::string Breed, std::string Name, int Age, std::string Photo);
	// Removes a dog object from the controller. Input: Dog breed and name
	void removeDog(std::string Breed, std::string Name);
	// Searches for dogs by a given breed.
	// Input: Breed
	// Output: 1 if a dog is found, 0 otherwise
	bool findBreed(std::string Breed);
	/*
	Searches for dogs by a given breed and name.
	Input: Breed and Name strings
	Output: 1 if a dog is found with the given Input, 0 otherwise
	*/
	bool findBreedAndName(std::string Breed, std::string Name);
	/*
	Input: string Breed, int Age
	Output: A Dynamic vector containing all dogs with the given breed and an age smaller than the one from the input.
	*/
	std::vector<Dog> filterBreedAndAge(std::string Breed, int Age);
	// Returns all the dog data from the controller
	std::vector<Dog> getDogs() { return repo.getDogs(); };
	// Returns the number of dogs from the controller
	unsigned long getSize() { return repo.getSize(); };
	unsigned int getCurrent() { return this->repo.getCurrent(); }
	void startDog() { this->repo.startDog(); }
	void nextDog() { this->repo.nextDog(); }
};