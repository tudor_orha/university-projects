#ifndef LAB11_H
#define LAB11_H

#include <QtWidgets/QMainWindow>
#include "ui_lab11.h"
#include "Controller.h"
#include <QListWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>

class lab11 : public QWidget
{
	Q_OBJECT

public:
	lab11(Controller &c,Controller &a,QWidget *parent = 0);
	~lab11();

private:
	Ui::lab11Class ui;
	Controller& ctrl;
	Controller& adoption;
	std::vector<Dog> currentSongsInRepoList;

	void initGUI();
	void populateRepoList();
	void populatePlaylist();
	void connectSignalsAndSlots();
	int getRepoListSelectedIndex();


	QListWidget* repoList;
	QLineEdit* titleEdit;
	QLineEdit* artistEdit;
	QLineEdit* durationEdit;
	QLineEdit* linkEdit;
	QPushButton* addButton;
	QPushButton* deleteButton;
	QPushButton* filterButton;
	QPushButton* moveOneSongButton;
	QPushButton* moveAllSongsButton;
	QPushButton* openPhotoButton;
	QListWidget* playList;

public slots:
	void addDog();
	void destroyDog();
	void moveDogToAdoptionList();
	void listItemChanged();
	void filterRepoDogs();
	void openPhoto();
};

#endif // LAB11_H
