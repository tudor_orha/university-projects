#pragma once
#include "Controller.h"

class UI
{
private:
	Controller control;

public:
	UI(const Controller& c) : control(c) {}
	void run();

private:
	void printAdminMenu();
	void printUserMenu();
	void adminMode();
	void userMode();
	void add(std::string use);
	void remove(std::string use);
	void update();
	void printController();
	int readPositiveInteger();
	void printDog(Dog d);
	std::string readString(std::string str);
	int stringIsPositiveInteger(std::string str);
};