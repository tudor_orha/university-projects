#include "UI.h"
#include "HTMLRepository.h"
#include <iostream>
#include <string>
#include <windows.h>

using namespace std;


void UI::run()
{
	int option = 1;
	cout << "1. Administrator Mode!\n";
	cout << "2. User Mode!\n";
	cout << "0. Exit\n";
	while (option != 0) {
		cout << "Insert option here:";
		option = readPositiveInteger();
		cout << "\n";
		switch (option) {
		case 1:
			adminMode();
			return;
		case 2:
			userMode();
			return;
		case 0:
			cout << "Exiting...\n";
			return;
		default:
			cout << "That was not a valid option! Try again.\n";
		}
	}
}


void UI::printAdminMenu()
{
	cout << "1. Add dog.\n";
	cout << "2. Remove dog.\n";
	cout << "3. Update dog.\n";
	cout << "4. Show dog data.\n";
	cout << "0. Exit.\n";
}

void UI::printUserMenu()
{
	cout << "1. See dogs one by one.\n";
	cout << "2. Adopt dog.\n";
	cout << "3. Next dog.\n";
	cout << "4. See all dogs of given breed and age.\n";
	cout << "5. See the adoption list.\n";
	cout << "6. Open Adoption list.\n";
	cout << "0. Exit.\n";
}

void UI::adminMode()
{
	int option;
	while (1)
	{
		printAdminMenu();
		cout << "Insert option here:";
		option = readPositiveInteger();
		switch (option) {
		case 1:
			add("add");
			break;
		case 2:
			remove("remove");
			break;
		case 3:
			update();
			break;
		case 4:
			printController();
			break;
		case 0:
			cout << "Exiting...\n";
			return;
		default:
			cout << "That option is not valid! Try again.\n";
		}
	}
}

void UI::userMode()
{
	bool ok;
	int option, age;
	string breed, save_type;
	vector<Dog> filtered;
	do
	{
		cout << "1. CSV adoption list!\n";
		cout << "2. HTML adoption list!\n";
		cout << "0. Exit!\n";
		cout << "Insert option here:";
		option = readPositiveInteger();
		switch (option) {
		case 1:
			save_type = "Adoption.txt";
			break;
		case 2:
			save_type = "Adoption.html";
			break;
		case 0:
			cout << "Exiting...\n";
			return;
		default:
			cout << "That option is invalid! Try again!\n\n";
			break;
		}
	} while (option != 0 && option != 1 && option != 2);
	if (save_type == "Adoption.html") {
		HTMLRepository adoption("Adoption.html");
		while (1)
		{
			printUserMenu();
			cout << "Insert option here:";
			option = readPositiveInteger();
			switch (option) {
			case 1:
				system("cls");
				this->control.startDog();
				printDog(this->control.getDogs()[0]);
				break;
			case 2:
				ok = 0;
				for (Dog d : adoption.getDogs())
				{
					if (d.getBreed() == this->control.getDogs()[this->control.getCurrent()].getBreed() &&
						d.getName() == this->control.getDogs()[this->control.getCurrent()].getName())  ok = 1;
				}
				if (ok == 0) adoption.addDog(this->control.getDogs()[this->control.getCurrent()]);
				else cout << "Dog has already been added to the adoption list!\n\n";
				break;
			case 3:
				system("cls");
				this->control.nextDog();
				printDog(this->control.getDogs()[this->control.getCurrent()]);
				break;
			case 4:
				while (1)
				{
					breed = readString("breed");
					if (this->control.findBreed(breed) != 1 && breed != "*")
						cout << "There is no dog in the database with the given breed!\n";
					else break;
				}
				cout << "Insert age here:";
				age = readPositiveInteger();
				filtered = this->control.filterBreedAndAge(breed, age);
				cout << "\n";
				for (Dog d : filtered) {
					cout << "Name : " << d.getName() << "\n";
					cout << "Breed: " << d.getBreed() << "\n";
					cout << "Age  : " << d.getAge() << "\n\n";
				}

				break;
			case 5:
				if (adoption.getSize() == 0) cout << "No dogs in the adoption list yet!\n\n";
				else {
					for (Dog d : adoption.getDogs()) {
						cout << "Name : " << d.getName() << "\n";
						cout << "Breed: " << d.getBreed() << "\n";
						cout << "Age  : " << d.getAge() << "\n\n";
					}
				}
				break;
			case 6:
				ShellExecuteA(NULL, NULL, "chrome.exe", "file:///E:/Programare/school/Object%20Oriented%20Programming/lab%208-9/lab%208-9/Adoption.html", NULL, SW_MAXIMIZE);
				break;
			case 0:
				cout << "Exiting...\n";
				return;
			default:
				cout << "That option is not valid! Try again.\n";
			}
		}
	}
	else
	{
		Repository adoption("Adoption.txt");
		while (1)
		{
			printUserMenu();
			cout << "Insert option here:";
			option = readPositiveInteger();
			switch (option) {
			case 1:
				system("cls");
				this->control.startDog();
				printDog(this->control.getDogs()[0]);
				break;
			case 2:
				ok = 0;
				for (Dog d : adoption.getDogs())
				{
					if (d.getBreed() == this->control.getDogs()[this->control.getCurrent()].getBreed() &&
						d.getName() == this->control.getDogs()[this->control.getCurrent()].getName())  ok = 1;
				}
				if (ok == 0) adoption.addDog(this->control.getDogs()[this->control.getCurrent()]);
				else cout << "Dog has already been added to the adoption list!\n\n";
				break;
			case 3:
				system("cls");
				this->control.nextDog();
				printDog(this->control.getDogs()[this->control.getCurrent()]);
				break;
			case 4:
				while (1)
				{
					breed = readString("breed");
					if (this->control.findBreed(breed) != 1 && breed != "*")
						cout << "There is no dog in the database with the given breed!\n";
					else break;
				}
				cout << "Insert age here:";
				age = readPositiveInteger();
				filtered = this->control.filterBreedAndAge(breed, age);
				cout << "\n";
				for (Dog d : filtered) {
					cout << "Name : " << d.getName() << "\n";
					cout << "Breed: " << d.getBreed() << "\n";
					cout << "Age  : " << d.getAge() << "\n\n";
				}

				break;
			case 5:
				if (adoption.getSize() == 0) cout << "No dogs in the adoption list yet!\n\n";
				else {
					for (Dog d : adoption.getDogs()) {
						cout << "Name : " << d.getName() << "\n";
						cout << "Breed: " << d.getBreed() << "\n";
						cout << "Age  : " << d.getAge() << "\n\n";
					}
				}
				break;
			case 6:
				ShellExecuteA(NULL, NULL, "notepad.exe", "Adoption.txt", NULL, SW_MAXIMIZE);
				break;
			case 0:
				cout << "Exiting...\n";
				return;
			default:
				cout << "That option is not valid! Try again.\n";
			}
		}
	}
}

void UI::add(string use)
{
	string breed, name, photo;
	int age;
	while (1)
	{
		breed = readString("breed");
		name = readString("name");
		if (this->control.findBreedAndName(breed, name) == 0) break;
		else cout << "That dog already exists!\n";
	}
	cout << "Insert age here:";
	age = readPositiveInteger();
	photo = readString("photo link");
	this->control.addDog(breed, name, age, photo);
	cout << "The given dog has been " << use << "ed!\n\n";
}

void UI::remove(string use)
{
	string breed, name;
	while (1)
	{
		breed = readString("breed");
		if (this->control.findBreed(breed) == 1) break;
		else cout << "That breed does not exist in the database!\n";
	}
	while (1)
	{
		name = readString("name");
		if (this->control.findBreedAndName(breed, name) == 1) break;
		else cout << "There is no dog with the given combination of breed and name!\n";
	}
	this->control.removeDog(breed, name);
	if (use == "remove") cout << "The given dog has been removed!\n\n";
}

void UI::update()
{
	remove("update");
	add("update");
}

void UI::printController()
{
	for (unsigned int i = 0; i < this->control.getSize(); i++)
	{
		cout << this->control.getDogs()[i].getBreed() << " " << this->control.getDogs()[i].getName() << " ";
		cout << this->control.getDogs()[i].getAge() << " " << this->control.getDogs()[i].getPhoto() << "\n";
	}
	cout << "\n";
}

void UI::printDog(Dog d)
{
	cout << "Name : " << d.getName() << "\n";
	cout << "Breed: " << d.getBreed() << "\n";
	cout << "Age  : " << d.getAge() << "\n\n";
	ShellExecuteA(NULL, NULL, "chrome.exe", d.getPhoto().c_str(), NULL, SW_MAXIMIZE);
}


int UI::readPositiveInteger()
{
	string input;
	int result;
	while (1)
	{
		getline(cin, input);
		result = stringIsPositiveInteger(input);
		if (result != -1) return result;
		else cout << "That was not a positive integer!\n";
	}
}

string UI::readString(string str)
{
	string input;
	cout << "Insert " << str << " here:";
	getline(cin, input);
	return input;
}

int UI::stringIsPositiveInteger(string str)
{
	bool isPositiveInteger = 1;
	int number = 0;
	for (unsigned int i = 0; i < str.length(); i++)
	{
		if (str[i] < '0' || str[i] > '9') isPositiveInteger = 0;
		else number = number * 10 + str[i] - '0';
	}
	if (isPositiveInteger == 0) return -1;
	else return number;
}
