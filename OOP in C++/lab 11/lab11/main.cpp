#include "lab11.h"
#include "Controller.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Repository repo{ "Dogs.txt" };
	Repository adoption{ "Adoption.txt" };
	repo.readFromFile();
	Controller ctrl{ repo };
	Controller adopt{ adoption };
	lab11 w{ ctrl,adopt };
	w.show();
	return a.exec();
}