#pragma once
#include "Repository.h"

class HTMLRepository : public Repository {
public:
	HTMLRepository(std::string file) : Repository(file) {};
	~HTMLRepository() {};
	void storeToFile() override;
};