/********************************************************************************
** Form generated from reading UI file 'lab11.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LAB11_H
#define UI_LAB11_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_lab11Class
{
public:
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *lab11Class)
    {
        if (lab11Class->objectName().isEmpty())
            lab11Class->setObjectName(QStringLiteral("lab11Class"));
        lab11Class->resize(600, 400);
        centralWidget = new QWidget(lab11Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lab11Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(lab11Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        lab11Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(lab11Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        lab11Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(lab11Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        lab11Class->setStatusBar(statusBar);

        retranslateUi(lab11Class);

        QMetaObject::connectSlotsByName(lab11Class);
    } // setupUi

    void retranslateUi(QMainWindow *lab11Class)
    {
        lab11Class->setWindowTitle(QApplication::translate("lab11Class", "lab11", 0));
    } // retranslateUi

};

namespace Ui {
    class lab11Class: public Ui_lab11Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LAB11_H
