#include "lab11.h"
#include "Controller.h"
#include <QMessageBox>
#include <windows.h>

lab11::lab11(Controller &c,Controller &a, QWidget *parent) : ctrl{ c }, QWidget{ parent }, adoption{a}
{
	this->initGUI();
	this->currentSongsInRepoList = this->ctrl.getDogs();
	this->populateRepoList();
}

lab11::~lab11()
{

}

void lab11::populateRepoList()
{
	// clear the list, if there are elements in it
	if (this->repoList->count() > 0)
		this->repoList->clear();
	for (auto s : this->currentSongsInRepoList)
	{
		QString itemInList = QString::fromStdString(s.getName() + "  " + s.getBreed());
		this->repoList->addItem(itemInList);
	}

	// set the selection on the first item in the list
	if (this->currentSongsInRepoList.size() > 0)
		this->repoList->setCurrentRow(0);
}

void lab11::populatePlaylist()
{
	// clear the list, if there are elements in it
	if (this->playList->count() > 0)
		this->playList->clear();

	for (auto s : this->adoption.getDogs())
	{
		QString itemInList = QString::fromStdString(s.getName() + " - " + s.getBreed());
		this->playList->addItem(itemInList);
	}
}

void lab11::addDog()
{
	std::string name = this->titleEdit->text().toStdString();
	std::string breed = this->artistEdit->text().toStdString();
	int age = atoi(this->durationEdit->text().toStdString().c_str());
	std::string source = this->linkEdit->text().toStdString();
	if (this->ctrl.findBreedAndName(breed, name) == 0) {
		this->ctrl.addDog(breed, name, age, source);
		// refresh the list
		this->currentSongsInRepoList = this->ctrl.getDogs();
		this->populateRepoList();
	}
	else {
		QMessageBox messageBox;
		messageBox.critical(0, "Error", QString::fromStdString("Dog already exists!"));
	}
}

void lab11::destroyDog() {
	std::string name = this->titleEdit->text().toStdString();
	std::string breed = this->artistEdit->text().toStdString();
	int age = atoi(this->durationEdit->text().toStdString().c_str());
	std::string source = this->linkEdit->text().toStdString();

	if (this->ctrl.findBreedAndName(breed,name) == 1) {
		this->ctrl.removeDog(breed,name);
		this->currentSongsInRepoList = this->ctrl.getDogs();
		this->populateRepoList();
	}
	else
	{
		QMessageBox messageBox;
		messageBox.critical(0, "Error", QString::fromStdString("Dog doesn't exist!"));
	}
}

int lab11::getRepoListSelectedIndex()
{
	if (this->repoList->count() == 0)
		return -1;

	// get selected index
	QModelIndexList index = this->repoList->selectionModel()->selectedIndexes();
	if (index.size() == 0)
	{
		this->artistEdit->clear();
		this->titleEdit->clear();
		this->durationEdit->clear();
		this->linkEdit->clear();
		return -1;
	}

	int idx = index.at(0).row();
	return idx;
}

void lab11::moveDogToAdoptionList()
{
	int idx = this->getRepoListSelectedIndex();
	if (idx == -1 || idx >= this->currentSongsInRepoList.size())
		return;

	Dog d = this->currentSongsInRepoList[idx];
	if (this->adoption.findBreedAndName(d.getBreed(), d.getName()) == 0){
		this->adoption.addDog(d.getBreed(), d.getName(), d.getAge(), d.getPhoto());
		this->populatePlaylist();
	}
	else {
		QMessageBox messageBox;
		messageBox.critical(0, "Error", QString::fromStdString("This dog is already in the adoption list!"));
	}
}

void lab11::filterRepoDogs()
{
	std::string breed = this->artistEdit->text().toStdString();
	int age = atoi(this->durationEdit->text().toStdString().c_str());
	if (breed == "")
	{
		this->currentSongsInRepoList = this->ctrl.getDogs();
		this->populateRepoList();
		return;
	}

	this->currentSongsInRepoList = this->ctrl.filterBreedAndAge(breed, age);
	this->populateRepoList();
}


void lab11::listItemChanged()
{
	int idx = this->getRepoListSelectedIndex();
	if (idx == -1)
		return;

	std::vector<Dog> dogs = this->currentSongsInRepoList;

	// get the dog at the selected index
	if (idx >= dogs.size())
		return;
	Dog d = dogs[idx];

	std::string age2 = std::to_string(d.getAge());
	this->artistEdit->setText(QString::fromStdString(d.getBreed()));
	this->titleEdit->setText(QString::fromStdString(d.getName()));
	this->durationEdit->setText(QString::fromStdString(age2));
	this->linkEdit->setText(QString::fromStdString(d.getPhoto()));
}

void lab11::openPhoto() {
	Dog d = this->adoption.getDogs()[0];
	ShellExecuteA(NULL, NULL, "chrome.exe", d.getPhoto().c_str(), NULL, SW_MAXIMIZE);

}

void lab11::connectSignalsAndSlots()
{
	// add a connection: function listItemChanged() will be called when an item in the list is selected
	QObject::connect(this->repoList, SIGNAL(itemSelectionChanged()), this, SLOT(listItemChanged()));

	// add button connections
	QObject::connect(this->addButton, SIGNAL(clicked()), this, SLOT(addDog()));
	QObject::connect(this->deleteButton, SIGNAL(clicked()), this, SLOT(destroyDog()));
	QObject::connect(this->filterButton, SIGNAL(clicked()), this, SLOT(filterRepoDogs()));

	QObject::connect(this->moveOneSongButton, SIGNAL(clicked()), this, SLOT(moveDogToAdoptionList()));
	//QObject::connect(this->openPhotoButton, SIGNAL(clicked()), this, SLOT(openPhoto()));
	//QObject::connect(this->moveAllSongsButton, SIGNAL(clicked()), this, SLOT(moveAllSongs()));
}

void lab11::initGUI()
{
	//General layout of the window
	QHBoxLayout* layout = new QHBoxLayout{ this };

	// Prepare left side components - vertical layout with: 
	// - list
	// - form layout with the song data
	// - grid layout with buttons: add, delete, update, filter
	QWidget* leftWidget = new QWidget{};
	QVBoxLayout* leftSide = new QVBoxLayout{ leftWidget };

	// list
	this->repoList = new QListWidget{};
	// set the selection model
	this->repoList->setSelectionMode(QAbstractItemView::SingleSelection);

	// Dog data
	QWidget* songDataWidget = new QWidget{};
	QFormLayout* formLayout = new QFormLayout{ songDataWidget };
	this->titleEdit = new QLineEdit{};
	this->artistEdit = new QLineEdit{};
	this->durationEdit = new QLineEdit{};
	this->linkEdit = new QLineEdit{};
	formLayout->addRow("&Name:", titleEdit);
	formLayout->addRow("&Breed:", artistEdit);
	formLayout->addRow("&Age:", durationEdit);
	formLayout->addRow("&Link:", linkEdit);

	// buttons
	QWidget* buttonsWidget = new QWidget{};
	QGridLayout* gridLayout = new QGridLayout{ buttonsWidget };
	this->addButton = new QPushButton("Add");
	this->deleteButton = new QPushButton("Delete");
	this->filterButton = new QPushButton("Filter");

	gridLayout->addWidget(addButton, 0, 0);
	gridLayout->addWidget(deleteButton, 0, 1);
	gridLayout->addWidget(filterButton, 0, 2);

	// add everything to the left layout
	leftSide->addWidget(new QLabel{ "All dogs" });
	leftSide->addWidget(repoList);
	leftSide->addWidget(songDataWidget);
	leftSide->addWidget(buttonsWidget);

	// middle component: just two button - to add the songs from the reposiotory to the playlist
	QWidget* middleWidget = new QWidget{};
	QVBoxLayout* vLayoutMiddle = new QVBoxLayout{ middleWidget };
	this->moveOneSongButton = new QPushButton{ ">> Move one dog" };
	//this->moveAllSongsButton = new QPushButton{ ">> Move all dogs" };
	QWidget* upperPart = new QWidget{};
	QWidget* lowerPart = new QWidget{};
	QVBoxLayout* vLayoutUpperPart = new QVBoxLayout{ upperPart };
	vLayoutUpperPart->addWidget(this->moveOneSongButton);
	//vLayoutUpperPart->addWidget(this->moveAllSongsButton);
	vLayoutMiddle->addWidget(upperPart);
	vLayoutMiddle->addWidget(lowerPart);

	// right component: the playlist
	QWidget* rightWidget = new QWidget{};
	QVBoxLayout* rightSide = new QVBoxLayout{ rightWidget };

	// playlist
	playList = new QListWidget{};

	// two buttons
	QWidget* playlistButtonsWidget = new QWidget{};
	QHBoxLayout* playlistButtonsLayout = new QHBoxLayout{ playlistButtonsWidget };
	//this->openPhotoButton = new QPushButton("Gsad");
	playlistButtonsLayout->addWidget(new QPushButton{ "&Open Photo" });
	playlistButtonsLayout->addWidget(new QPushButton{ "&Next" });

	// add everything to the right layout
	rightSide->addWidget(new QLabel{ "Adoption List" });
	rightSide->addWidget(playList);
	rightSide->addWidget(playlistButtonsWidget);

	// add the three layouts to the main layout
	layout->addWidget(leftWidget);
	layout->addWidget(middleWidget);
	layout->addWidget(rightWidget);

	// connect the signals and slots
	this->connectSignalsAndSlots();
}