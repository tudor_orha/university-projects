#include "Controller.h"

using namespace std;

void Controller::addDog(string Breed, string Name, int Age, string Photo)
{
	Dog dog{ Breed, Name, Age, Photo };
	this->repo.addDog(dog);
}

void Controller::removeDog(string Breed, string Name)
{
	Dog dog{Breed, Name, 0, ""};
	this->repo.removeDog(dog);
}

bool Controller::findBreed(string Breed)
{
	for (Dog d : this->repo.getDogs())
	{
		if (d.getBreed() == Breed) return 1;
	}
	return 0;
}

bool Controller::findBreedAndName(string Breed, string Name) {
	for (Dog d : this->repo.getDogs())
	{
		if (d.getBreed() == Breed && d.getName() == Name) return 1;
	}
	return 0;
}

vector<Dog> Controller::filterBreedAndAge(string Breed, int Age) {
	vector<Dog> dogs;
	for (Dog d: this->repo.getDogs())
	{
		if ((Breed == "" || d.getBreed() == Breed) && d.getAge() < Age) dogs.push_back(d);
	}
	return dogs;
}