/**
 * Created by Tudor on 6/20/2017.
 */
import {Component, Input, OnInit} from '@angular/core';
import {PizzaService} from "../shared/pizza.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from '@angular/common';
import {Pizza} from '../shared/pizza';


@Component({
  moduleId: module.id,
  selector: 'ingredient-list',
  templateUrl: './pizzashop2010-pizza-detail.html',
  styleUrls: ['./pizzashop2010-pizza-detail.css'],
})
export class PizzaListIngredientsComponent implements OnInit {
  @Input() pizza: Pizza;

  constructor(private pizzaService: PizzaService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.pizzaService.getPizza(+params['id']))
      .subscribe(pizza => this.pizza = pizza);
  }

  goBack(): void {
    this.location.back();
  }

}
