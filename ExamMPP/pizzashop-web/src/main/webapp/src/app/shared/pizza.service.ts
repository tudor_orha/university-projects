import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";
import {Pizza} from "./pizza";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class PizzaService {
  private pizzasUrl = 'http://localhost:8080/api/pizzas';
  private pizzasFilterUrl = 'http://localhost:8080/api/pizzafilter';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }


  private extractData(res: Response) {
    let body = res.json();
    return body.pizzas || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private extractPizzaData(res: Response) {
    let body = res.json();
    return body.client || {};
  }

  create(name: string, description: string, price: number): Observable<Pizza> {
    let pizza = {name, description, price};
    return this.http
      .post(this.pizzasUrl, JSON.stringify({"pizza": pizza}), {headers: this.headers})
      .map(this.extractPizzaData)
      .catch(this.handleError);
  }

  getPizzas(): Observable<Pizza[]> {
    return this.http.get(this.pizzasUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }


  update(id: number,price: number): Observable<Response> {
    const url = `${this.pizzasUrl}`;
    return this.http
      .put(url, JSON.stringify({"id":id,"price":price}), {headers: this.headers})
      .catch(this.handleError);
  }

  updateAll(price: number): Observable<Response>{
  const url = `${this.pizzasUrl}/updateall`;
  return this.http
    .post(url, JSON.stringify({"price": price}), {headers: this.headers})
    .catch(this.handleError);
  }

  getPizza(id: number): Observable<Pizza> {
    return this.getPizzas()
      .map(pizzas => pizzas.find(pizza => pizza.id === id));
  }

}
