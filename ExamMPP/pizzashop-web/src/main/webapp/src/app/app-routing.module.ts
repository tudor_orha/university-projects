import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Pizzashop10NewComponent} from "./pizzashop10-new/pizzashop10-new.component";
import {PizzasListComponent} from "./pizzashop2010-pizzas/pizzashop2010-pizzas.component";
import {PizzaListIngredientsComponent} from "./pizzashop2010-pizza-detail/pizzashop2010-pizza-detail";

const routes: Routes = [
  { path: '', redirectTo: 'pizzashop10/new', pathMatch: 'full' },
  { path: 'pizzashop10/new',     component: Pizzashop10NewComponent },
  { path: 'pizzashop2010/pizzas', component: PizzasListComponent},
  { path: 'pizzashop2010/pizza/:id', component: PizzaListIngredientsComponent}
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
