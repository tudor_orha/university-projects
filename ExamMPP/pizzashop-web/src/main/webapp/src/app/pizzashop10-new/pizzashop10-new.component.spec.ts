import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pizzashop10NewComponent } from './pizzashop10-new.component';

describe('Pizzashop10NewComponent', () => {
  let component: Pizzashop10NewComponent;
  let fixture: ComponentFixture<Pizzashop10NewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pizzashop10NewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pizzashop10NewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
