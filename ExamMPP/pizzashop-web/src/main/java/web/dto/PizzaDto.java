package web.dto;

import core.model.Pizza;
import lombok.*;

/**
 * Created by Tudor on 6/20/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PizzaDto extends BaseDto {
    private String name;
    private String description;
    private float price;
    private String ingredients;

    @Override
    public String toString() {
        return "PizzaDto{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price + + '\'' +
                "'} " + super.toString();
    }
}