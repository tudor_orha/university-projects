package web.dto;

import lombok.*;

/**
 * Created by Tudor on 6/20/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class FilterDto extends BaseDto {
    private String filterType;
    private String filterValue;
    @Override
    public String toString() {
        return "FilterDto{" +
                "filterType='" + filterType + '\'' +
                ", filterValue='" + filterValue +
                "'} " + super.toString();
    }
}
