package web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Tudor on 6/20/2017.
 */
@JsonSerialize
public class EmptyJsonResponse {
}
