package web.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Created by Tudor on 6/20/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class BaseDto implements Serializable {
    private Long id;
}