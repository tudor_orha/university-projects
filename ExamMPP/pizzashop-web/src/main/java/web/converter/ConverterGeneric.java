package web.converter;

/**
 * Created by Tudor on 6/20/2017.
 */
public interface ConverterGeneric<Model, Dto> {
    Model convertDtoToModel(Dto dto);
    Dto convertModelToDto(Model model);
}