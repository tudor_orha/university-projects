package web.converter;

import core.model.BaseEntity;
import web.dto.BaseDto;


/**
 * Created by Tudor on 6/20/2017.
 */
public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto> extends ConverterGeneric<Model, Dto> {
}
