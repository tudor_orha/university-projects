package web.converter;

import core.model.Pizza;
import core.model.Ingredient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import web.dto.PizzaDto;

import java.sql.Array;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;

/**
 * Created by Tudor on 6/20/2017.
 */
@Component
public class PizzaConverter extends BaseConverter<Pizza, PizzaDto> {
    private static final Logger log = LoggerFactory.getLogger(PizzaConverter.class);

    @Override
    public PizzaDto convertModelToDto(Pizza pizza) {
        Set<Ingredient> set = pizza.getPizzaIngredients();
        String Ingredients = set.toString();

        PizzaDto pizzaDto = PizzaDto.builder()
                .name(pizza.getName())
                .description(pizza.getDescription())
                .price(pizza.getPrice())
                .ingredients(Ingredients)
                .build();
        pizzaDto.setId(pizza.getId());
        return pizzaDto;
    }
}