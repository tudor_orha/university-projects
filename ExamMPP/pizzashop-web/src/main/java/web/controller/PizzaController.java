package web.controller;

import core.model.Pizza;
import core.service.PizzaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.converter.PizzaConverter;
import web.dto.FilterDto;
import web.dto.PizzaDto;
import web.dto.PizzasDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tudor on 6/20/2017.
 */
@RestController
public class PizzaController {
    private static final Logger log = LoggerFactory.getLogger(PizzaController.class);

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private PizzaConverter pizzaConverter;

    @RequestMapping(value = "/pizzas", method = RequestMethod.POST)
    public Map<String, PizzaDto> createPizza(
            @RequestBody final Map<String, PizzaDto> pizzaDtoMap) {
        log.trace("createPizza: pizzaDtoMap={}", pizzaDtoMap);

        PizzaDto pizzaDto = pizzaDtoMap.get("pizza");
        Pizza pizza = pizzaService.createPizza(pizzaDto.getName(), pizzaDto.getDescription(), pizzaDto.getPrice());

        Map<String, PizzaDto> result = new HashMap<>();
        result.put("pizza", pizzaConverter.convertModelToDto(pizza));

        log.trace("createPizza: result={}", result);

        return result;
    }


    @RequestMapping(value = "/pizzas", method = RequestMethod.GET)
    public PizzasDto getPizzas() {
        log.trace("getPizzas");
        List<Pizza> pizzas = pizzaService.findAll();
        log.trace("getPizzas: pizzas={}", pizzas);
        return new PizzasDto(pizzaConverter.convertModelsToDtos(pizzas));
    }

    @RequestMapping(value = "/pizzas", method = RequestMethod.PUT)
    public Map<String, PizzaDto> updateClient(
            @RequestBody Map<String,String> idPrice) {
        Long pizzaId = Long.valueOf(idPrice.get("id")).longValue();
        float price = Float.parseFloat(idPrice.get("price"));
        log.trace("updatePizza: pizzaId={}, pizzaPrice={}",pizzaId, price);
        Pizza pizza = pizzaService.updatePizza(pizzaId,price);
        Map<String, PizzaDto> result = new HashMap<>();
        result.put("pizza", pizzaConverter.convertModelToDto(pizza));
        log.trace("updatePizza: result={}", result);
        return result;
    }

    @RequestMapping(value = "/pizzas/updateall", method = RequestMethod.POST)
    public Map<String, PizzasDto> updateAllPizzas(
            @RequestBody Map<String,String> Price) {
        float price = Float.parseFloat(Price.get("price"));
        log.trace("updateAllPizzas: pizzaExtraPrice={}", price);
        List<Pizza> pizzas = pizzaService.updateAllPizzas(price);
        Map<String, PizzasDto> result = new HashMap<>();
        //result.put("pizzas", pizzaConverter.convertModelToDto(pizzas));
       // log.trace("updatePizza: result={}", result);
        return result;
    }


}