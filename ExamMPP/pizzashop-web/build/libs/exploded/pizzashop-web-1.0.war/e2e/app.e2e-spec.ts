import { PizzashopPage } from './app.po';

describe('pizzashop App', () => {
  let page: PizzashopPage;

  beforeEach(() => {
    page = new PizzashopPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
