/**
 * Created by Tudor on 6/20/2017.
 */
import {Component, OnInit} from '@angular/core';
import {Pizza} from '../shared/pizza';
import {PizzaService} from "../shared/pizza.service";
import {Router} from "@angular/router";


@Component({
  moduleId: module.id,
  selector: 'pizza-list',
  templateUrl: './pizzashop2010-pizzas.component.html',
  styleUrls: ['./pizzashop2010-pizzas.component.css'],
})
export class PizzasListComponent implements OnInit {
  errorMessage: string;
  pizzas: Pizza[];
  selectedPizza: Pizza;

  constructor(private pizzaService: PizzaService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getPizzas();
  }

  getPizzas() {
    this.pizzaService.getPizzas()
      .subscribe(
        pizzas => this.pizzas = pizzas,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(pizza: Pizza): void {
    this.selectedPizza = pizza;
    this.gotoDetail();
  }

  gotoDetail(): void {
    this.router.navigate(['/pizzashop2010/pizza/', this.selectedPizza.id]);
  }


  update(pizza: Pizza, price: number): void {
    this.pizzaService.update(pizza.id,price).subscribe();
  }

  updateAll(extraPrice: number): void{
    this.pizzaService.updateAll(extraPrice).subscribe();
  }

}
