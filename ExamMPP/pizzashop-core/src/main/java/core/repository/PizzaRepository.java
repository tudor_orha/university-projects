package core.repository;

import core.model.Pizza;
import core.model.Ingredient;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by Tudor on 6/20/2017.
 */
public interface PizzaRepository extends MainRepository<Pizza, Long> {
    @Query("select distinct p from Pizza p")
    @EntityGraph(value = "pizzaWithIngredients", type = EntityGraph.EntityGraphType.LOAD)
    List<Pizza> findAllWithIngredientsGraph();

    //@Query("update Pizza SET price = price + 1 WHERE price > 0")
    //@EntityGraph(value = "pizzaWithIngredients", type = EntityGraph.EntityGraphType.LOAD)
    //List<Pizza> updateAllWithGraph();

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Pizza SET price =:increment + price where 1=1")
    public void updateAll(@Param("increment") float increment);

    //@Modifying(clearAutomatically = true)
    //@Query("update RssFeedEntry feedEntry set feedEntry.read =:isRead where feedEntry.id =:entryId")
    //void markEntryAsRead(@Param("entryId") Long rssFeedEntryId, @Param("isRead") boolean isRead);
}
