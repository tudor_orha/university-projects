package core.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Tudor on 6/20/2017.
 */
@Entity
@Table(name = "ingredient")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Ingredient extends BaseEntity<Long> {
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "pizza_id")
    private Pizza pizza;

    @Override
    public String toString() {
        return name;
    }
}
