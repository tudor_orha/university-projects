package core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Tudor on 6/20/2017.
 */
@Entity
@Table(name = "pizza")
@NamedEntityGraphs({
        @NamedEntityGraph(name = "pizzaWithIngredients", attributeNodes = {
                @NamedAttributeNode(value = "pizzaIngredients", subgraph = "pizzaIngredientsGraph")
        }, subgraphs = {
                @NamedSubgraph(name = "pizzaIngredientsGraph", attributeNodes = {
                        @NamedAttributeNode(value = "pizza")
                })
        }
        )
})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Pizza extends BaseEntity<Long> {

    @Column(name = "name", nullable = false,  unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price", nullable = false)
    private float price;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pizza")
    private Set<Ingredient> pizzaIngredients = new HashSet<>();


}
