package core.service;

import core.model.Pizza;
import core.repository.PizzaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Tudor on 6/20/2017.
 */
@Service
public class PizzaServiceImpl implements PizzaService {
    private static final Logger log = LoggerFactory.getLogger(PizzaServiceImpl.class);

    @Autowired
    private PizzaRepository pizzaRepository;

    @Override
    public Pizza createPizza(String name, String description, float price) {
        log.trace("createPizza: name={}, description={}, price={}", name, description, price);

        Pizza pizza = Pizza.builder()
                .name(name)
                .description(description)
                .price(price)
                .build();
        pizza = pizzaRepository.save(pizza);

        log.trace("createPizza: pizza={}", pizza);

        return pizza;
    }


    @Override
    public List<Pizza> findAll() {
        log.trace("findAll");
        //List<Pizza> pizzas = pizzaRepository.findAll();
        List<Pizza> pizzas = pizzaRepository.findAllWithIngredientsGraph();
        log.trace("findAll: pizzas={}", pizzas);
        return pizzas;
    }

    @Override
    @Transactional
    public Pizza updatePizza(Long id, float price) {
        log.trace("updatePizza: id={}, price={}", id, price);
        Pizza pizza = pizzaRepository.findOne(id);
        pizza.setPrice(price);
        log.trace("updatePizza: pizza={}", pizza);
        return pizza;
    }

    @Override
    public List<Pizza> updateAllPizzas(float price) {
        log.trace("updateAllPizzas: price={}", price);
        pizzaRepository.updateAll(price);
        List<Pizza> pizzas = pizzaRepository.findAll();
        //pizzas.forEach(p->p.setPrice(p.getPrice()+price));
        log.trace("updateAllPizzas: pizzas={}", pizzas);
        return pizzas;
    }
}
