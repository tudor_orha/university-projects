package core.service;

import core.model.Pizza;

import java.util.List;

/**
 * Created by Tudor on 6/20/2017.
 */
public interface PizzaService {
    Pizza createPizza(String name, String description, float price);

    List<Pizza> findAll();

    Pizza updatePizza(Long id, float price);

    public List<Pizza> updateAllPizzas(float price);
}