package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        boolean eroare = false;
        int nrLinii = 0;
        Scanner fileScanner = new Scanner(new File("program1.txt"));
        while (fileScanner.hasNextLine()) {
            nrLinii++;
            String line = fileScanner.nextLine();
            Scanner lineScanner = new Scanner(line);
            String element;
            int contor = 0;
            while (lineScanner.hasNext()) {
                element = lineScanner.next();
                contor++;
                if ((element.length() > 8 && !Utils.codificationTable.containsKey(element) && !Utils.isNumeric(element)) ||
                        (!Utils.isValidJavaIdentifier(element) && !Utils.codificationTable.containsKey(element) && !Utils.isNumeric(element))) {
                    System.out.println("EROARE!!!!!!!!!! - identificator invalid: " + element + " la linia: " + nrLinii + ", elementul cu numarul: " + contor);
                    eroare = true;
                }

                if (Utils.codificationTable.containsKey(element))
                    Utils.Fip.add(new Pereche(Utils.codificationTable.get(element), -1));
                else {
                    if (Utils.isNumeric(element)) {
                        Utils.TSConstante.add(new Pereche(Utils.TSConstante.size() + 1, Double.parseDouble(element)));
                        Utils.Fip.add(new Pereche(1, Utils.TSConstante.size()));
                    } else {
                        Utils.TSIdentificator.add(new Pereche(Utils.TSIdentificator.size() + 1, element));
                        Utils.Fip.add(new Pereche(0, (Utils.TSIdentificator.size())));
                    }
                }
            }
            lineScanner.close();
        }
        fileScanner.close();
        if (eroare == false) {
            System.out.println("FIP");
            for (Pereche per : Utils.Fip) {
                per.print();
            }

            System.out.println("IDENTIFICATORI");

            for (Pereche str : Utils.TSIdentificator)
                str.print();


            System.out.println("CONSTANTE");

            for (Pereche str : Utils.TSConstante)
                str.print();
        }
    }
}
