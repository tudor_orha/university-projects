package com.company;

/**
 * Created by alex on 10/11/16.
 */
public class Tranzitie {

    private final String valoare;
    private final String stareFinala;
    private final String stareInitiala;

    public Tranzitie(String stareInitiala, String stareFinala, String valoare) {
        this.stareInitiala = stareInitiala;
        this.stareFinala = stareFinala;
        this.valoare = valoare;
    }

    public String getValoare() {
        return valoare;
    }

    public String getStareFinala() {
        return stareFinala;
    }

    public String getStareInitiala() {
        return stareInitiala;
    }

    @Override
    public String toString() {
        return stareInitiala + " " + stareFinala + " " + valoare;
    }
}
