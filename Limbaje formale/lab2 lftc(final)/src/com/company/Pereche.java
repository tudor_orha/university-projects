package com.company;


public class Pereche<T> implements Comparable<Pereche<T>>{
    int index;
    T element;

    public Pereche(int index, T element){
        this.index = index;
        this.element = element;
    }

    public void print() {
        System.out.println(index + " " + element);
    }


    //@Override
    public boolean equals(Pereche p){
        if(this.index == p.index && this.element == p.element)
            return  true;

        return false;

    }
    @Override
    public int compareTo(Pereche<T> o) {
        if(o.element instanceof  Double)
            return ((Double) this.element).compareTo((Double) o.element);
        else if(o.element instanceof  Integer)
            return ((Integer) this.element).compareTo((Integer) o.element);
        else
            return ((String) this.element).compareTo((String) o.element);
    }
}
