package com.company;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by Alex on 10/23/2016.
 */
public class Utils {
    public static boolean isNumeric(String str) throws IOException {
        AutomatFinit af = new AutomatFinit("auto.txt");
       // return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
        return af.verifica(str);
    }

    public static Map<String,Integer> codificationTable = new HashMap<>();
    public static List<Pereche> Fip = new ArrayList<>();
    static {
        codificationTable.put("identifier",0);
        codificationTable.put("constant",1);
        codificationTable.put("array",2);
        codificationTable.put("intreg",3);
        codificationTable.put("real",4);
        codificationTable.put("bool",5);
        codificationTable.put("constant",6);
        codificationTable.put("citeste",7);
        codificationTable.put("scrie",8);
        codificationTable.put("daca",9);
        codificationTable.put("altfel",10);
        codificationTable.put("pentru",11);
        codificationTable.put("catTimp",12);
        codificationTable.put("=",13);
        codificationTable.put("si",14);
        codificationTable.put("sau",15);
        codificationTable.put("<>",16);
        codificationTable.put("_",17);
        codificationTable.put(";",18);
        codificationTable.put("+",19);
        codificationTable.put("*",20);
        codificationTable.put("/",21);
        codificationTable.put("%",22);
        codificationTable.put("(",23);
        codificationTable.put(")",24);
        codificationTable.put("{",25);
        codificationTable.put("}",26);
        codificationTable.put("[",27);
        codificationTable.put("]",28);
        codificationTable.put("<",29);
        codificationTable.put("?",30);
        codificationTable.put("==",31);
        codificationTable.put("<=",32);
        codificationTable.put(">=",33);
        codificationTable.put("incepe",34);
        codificationTable.put("termina",35);
        codificationTable.put("var",36);
        codificationTable.put("program",37);
        codificationTable.put("-",38);
        codificationTable.put(">",39);
        codificationTable.put("<",40);
        codificationTable.put("executa",41);
        codificationTable.put("to",42);
    }
    public static List<String> list = new  ArrayList<>();
    public static TreeSet<Pereche> TSConstante = new TreeSet<>();
    public static TreeSet<Pereche> TSIdentificator = new TreeSet<>();
    public final static boolean isValidJavaIdentifier(String s) throws IOException {
        // an empty or null string cannot be a valid identifier
//        if (s == null || s.length() == 0)
//        {
//            return false;
//        }
//
//        char[] c = s.toCharArray();
//        if (!Character.isJavaIdentifierStart(c[0]))
//        {
//            return false;
//        }
//
//        for (int i = 1; i < c.length; i++)
//        {
//            if (!Character.isJavaIdentifierPart(c[i]))
//            {
//                return false;
//            }
//        }
//        return true;
        AutomatFinit af = new AutomatFinit("automatIdentificator.txt");
        return  af.verifica(s);
    }


}
