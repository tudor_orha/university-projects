package com.company;


public class PerecheFip implements Comparable<PerecheFip> {

    int codAtom;
    int pozTS;

    public PerecheFip(int codAtom, int pozTS){
        this.codAtom = codAtom;
        this.pozTS = pozTS;
    }

    public void print() {
        System.out.println(codAtom + " " + pozTS);
    }


    @Override
    public int compareTo(PerecheFip per) {
        if (per.codAtom == this.codAtom && per.pozTS == this.pozTS)
            return 0;
        else if (per.codAtom < this.codAtom && per.pozTS < this.pozTS)
            return -1;
        else
            return 1;
    }
}
