package com.company;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by alex on 10/11/16.
 */
public class AutomatFinit {
    private List<String> stari = new ArrayList<>();
    private List<Tranzitie> tranzitii = new ArrayList<>();
    private String stareInitiala;
    private List<String> stariFinale = new ArrayList<>();
    private HashSet<String> alfabet = new HashSet<>();

    public AutomatFinit(String filename) throws FileNotFoundException {
        readFile(filename);
    }

    private void readFile(String filename) throws FileNotFoundException {
        Scanner s = new Scanner(new File(filename));
        String[] line;

        // citim starile
        line = s.nextLine().split(" ");
        Collections.addAll(stari, line);

        // citim care in ce stare se afla
        List<Integer> statuses = new ArrayList<>();
        line = s.nextLine().split(" ");
        for (String status : line) {
            statuses.add(Integer.parseInt(status));
        }

        // setam starea initiala si starile finale
        for (int i = 0; i < stari.size(); i++) {
            if (statuses.get(i) == 1) {
                stareInitiala = stari.get(i);
            } else if (statuses.get(i) == 2) {
                stariFinale.add(stari.get(i));
            }
        }

        // citim nrTranzitii
        int nrTranzitii = Integer.parseInt(s.nextLine());
        // citim tranzitiile
        for (int i = 0; i < nrTranzitii; i++) {
            line = s.nextLine().split(" ");
            tranzitii.add(new Tranzitie(line[0], line[1], line[2]));
            alfabet.add(line[2]);
        }
    }

    boolean verifica(String secventa) throws IOException {

        boolean este = verificaSecventa(secventa);
        return este;
    }

    private boolean verificaSecventa(String secventa) {
        // plecam de la starea initiala
        String starePlecare = stareInitiala;
        boolean este = true;
        String stareFinala = "";

        // iteram pana prefixul nu mai coincine cu nici o tranzitie valida
        for (int i = 0; i < secventa.length() && este; ++i) {
            // lucam caracter cu caracter pana nu mai coincide nimic
            String character = secventa.substring(i, i+1), next = "";

            // vedem daca din starePlecare putem ajunge undeva cu valoarea caracter din prefix
            for (Tranzitie tranzitie : tranzitii) {
                if (Objects.equals(tranzitie.getStareInitiala(), starePlecare) && Objects.equals(tranzitie.getValoare(), character)) {
                    next = tranzitie.getStareFinala();
                    break;
                }
            }
            // daca nu am gasit nici o stare urmatoare ne oprim
            if (Objects.equals(next, "")) este = false;
            // daca prefixul s-a terminat si ultimul caracter este stare finala, ne oprim
//            System.out.println(secventa.length() - 1);

            if (stariFinale.contains(next) && i == secventa.length() - 1) {este = true;stareFinala = next;break;}
            // trecem la urmatoarea starePlecare si tinem minte stareFinala
            starePlecare = next;stareFinala = next;
        }
        // verificam daca stareFinala este intradevar stare finala
        if (!stariFinale.contains(stareFinala))
            este = false;
        return este;
    }
}
