

/**
 * Created by Tudor on 11/24/2017.
 */
public class Tuple<X, Y> implements Comparable<Tuple> {
    public final X x;
    public final Y y;
    public Tuple(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString(){
        return x + "-" + y;
    }

    //@Override
    public boolean equals(Tuple t) {
        if (this.x == t.x && this.y == t.y)
            return true;
        return false;
    }

    @Override
    public int compareTo(Tuple t) {
        if(t.x instanceof  Double)
            return ((Double) this.x).compareTo((Double) t.x);
        else if(t.x instanceof  Integer)
            return ((Integer) this.x).compareTo((Integer) t.x);
        else
            return ((String) (this.x)).compareTo((String) t.x);
    }
}
