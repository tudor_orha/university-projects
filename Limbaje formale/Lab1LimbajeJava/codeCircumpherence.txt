#include <iostream>

using namespace std;

int main() {
	float pi = 3.14;
	int r;
	cin >> r;
	cout << 2 * pi * r << endl;
	cout << pi * r * r << endl;
}