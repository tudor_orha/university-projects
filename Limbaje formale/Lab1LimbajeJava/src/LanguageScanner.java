
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class LanguageScanner {

    public Map<String,Integer> specsTable = new HashMap<>();
    public List<Tuple> PIF = new ArrayList<>();
    public TreeSet<Tuple> constantsTree = new TreeSet<>();
    public TreeSet<Tuple> identifiersTree = new TreeSet<>();

    LanguageScanner(String fileName) throws FileNotFoundException {

        int lineCounter = 0;

        //Scanning the Language Specifications
        Scanner fileScanner = new Scanner(new File("miniLanguageSpecs.txt"));
        while(fileScanner.hasNextLine()){
            String line = fileScanner.nextLine();
            if(!line.isEmpty()) {
                String[] parts = line.split(" ");
                specsTable.put(parts[0], Integer.parseInt(parts[1]));
            }
        }
        fileScanner.close();

        //Scanning the c++ file
        fileScanner = new Scanner(new File(fileName));
        while (fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            lineCounter++;
            Scanner lineScanner = new Scanner(line);
            String element;
            while(lineScanner.hasNext()){
                element = lineScanner.next();
                String[] parts = {element};
                String[] chars = {";","(",")",","};
                List<String> extraChars = new ArrayList<String>();
                for (char c: element.toCharArray()) {
                    if(c == ',') extraChars.add("" + c);
                    if(c == ';') extraChars.add("" + c);
                }
                parts = element.split("[,\\;]");
                for(String part : parts) {
                    //If found element is part of the language specs
                    if (specsTable.containsKey(part)) {
                        PIF.add(new Tuple(specsTable.get(part),-1));
                    }
                    //If found element is a number
                    else try
                    {
                        Double.parseDouble(part);
                        constantsTree.add(new Tuple(part,constantsTree.size()+1));
                        PIF.add(new Tuple(specsTable.get("CONST"),
                                constantsTree.ceiling(new Tuple(part,0))));
                    }
                    catch(NumberFormatException e)
                    {
                        if(part.length() <= 8) {
                            identifiersTree.add(new Tuple(part, identifiersTree.size() + 1));
                            PIF.add(new Tuple(specsTable.get("ID"),
                                    identifiersTree.ceiling(new Tuple(part,0))));
                        }
                        else throw new FileNotFoundException("ERROR! Identifier length is over 8. (at line "
                                + lineCounter + ").");
                    }
                    try {PIF.add(new Tuple(specsTable.get(extraChars.remove(0)),-1));}catch(IndexOutOfBoundsException e){}
                }
            }
            lineScanner.close();
        }
        fileScanner.close();
    }

}
