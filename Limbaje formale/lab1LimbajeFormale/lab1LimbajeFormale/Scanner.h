#pragma once
#include <string>
#include <unordered_map>

class Scanner{
private:
	std::string fileName;
	std::string programInternalForm;
	std::unordered_map<int, std::string> symbolsTable;
	std::unordered_map<int, int> identifiersTable;
	std::unordered_map<int, int> constantsTable;
public:
	Scanner(std::string fileName);
};