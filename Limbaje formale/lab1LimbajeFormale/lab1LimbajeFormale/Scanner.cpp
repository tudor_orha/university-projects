#include "Scanner.h"
#include <iostream>
#include <fstream>

Scanner::Scanner(std::string fileName) :fileName(fileName), symbolsTable({}), identifiersTable({}), constantsTable({}) {
	std::string line;
	std::ifstream symFile("miniLanguageSpecs.txt");
	if (symFile.is_open()) {
		while (std::getline(symFile, line) && line!="") {
			std::string symbol = line.substr(0, line.find(" "));
			line.erase(0, line.find(" ") + 1);
			std::string idString = line.substr(0, line.find(" "));
			int id = std::stoi(idString, nullptr, 10);
			symbolsTable.insert({ id,symbol });
		}
		symFile.close();
	}
	else std::cout << "Unable to open symbolTableFile\n";
}