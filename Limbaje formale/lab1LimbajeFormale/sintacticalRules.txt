program = Begin Statements End

Begin = "#include<iostream>

using namespace std;

int main() {"

End = "return 0;
}"

Statements = Statement | Statement Statements

Statement = Declaration | Assignment | Read | Print | If | While 

Declaration = Datatype ID (,ID) ";" | Datatype Assignment ";"
Datatype = "int" | "double" 
Assignment = ID " = " Expression ";" | Expression ";"

Read = "cin >> " ID ";"
Print = "cout << " Expression ";" | "cout<< " Expression " << endl;"
If = "if(" Condition "){" Statements "}"
While = "while(" Condition "){" Statements "}"

Expression = Operand | Expression Operation Operand | Operand UnaryOperation 
Operation = "+" | "-" | "*" | "/" | "%" | "+=" | "-="
UnaryOperation = "--" | "++"
Operand = ID | CONST

Condition = Expression | Expression Relation Expression
Relation = ">" | ">=" | "<" | ">=" | "==" | "!="


