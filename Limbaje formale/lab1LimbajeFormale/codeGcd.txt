#include<iostream>

using namespace std;

int main() {
	int a, b;
	cin >> a;
	cin >> b;
	if (a < b) {
		int aux = a;
		a = b;
		b = aux;
	}
	while (b > 0) {
		int f = a % b;
		a = b;
		b = f;
	}
	cout << a << endl;
}