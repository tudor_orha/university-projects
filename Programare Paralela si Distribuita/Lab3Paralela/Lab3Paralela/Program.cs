﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace Lab3Paralela
{
    class Program
    {

        static int numberOfThreads = Environment.ProcessorCount;

        static void DoTask(Object state)
        {
        }

        private static void add(int[,] firstMatrix, int[,] secondMatrix, int[,] sumMatix, int nrThread, int nrOfTasks)
        {
            for (int i = nrThread * firstMatrix.GetLength(0) / nrOfTasks;
                i < (1 + nrThread) * firstMatrix.GetLength(0) / nrOfTasks; i++)
                for (int j = 0; j < firstMatrix.GetLength(1); j++)
                    sumMatix[i, j] = firstMatrix[i, j] + secondMatrix[i, j];
        }



        static void Main(string[] args)
        {
            Stopwatch sw;
            int numberOfTasks;
            Console.Write("x = "); String xString = Console.ReadLine();
            Console.Write("y = "); String yString = Console.ReadLine();
            Console.Write("z = "); String zString = Console.ReadLine();
            int x = Int32.Parse(xString);
            int y = Int32.Parse(yString);
            int z = Int32.Parse(zString);
            int[,] matrix = new int[x, y];
            int[,] matrixForSum = new int[x, y];
            int[,] sumMatrix = new int[x, y];
            int[,] matrixForProduct = new int[y, z];
            int[,] productMatrix = new int[x, z];
            Random rnd = new Random();
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    matrix[j, i] = rnd.Next(-100, 101);
                    matrixForSum[j, i] = rnd.Next(-100, 101);
                }
                for (int j = 0; j < z; j++) matrixForProduct[i, j] = rnd.Next(-100, 101);
            }

            
            sw = new Stopwatch();
            sw.Start();
            add(matrix, matrixForSum, sumMatrix, 0, 1);
            sw.Stop();
            Console.WriteLine("Time for no threads: {0}", sw.Elapsed);



            sw = new Stopwatch();
            numberOfTasks = 1000;
            sw.Start();
            //for (int i = 0; i < numberOfTasks; i++)
            //ThreadPool.QueueUserWorkItem(o => add(matrix, matrixForSum, sumMatrix, i, numberOfTasks));
            add(matrix, matrixForSum, sumMatrix, 0, 1);
            sw.Stop();
            Console.WriteLine("Time for threadpool: {0}",sw.Elapsed);
            
        }
    }
}
