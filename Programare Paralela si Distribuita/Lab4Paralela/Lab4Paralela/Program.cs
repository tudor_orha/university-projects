﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lab4Paralela
{
    class Program
    {

        private static Mutex mutex = new Mutex();
        static int activeTasks = 0;

        private class TaskInfo
        {
            public int[,] firstMatrix, secondMatrix, resultMatrix;
            public int nrThread, nrOfTasks;

            public TaskInfo(int[,] firstMatrix, int[,] secondMatrix, int[,] resultMatrix, int nrThread, int nrOfTasks)
            {
                this.firstMatrix = firstMatrix;
                this.secondMatrix = secondMatrix;
                this.resultMatrix = resultMatrix;
                this.nrThread = nrThread;
                this.nrOfTasks = nrOfTasks;
            }
        }

        private static void multiplyWithObject(Object taskInfo)
        {
            TaskInfo ti = (TaskInfo)taskInfo;
            for (int i = ti.nrThread * ti.firstMatrix.GetLength(0) / ti.nrOfTasks;
                i < (1 + ti.nrThread) * ti.firstMatrix.GetLength(0) / ti.nrOfTasks; i++)
            {
                for (int j = 0; j < ti.secondMatrix.GetLength(1); j++)
                {
                    ti.resultMatrix[i, j] = 0;
                    for (int k = 0; k < ti.firstMatrix.GetLength(1); k++)
                        ti.resultMatrix[i, j] += ti.firstMatrix[i, k] * ti.secondMatrix[k, j];
                }
            }
            Interlocked.Decrement(ref activeTasks);
        }

        private static void multiplyParllelWithObject(Object taskInfo)
        {
            TaskInfo ti = (TaskInfo)taskInfo;
            for (int i = ti.nrThread * ti.firstMatrix.GetLength(0) / ti.nrOfTasks;
                i < (1 + ti.nrThread) * ti.firstMatrix.GetLength(0) / ti.nrOfTasks; i++)
            {
                for (int j = 0; j < ti.secondMatrix.GetLength(1); j++)
                {
                    ti.resultMatrix[i, j] = 0;
                    for (int k = 0; k < ti.firstMatrix.GetLength(1); k++)
                        ti.resultMatrix[i, j] += ti.firstMatrix[i, k] * ti.secondMatrix[k, j];
                }
            }
            Interlocked.Decrement(ref activeTasks);
        }

        static void Main(string[] args)
        {
            Stopwatch sw;
            int numberOfTasks;
            Console.Write("x = "); String xString = Console.ReadLine();
            Console.Write("y = "); String yString = Console.ReadLine();
            Console.Write("z = "); String zString = Console.ReadLine();
            Console.Write("t = "); String tString = Console.ReadLine();
            int x = Int32.Parse(xString);
            int y = Int32.Parse(yString);
            int z = Int32.Parse(zString);
            int t = Int32.Parse(tString);
            int[,] matrix1 = new int[x, y];
            int[,] matrix2 = new int[y, z];
            int[,] productMatrix1 = new int[x, z];
            int[,] matrix3 = new int[z, t];
            int[,] productMatrix2 = new int[x, t];
            Random rnd = new Random();
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    matrix1[j, i] = rnd.Next(-100, 101);
                }
                for (int j = 0; j < z; j++) matrix2[i, j] = rnd.Next(-100, 101);
            }
            for (int i = 0; i < z; i++) for (int j = 0; j < t; j++) matrix3[i, j] = rnd.Next(-100, 101);

            sw = new Stopwatch();
            numberOfTasks = 8;
            activeTasks = numberOfTasks*2;
            sw.Start();
            for (int i = 0; i < numberOfTasks; i++)
            {
                TaskInfo ti = new TaskInfo(matrix1, matrix2, productMatrix1, i, numberOfTasks);
                TaskInfo ti2 = new TaskInfo(productMatrix1, matrix3, productMatrix2, i, numberOfTasks);

                //Task.Factory.StartNew(() => multiplyWithObject(ti));
                //Task.Factory.StartNew(() => multiplyWithObject(ti2));

                Task taskA = Task.Factory.StartNew(() => multiplyWithObject(ti));
                //Task taskB = Task.Factory.StartNew(() => multiplyWithObject(ti2));
                Task continuation = taskA.ContinueWith(antecedent => multiplyWithObject(ti2));
            }
            while (activeTasks > 0) { }
            sw.Stop();
            Console.WriteLine("Time for product with continuation: {0}", sw.Elapsed);

            //for (int i = 0; i < x; i++) for (int j = 0; j < t; j++) Console.WriteLine(productMatrix2[i, j]);

            sw = new Stopwatch();
            numberOfTasks = 8;
            activeTasks = numberOfTasks;
            sw.Start();
            for (int i = 0; i < numberOfTasks; i++)
            {
                TaskInfo ti = new TaskInfo(matrix1, matrix2, productMatrix1, i, numberOfTasks);
                Task.Factory.StartNew(() => multiplyWithObject(ti));
            }
            while (activeTasks > 0) { }
            activeTasks = numberOfTasks;
            for (int i = 0; i < numberOfTasks; i++)
            {
                TaskInfo ti2 = new TaskInfo(productMatrix1, matrix3, productMatrix2, i, numberOfTasks);
                Task.Factory.StartNew(() => multiplyWithObject(ti2));
            }
            while (activeTasks > 0) { }
            sw.Stop();
            Console.WriteLine("Time for product with NO continuation: {0}", sw.Elapsed);



            //for (int i = 0; i < x; i++) for (int j = 0; j < t; j++) Console.WriteLine(productMatrix2[i,j]);
        }
    }
}
