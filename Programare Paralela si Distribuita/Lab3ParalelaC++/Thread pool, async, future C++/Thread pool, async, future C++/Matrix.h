#pragma once
#include <vector>


template <typename T>
class Matrix {
private:
	std::vector<T> content;
	unsigned int x, y;
public:
	Matrix(unsigned int x, unsigned int y) : x(x), y(y) { content.resize(x*y); }
	T& operator()(unsigned int x, unsigned int y) {
		if (x >= this->x || y >= this->y)
			throw 0;
		return content[this->y*x + y];
	}
	unsigned int getX() { return this->x; }
	unsigned int getY() { return this->y; }
};