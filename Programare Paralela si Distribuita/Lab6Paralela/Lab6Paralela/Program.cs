﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Lab6Paralela
{
    class Program
    {
        static int nrOfThreads = Environment.ProcessorCount;
        static int activeTasks = 0;

        private class TaskInfo
        {
            public int[] firstPolynom, secondPolynom, resultPolynom;
            public int nrThread, nrOfThreads;

            public TaskInfo(int[] firstPolynom, int[] secondPolynom, int[] resultPolynom, int nrThread, int nrOfThreads)
            {
                this.firstPolynom = firstPolynom;
                this.secondPolynom = secondPolynom;
                this.resultPolynom = resultPolynom;
                this.nrThread = nrThread;
                this.nrOfThreads = nrOfThreads;
            }
        }

        private static void simpleMultiplication(TaskInfo ti)
        {
            for (int i = ti.nrThread * ti.firstPolynom.GetLength(0) / ti.nrOfThreads;
                     i < (1 + ti.nrThread) * ti.firstPolynom.GetLength(0) / ti.nrOfThreads; i++)
            {
                for (int j = 0; j < ti.secondPolynom.GetLength(0); j++)
                {
                    ti.resultPolynom[i + j] += ti.firstPolynom[i] * ti.secondPolynom[j];
                }
            }
            Interlocked.Decrement(ref activeTasks);
        } 

        // Only for same length polynoms
        private static void karatsubaMultiplication(TaskInfo ti)
        {
            if(ti.firstPolynom.GetLength(0) == 1 && ti.secondPolynom.GetLength(0) == 1)
            {
                ti.resultPolynom[0] = ti.firstPolynom[0] * ti.secondPolynom[0];
                return;
            }
            int half = ti.firstPolynom.GetLength(0) / 2 + ti.firstPolynom.GetLength(0) % 2;
            int[] firstPolStart = new int[half];
            int[] firstPolEnd = new int[half-(half+1)%2];
            int[] secondPolStart = new int[half];
            int[] secondPolEnd = new int[half - (half+1)%2];
            int[] firstResult = new int[2*half-(half+1)%2-1];
            int[] secondResult = new int[2*half-(half+1)%2-1];     
            Array.Copy(ti.firstPolynom, 0, firstPolStart, 0, half);
            Array.Copy(ti.firstPolynom, half, firstPolEnd, 0, half - (half + 1) % 2);
            Array.Copy(ti.secondPolynom, 0, secondPolStart, 0, half);
            Array.Copy(ti.secondPolynom, half, secondPolEnd, 0, half - (half + 1) % 2);

            TaskInfo first = new TaskInfo(firstPolStart, secondPolStart, firstResult, ti.nrThread, ti.nrOfThreads);
            karatsubaMultiplication(first);
            TaskInfo second = new TaskInfo(firstPolEnd, secondPolEnd, secondResult, ti.nrThread, ti.nrOfThreads);
            karatsubaMultiplication(second);
            Array.Copy(firstResult,0,ti.resultPolynom,0,firstResult.GetLength(0));

            for(int i = 0; i < firstPolEnd.GetLength(0); i++)
            {
                firstPolStart[i] += firstPolEnd[i];
                secondPolStart[i] += secondPolEnd[i];
            }

            int[] middleResult = new int[firstPolStart.GetLength(0) + secondPolStart.GetLength(0) - 1];
            TaskInfo mid = new TaskInfo(firstPolStart, secondPolStart, middleResult, ti.nrThread, ti.nrOfThreads);
            karatsubaMultiplication(mid);

            for (int i = 0; i < middleResult.GetLength(0); i++)
            {
                ti.resultPolynom[i + half] += middleResult[i] - firstResult[i] - secondResult[i];
            }
            for (int i = 0; i < secondResult.GetLength(0); i++)
            {
                ti.resultPolynom[i + 2 * half] += secondResult[i];
            }

        }

        private static void displayPolynom(int[] polynom)
        {
            for (int i = polynom.GetLength(0) - 1; i > 0; i--)
            {
                Console.Write("{0}x^{1} + ",polynom[i],i);
            }
            Console.Write("{0}\n",polynom[0]);
        }

        static void Main(string[] args)
        {
            Stopwatch sw;
            Console.Write("x = "); String xString = Console.ReadLine();
            Console.Write("y = "); String yString = Console.ReadLine();
            int x = Int32.Parse(xString);
            int y = Int32.Parse(yString);
            int[] firstPolynom  = new int[x];
            int[] secondPolynom = new int[y];
            int[] resultPolynom = new int[x + y - 1];
            Random rnd = new Random();
            for (int i = 0; i < x; i++) firstPolynom[i] = rnd.Next(-100, 101);
            for (int i = 0; i < y; i++) secondPolynom[i] = rnd.Next(-100, 101);
            //for (int i = 0; i < x + y - 1; i++) resultPolynom[i] = 0;

            firstPolynom[0] = 1;
            firstPolynom[1] = 1;
            firstPolynom[2] = 1;
            firstPolynom[3] = 1;
            secondPolynom[0] = 1;
            secondPolynom[1] = 1;
            secondPolynom[2] = 1;
            secondPolynom[2] = 1;

            sw = new Stopwatch();
            TaskInfo ti = new TaskInfo(firstPolynom, secondPolynom, resultPolynom, 0, 1);
            sw.Start();
            //simpleMultiplication(ti);
            karatsubaMultiplication(ti);
            sw.Stop();
            Console.WriteLine("Time for simple Multiplication no threads: {0}", sw.Elapsed);
            //for (int i = 0; i < x + y - 1; i++) resultPolynom[i] = 0;

            displayPolynom(resultPolynom);

            sw = new Stopwatch();
            activeTasks = nrOfThreads;
            sw.Start();
            for (int i = 0; i < nrOfThreads; i++)
            {
                ti = new TaskInfo(firstPolynom, secondPolynom, resultPolynom, i, nrOfThreads);
                Task.Factory.StartNew(() => simpleMultiplication(ti));
                System.Threading.Thread.Sleep(1);
            }
            while (activeTasks > 0) { }
            sw.Stop();
            Console.WriteLine("Time for simple Multiplication with threads: {0}", sw.Elapsed);
           

            // Date de test (x^2 + x + 1)*(x + 1) = x^3 + 2*x^2 + 2*x + 1;
            /*
            firstPolynom[0] = 1;
            firstPolynom[1] = 1;
            secondPolynom[0] = 1;
            secondPolynom[1] = 1;
            secondPolynom[2] = 1;
            for(int i = 0; i < nrOfThreads; i++)
                simpleMultiplication(firstPolynom,secondPolynom,resultPolynom,i);
            displayPolynom(resultPolynom);
            */
        }
    }
}
