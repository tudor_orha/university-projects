// Lab1Paralela.cpp : Defines the entry point for the console application.
//

#include "Matrix.h"
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <time.h>

using namespace std;

void sum(Matrix<int> firstMatrix, Matrix<int> secondMatrix, Matrix<int>& sumMatrix, int nrThread) {
	for (unsigned int i = nrThread * firstMatrix.getX()/8; i < (1 + nrThread) * firstMatrix.getX()/8 ; i++)
		for (unsigned int j = 0; j < firstMatrix.getY(); j++)
			sumMatrix(i, j) = firstMatrix(i, j) + secondMatrix(i, j);
}

void product(Matrix<int> firstMatrix, Matrix<int> secondMatrix, Matrix<int>& productMatrix, int nrThread) {
	for (unsigned int i = nrThread * firstMatrix.getX() / 8; i < (1 + nrThread) * firstMatrix.getX() / 8; i++) {
		for (unsigned int j = 0; j < secondMatrix.getY(); j++) {
			productMatrix(i, j) = 0;
			for (unsigned int k = 0; k < firstMatrix.getY(); k++)
				productMatrix(i, j) += firstMatrix(i, k) * secondMatrix(k, j);
		}
	}
}

int main()
{	
	unsigned int x, y, z;
	cout << "x = "; cin >> x;
	cout << "y = "; cin >> y;
	cout << "z = "; cin >> z;

	//Generate Matrices
	Matrix<int> matrix(x,y), matrixForSum(x,y), sumMatrix(x,y), matrixForProduct(y,z), productMatrix(x,z);
	srand(time(NULL));
	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < y; j++) {
			matrix(i, j) = rand() % 201 - 100;
			matrixForSum(i, j) = rand() % 201 - 100;
		}
	}

	for (unsigned int i = 0; i < y; i++) {
		for (unsigned int j = 0; j < z; j++) {
			matrixForProduct(i, j) = rand() % 201 - 100;
		}
	}

	//Calculate sums and products
	//Sum with threads
	vector<thread> threads;
	clock_t begin = clock();

	for (unsigned int i = 0; i < 8; i++)
			threads.push_back(std::thread(sum, matrix, matrixForSum, std::ref(sumMatrix), i));
	
	for (unsigned int i = 0; i < 8; i++)
		threads[i].join();

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time to calculate the sum with threads: " << elapsed_secs << endl;
	

	//Sum without threads
	begin = clock();
	for (unsigned int i = 0; i < x; i++)
		for (unsigned int j = 0; j < y; j++)
			sumMatrix(i, j) = matrix(i, j) + matrixForSum(i, j);
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time to calculate the sum without threads: " << elapsed_secs << endl;


	//Product with threads
	vector<thread> threads2;
	begin = clock();

	for (unsigned int i = 0; i < 8; i++)
			threads2.push_back(std::thread(product, matrix, matrixForProduct, std::ref(productMatrix), i));

	for (unsigned int i = 0; i < 8; i++)
		threads2[i].join();

	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time to calculate the product with threads: " << elapsed_secs << endl;

	//Product without threads
	begin = clock();
	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < z; j++) {
			productMatrix(i, j) = 0;
			for (unsigned int k = 0; k < y; k++)
				productMatrix(i, j) += matrix(i, k) * matrixForProduct(k, j);
		}
	}
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time to calculate the product without threads: " << elapsed_secs << endl;

	//Display matrices that are added, and the sum
	/*
	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < y; j++)
			cout << matrix(i, j) << " ";
		cout << endl;
	}

	cout << endl;

	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < y; j++)
			cout << matrixForSum(i, j) << " ";
		cout << endl;
	}

	cout << endl;

	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < y; j++)
			cout << sumMatrix(i, j) << " ";
		cout << endl;
	}
	*/

	//Display matrices that are multiplied, and the product
	/*
	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < y; j++)
			cout << matrix(i, j) << " ";
		cout << endl;
	}

	cout << endl;

	for (unsigned int i = 0; i < y; i++) {
		for (unsigned int j = 0; j < z; j++)
			cout << matrixForProduct(i, j) << " ";
		cout << endl;
	}

	cout << endl;

	for (unsigned int i = 0; i < x; i++) {
		for (unsigned int j = 0; j < z; j++)
			cout << productMatrix(i, j) << " ";
		cout << endl;
	}
	*/
	return 0;
}
