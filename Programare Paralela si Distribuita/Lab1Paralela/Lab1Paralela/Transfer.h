#pragma once
#include "Account.h"

class Transfer {
private:
	unsigned int id;
	Account sender;
	Account reciever;
	double ammount;
public:
	Transfer(unsigned int id, Account sender, Account reciever, double ammount) : id(id), sender(sender), reciever(reciever), ammount(ammount) {}
	unsigned int getId() { return this->id; }
	Account getSender() { return this->sender; }
	Account getReciever() { return reciever; }
};