#pragma once
#include <string>
#include <vector>

class Account {
private:
	std::string name;
	double balance;
	std::vector<int> transfersIds;
public:
	Account() {};
	Account(std::string name) : name(name), balance(0), transfersIds({}) {};
	double getBalance() { return this->balance; }
	void changeBalance(double ammount) { this->balance += ammount; }
	std::vector<int> getTransfers() { return this->transfersIds; }
};