// Lab1Paralela.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Account.h"
#include "Transfer.h"
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <atomic>
#include <vector>

using namespace std;

std::mutex mtx;

void transfer(std::vector<Transfer>& transfers,Account& sender, Account& reciever, double sum) {
	static std::atomic_int id;
	const int local_id = id++;
	mtx.lock();
	Transfer t(local_id, sender, reciever, sum);
	sender.changeBalance(-sum);
	reciever.changeBalance(sum);
	transfers.push_back(t);
	mtx.unlock();
}

void consistencyCheck(std::vector<reference_wrapper<Account>>& accounts,std::vector<Transfer>& transfers) {
	int i = 100;
	while (i) {
		mtx.lock();
		for (Account acc : accounts) {
			for (int id : acc.getTransfers()) {
				Account reciever = transfers[id].getReciever();
				bool found = 0;
				for (int idR : reciever.getTransfers()) if (idR == id) found = 1;
				if (found == 0) cout << "CHECK FAILED!";
			}
		}
		mtx.unlock();
		i--;
		std::this_thread::sleep_for(30ms);
	}
}

int main()
{

	/*
	thread t1(transfer, 0, 1, 50);
	for (int i = 0; i < 4; i++) {
		cout << account[i] << ": " << sum[i] << endl;
	}

	t1.join();*/

	std::vector<std::reference_wrapper<Account>> accounts;
	std::vector<Transfer> transfers;
	Account a1("Account1"), a2("Account2"), a3("Account3"), a4("Account4");
	accounts.push_back(a1); accounts.push_back(a2); accounts.push_back(a3); accounts.push_back(a4);
	thread th1(transfer, std::ref(transfers), std::ref(a1), std::ref(a2), 100);
	thread th2(transfer, std::ref(transfers), ref(a3), ref(a2), 100);
	thread th3(transfer, std::ref(transfers), ref(a4), ref(a3), 100);
	thread th4(transfer, std::ref(transfers), ref(a4), ref(a2), 100);
	thread th5(transfer, std::ref(transfers), ref(a1), ref(a4), 100);	

	th1.join();
	th2.join();
	th3.join();
	th4.join();
	th5.join();
	consistencyCheck(ref(accounts),ref(transfers));

	for (int i = 0; i < 4; i++) {
		cout << "Acc" << i + 1 << " : " << accounts[i].get().getBalance() << endl;
	}

	return 0;
}

