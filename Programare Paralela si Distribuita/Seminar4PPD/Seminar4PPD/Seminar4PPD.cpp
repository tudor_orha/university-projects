// Seminar4PPD.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <future>

using namespace std;

void partSumChunks(vector<int> const& a, vector<int> const& b, vector<int>& c, size_t begin, size_t end) {
	for (size_t i = begin; i < end; ++i) {
		c[i] = a[i] + b[i];
	}
}

//Takes longer on nrThreads > SystemThreads, equal otherwise with partSumChunks? Bad caching
void partSumInterlaced(vector<int> const& a, vector<int> const& b, vector<int>& c, size_t nrThreads, size_t t) {
	size_t const n = a.size();
	for (size_t i = t; i < n; i += nrThreads) {
		c[i] = a[i] + b[i];
	}

}

void vectorSum(vector<int> const& a,vector<int> const& b,vector<int>& c,int nrThreads) {
	size_t const n = a.size();
	c.resize(n);
	vector<future<void>> futures;
	futures.reserve(nrThreads);
	for (size_t i = 0; i < nrThreads; ++i) {
		futures.emplace_back(async([&a,&b,&c,i,n,nrThreads]() -> void {
			partSumChunks(a, b, c, i*n/nrThreads, (i+1)*n/nrThreads );
		}));
	}
	for (auto& f : futures) {
		f.get();
	}
}

void generate(vector<int>& v, size_t n) {
	v.clear();
	v.reserve(n);
	for (size_t i = 0; i < n; ++i) {
		v.push_back(rand());
	}
}


void printVector(vector<int> const& v) {
	printf("Vector=");
	for (int val : v) printf(" %d", val);
	printf("\n");
}

bool checkSum(vector<int> const& a,vector<int> const& b,vector<int> const& s) {
	if (a.size() != s.size() || b.size() != s.size()) return false;
	for (size_t i = 0; i < s.size(); ++i) {
		if (s[i] != a[i] + b[i]) return false;
	}
	return true;
}

int main()
{
	unsigned n = 0;
	unsigned nrThreads = 0;
	if (true);
	vector<int> a, b, c;
	generate(a, n);
	generate(b, n);
	vectorSum(a, b, c, nrThreads);
	printf("Result: %d", checkSum(a, b, c));
}

