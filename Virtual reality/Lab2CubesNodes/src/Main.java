import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.math.Quaternion;
import com.jme3.math.FastMath;

import java.util.HashMap;

/** Sample 1 - how to get started with the most simple JME 3 application.
 * Display a blue 3D cube and view from all sides by
 * moving the mouse and pressing the WASD keys. */
public class Main extends SimpleApplication {

    float scale = (float) 1/30;
    HashMap<String, Geometry> scene = new HashMap<String, Geometry>();
    Node head = new Node("head");
    Node neck = new Node("neck");
    Node chest = new Node("chest");

    Node rightUpperArm = new Node("rightUpperArm");
    Node leftUpperArm = new Node("leftUpperArm");
    Node rightForearm = new Node("rightForearm");
    Node leftForearm = new Node("leftForearm");
    Node rightHand = new Node("rightHand");
    Node leftHand = new Node("leftHand");

    Node rightUpperLeg = new Node("rightUpperLeg");
    Node leftUpperLeg = new Node("leftUpperLeg");
    Node rightLowerLeg = new Node("rightLowerLeg");
    Node leftLowerLeg = new Node("leftLowerLeg");
    Node rightFoot = new Node("rightFoot");
    Node leftFoot = new Node("leftFoot");

    public static void main(String[] args) {
        Main app = new Main();
        app.start(); // start the game
    }

    @Override
    public void simpleInitApp() {
        initScene();

        /** Set Relations Between nodes */
        rootNode.attachChild(chest);
        chest.attachChild(neck);
        chest.attachChild(rightUpperArm);
        chest.attachChild(leftUpperArm);
        chest.attachChild(rightUpperLeg);
        chest.attachChild(leftUpperLeg);

        neck.attachChild(head);

        rightUpperArm.attachChild(rightForearm);
        leftUpperArm.attachChild(leftForearm);
        rightForearm.attachChild(rightHand);
        leftForearm.attachChild(leftHand);

        rightUpperLeg.attachChild(rightLowerLeg);
        leftUpperLeg.attachChild(leftLowerLeg);
        rightLowerLeg.attachChild(rightFoot);
        leftLowerLeg.attachChild(leftFoot);

        /** Set distance between nodes and their parents */
        head.setLocalTranslation(0,32*scale,3*scale);
        neck.setLocalTranslation(0,69*scale,0);

        rightUpperArm.setLocalTranslation(-37*scale,58*scale,0);
        leftUpperArm.setLocalTranslation((37-8)*scale,58*scale,0);
        rightForearm.setLocalTranslation(0,-60*scale,0);
        leftForearm.setLocalTranslation((0+8)*scale,-60*scale,0);
        rightHand.setLocalTranslation(0,-46*scale,0);
        leftHand.setLocalTranslation(0,-46*scale,0);

        rightUpperLeg.setLocalTranslation(-16*scale,-58*scale,0);
        leftUpperLeg.setLocalTranslation(16*scale,-58*scale,0);
        rightLowerLeg.setLocalTranslation(0,-72*scale,0);
        leftLowerLeg.setLocalTranslation(0,-72*scale,0);
        rightFoot.setLocalTranslation(0,-68*scale,-15*scale);
        leftFoot.setLocalTranslation(0,-68*scale,-15*scale);

        /** Attach the boxes to the nodes */
        head.attachChild(scene.get("Head"));
        neck.attachChild(scene.get("Neck"));
        chest.attachChild(scene.get("Chest"));

        rightUpperArm.attachChild(scene.get("RightUpperArm"));
        leftUpperArm.attachChild(scene.get("LeftUpperArm"));
        rightForearm.attachChild(scene.get("RightForearm"));
        leftForearm.attachChild(scene.get("LeftForearm"));
        rightHand.attachChild(scene.get("RightHand"));
        leftHand.attachChild(scene.get("LeftHand"));

        rightUpperLeg.attachChild(scene.get("RightUpperLeg"));
        leftUpperLeg.attachChild(scene.get("LeftUpperLeg"));
        rightLowerLeg.attachChild(scene.get("RightLowerLeg"));
        leftLowerLeg.attachChild(scene.get("LeftLowerLeg"));
        rightFoot.attachChild(scene.get("RightFoot"));
        leftFoot.attachChild(scene.get("LeftFoot"));


    }

    float time = 0;
    @Override
    public void simpleUpdate(float tpf) {

        time += tpf;
        float sin = (float) Math.sin(time);
        Quaternion q1 = new Quaternion();

        head.setLocalRotation(q1.fromAngleAxis(sin * 45 * FastMath.DEG_TO_RAD, new Vector3f(1, 0, 0)));
        neck.setLocalRotation(q1.fromAngleAxis(sin * 90 * FastMath.DEG_TO_RAD, new Vector3f(0, 1, 0)));
        rightUpperArm.setLocalRotation(q1.fromAngleAxis(sin * 90 * FastMath.DEG_TO_RAD, new Vector3f(1, 1, 0)));
        leftUpperArm.setLocalRotation(q1.fromAngleAxis(Math.abs(sin) * 45 * FastMath.DEG_TO_RAD, new Vector3f(0, 0, 1)));
        rightForearm.setLocalRotation(q1.fromAngleNormalAxis(-Math.abs(sin) * 145 * FastMath.DEG_TO_RAD, new Vector3f(1, 0, 0)));
        //rightHand.setLocalRotation(q1.fromAngleNormalAxis(-Math.abs(sin) * 145 * FastMath.DEG_TO_RAD, new Vector3f(0, 0, 1)));

        //leftUpperLeg.setLocalRotation(q1.fromAngleNormalAxis(sin* 90 * FastMath.DEG_TO_RAD, new Vector3f(0, 1, 0)));


        rightUpperLeg.setLocalRotation(q1.fromAngleAxis(sin * 90 * FastMath.DEG_TO_RAD, new Vector3f(1, 0, 0)));
        rightLowerLeg.setLocalRotation(q1.fromAngleAxis(Math.abs(sin) * 135 * FastMath.DEG_TO_RAD, new Vector3f(1, 0, 0)));
        leftFoot.setLocalRotation(q1.fromAngleAxis(sin * 22.5f * FastMath.DEG_TO_RAD, new Vector3f(0, 1, 0)));
        rightFoot.setLocalRotation(q1.fromAngleAxis(sin * 22.5f * FastMath.DEG_TO_RAD, new Vector3f(1, 0, 0)));
    }


    private void initScene() {
        flyCam.setMoveSpeed(20);
        cam.setLocation(new Vector3f(0,0,20));

        Material blue = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
        blue.setColor("Color", ColorRGBA.Blue);
        Material red = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
        red.setColor("Color", ColorRGBA.Red);
        Material green = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
        green.setColor("Color", ColorRGBA.Green);

        //14,21,17
        Geometry headGeometry = new Geometry("headGeometry", new Box(14*scale,21*scale,17*scale));
        headGeometry.setMaterial(green);
        this.scene.put("Head", headGeometry);

        //11,11,12
        Geometry neckGeometry = new Geometry("neckGeometry", new Box(11*scale,11*scale,12*scale));
        neckGeometry.setMaterial(red);
        this.scene.put("Neck", neckGeometry);

        //29,58,15
        Geometry chestGeometry = new Geometry("chestGeometry", new Box(29*scale,58*scale,15*scale));
        chestGeometry.setMaterial(blue);
        this.scene.put("Chest", chestGeometry);


        //8,30,10
        Geometry rightUpperArmGeometry = new Geometry("upperArmGeometry", new Box(
                new Vector3f(0,-30*scale,0),8*scale,30*scale,10*scale));
        Geometry leftUpperArmGeometry = new Geometry("upperArmGeometry", new Box(
                new Vector3f(8*scale,-30*scale,0),8*scale,30*scale,10*scale));
        rightUpperArmGeometry.setMaterial(red);
        leftUpperArmGeometry.setMaterial(red);
        this.scene.put("RightUpperArm", rightUpperArmGeometry);
        this.scene.put("LeftUpperArm", leftUpperArmGeometry);

        //8,23,8
        Geometry rightForearmGeometry = new Geometry("forearmGeometry", new Box(
                new Vector3f(0,-23*scale,0),8*scale,23*scale,8*scale));
        rightForearmGeometry.setMaterial(green);
        Geometry leftForearmGeometry = rightForearmGeometry.clone();
        this.scene.put("RightForearm", rightForearmGeometry);
        this.scene.put("LeftForearm", leftForearmGeometry);

        //5,10,9
        Geometry rightHandGeometry = new Geometry("handGeometry", new Box(
                new Vector3f(0,-10*scale,0),5*scale,10*scale,9*scale));
        rightHandGeometry.setMaterial(blue);
        Geometry leftHandGeometry = rightHandGeometry.clone();
        this.scene.put("RightHand", rightHandGeometry);
        this.scene.put("LeftHand", leftHandGeometry);


        //13,36,15
        Geometry rightUpperLegGeometry = new Geometry("upperLegGeometry", new Box(
                new Vector3f(0,-36*scale,0),13*scale,36*scale,15*scale));
        rightUpperLegGeometry.setMaterial(red);
        Geometry leftUpperLegGeometry = rightUpperLegGeometry.clone();
        this.scene.put("RightUpperLeg", rightUpperLegGeometry);
        this.scene.put("LeftUpperLeg", leftUpperLegGeometry);

        //10,34,12
        Geometry rightLowerLegGeometry = new Geometry("lowerLegGeometry", new Box(
                new Vector3f(0,-34*scale,0),13*scale,34*scale,15*scale));
        rightLowerLegGeometry.setMaterial(green);
        Geometry leftLowerLegGeometry = rightLowerLegGeometry.clone();
        this.scene.put("RightLowerLeg", rightLowerLegGeometry);
        this.scene.put("LeftLowerLeg", leftLowerLegGeometry);

        //10,5,24
        Geometry rightFootGeometry = new Geometry("footGeometry", new Box(
                new Vector3f(0,-5*scale,22*scale),13*scale,5*scale,24*scale));
        rightFootGeometry.setMaterial(blue);
        Geometry leftFootGeometry = rightFootGeometry.clone();
        this.scene.put("RightFoot", rightFootGeometry);
        this.scene.put("LeftFoot", leftFootGeometry);

    }
}
