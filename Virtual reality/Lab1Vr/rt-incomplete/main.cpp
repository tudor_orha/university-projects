#include <cmath>
#include <iostream>
#include <string>
#include <ctime>
#include <windows.h>
#include <thread>

#include "Vector.hpp"
#include "Line.hpp"
#include "Geometry.hpp"
#include "Sphere.hpp"
#include "Image.hpp"
#include "Color.hpp"
#include "Intersection.hpp"
#include "Material.hpp"

#include "Scene.hpp"

//RAYTRACER SEARCH for intersection

using namespace std;
using namespace rt;

double imageToViewPlane(int n, int imgSize, double viewPlaneSize) {
	double u = (double)n*viewPlaneSize / (double)imgSize;
	u -= viewPlaneSize / 2;
	return u;
}

const Intersection findFirstIntersection(const Line& ray,
	double minDist, double maxDist) {
	Intersection intersection;

	for (int i = 0; i < geometryCount; i++) {
		Intersection in = scene[i]->getIntersection(ray, minDist, maxDist);
		if (in.valid()) {
			if (!intersection.valid()) {
				intersection = in;
			}
			else if (in.t() < intersection.t()) {
				intersection = in;
			}
		}
	}

	return intersection;
}

void displayPart(int iStart, int iFinish, int imageHeight, Image image, HDC mydc) {
	Vector viewPoint(0, 0, 0);
	Vector viewDirection(0, 0, 1);
	Vector viewUp(0, -1, 0);
	/*
	viewDirection = Vector(1, 0, 0);
	viewUp = Vector( 0, -1, 0);
	viewPoint = Vector(-50,0,0);
	*/

	double frontPlaneDist = 0;
	double backPlaneDist = 1000;
	backPlaneDist = 300;

	double viewPlaneDist = 512;
	double viewPlaneWidth = 1024;
	double viewPlaneHeight = 768;

	Vector viewParallel = viewUp^viewDirection;
	// (-1,0,0)

	viewDirection.normalize();
	viewUp.normalize();
	viewParallel.normalize();

	for (double i = iStart; i < iFinish; i++)
		for (double j = 0; j < imageHeight; j++) {
			image.setPixel(i, j, Color(0, 0, 0));
			Intersection in;
			//Vector hitPoint = Vector(viewPlaneWidth / 2 - i - 1, viewPlaneHeight / 2 - j - 1, viewPlaneDist);
			//viewUp(0,-1,0)
			//direction(0,0,1)
			//Vector hitPoint = Vector(i + 1 - viewPlaneWidth/2, j + 1 - viewPlaneHeight/2, 512);
			//direction(-1,0,0)
			//Vector hitPoint = Vector( -512 , j + 1 - viewPlaneHeight / 2, (i+1-viewPlaneWidth/2));
			//direction(1,0,0)
			//Vector hitPoint = Vector( 512, j + 1 - viewPlaneHeight / 2, -(i + 1 - viewPlaneWidth / 2));
			//direction(0,1,0)
			//Vector hitPoint = Vector(,512, );
			//direction(-0.7,0,0.7);
			//Vector hitPoint = Vector((i + 1 - viewPlaneWidth / 2)*0.7071, (j + 1 - viewPlaneHeight / 2), ((i+1) - viewPlaneWidth/2)*0.7071)
			//	+ Vector(512 * viewDirection._xyz[0], 512 * viewDirection._xyz[1], 512 * viewDirection._xyz[2]);
			//direction general NOT DONE
			Vector hitPoint = Vector((i + 1 - viewPlaneWidth / 2) * viewParallel._xyz[0], (i + 1 - viewPlaneWidth / 2) * viewParallel._xyz[1],
				(i + 1 - viewPlaneWidth / 2) * viewParallel._xyz[2]) +
				Vector((j + 1 - viewPlaneHeight / 2) * viewUp._xyz[0], (j + 1 - viewPlaneHeight / 2) * viewUp._xyz[1],
					(j + 1 - viewPlaneHeight / 2) * viewUp._xyz[2]) +
				Vector(viewPlaneDist * viewDirection._xyz[0], viewPlaneDist * viewDirection._xyz[1], viewPlaneDist * viewDirection._xyz[2]);
			//(512*0.7,-0.7*512,-512*0.7)

			Line ray = Line(viewPoint, hitPoint, 0);
			//cout << ray.dx()._xyz[0] << " " << ray.dx()._xyz[1] << " " << ray.dx()._xyz[2] << endl;
			in = findFirstIntersection(ray, frontPlaneDist, backPlaneDist);
			if (in.valid()) {
				// in.vec() is the point of intersection
				Color color(0,0,0);
				Vector N = in.geometry()->normal(in.vec());
				for (int k = 0; k < lightCount; k++) {
					Vector T = Line(in.vec(), lights[k]->position(), 0).dx();
					Vector R = N*(N*T) * 2 - T;
					Vector E = Vector(0, 0, 0) - ray.dx();

					Material material = in.geometry()->material();
					Light *light = lights[k];

					color += material.ambient() * light->ambient();
					if (N*T > 0) color += material.diffuse() * light->diffuse() * (N * T);
					if (E*R > 0) color += material.specular() * light->specular() * pow(E * R, material.shininess());
					color *= light->intensity();
				}

				image.setPixel(i, j, color);
				SetPixel(mydc, i, j, RGB(color.red() * 255, color.green() * 255, color.blue() * 255));
			}
		}
}



int main() {
	ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);
	HWND myconsole = GetConsoleWindow();
	HDC mydc = GetDC(myconsole);

	clock_t begin = clock();

	int imageWidth = 1024;
	int imageHeight = 768;
	Image image(imageWidth, imageHeight);

	std::vector<std::thread> threads;

	int max = 8;
	for (int i = 0; i < max; i++) {
		threads.push_back(std::thread(displayPart, 0 + i * (imageWidth / max), imageWidth - (max - i - 1)*(imageWidth / max), imageHeight, image, mydc));
	}

	for (int i = 0; i < max; i++) {
		threads[i].join();
	}

	//120 - 1 thread
	//63.736 - 2 threads
	//49 - 3 threads
	//40 - 4 threads
	//36 - 5 threads
	//31 - 7 threads
	//29 - 8 threads
	//30 - 9 threads

	image.store("scene.png");

	for (int i = 0; i < geometryCount; i++) {
		delete scene[i];
	}
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << elapsed_secs;
	//cin >> elapsed_secs;
	ReleaseDC(myconsole, mydc);
	cin.ignore();

	return 0;
}
