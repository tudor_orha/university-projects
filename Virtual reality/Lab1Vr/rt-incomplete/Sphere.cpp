#include "Sphere.hpp"

using namespace rt;

Intersection Sphere::getIntersection(const Line& line, double minDist, double maxDist) {
    Intersection in;
    // ADD CODE HERE
	//Inefficient way to do it, check each point of one sent ray.
	/*
	in = Intersection(false, this, &line, 0);
	for (double i = minDist; i <= maxDist; i += 1) {
		if ((this->_center - line.vec(i)).length() <= this->_radius) {
			in = Intersection(true, this, &line, i);
			return in;
		}
	}*/
	//Efficient way
	in = Intersection(false, this, &line, 0);
	double inc = 2;
	for (double i = minDist; i <= maxDist; i += inc) {
		if ((this->_center - line.vec(i)).length() <= this->_radius) {
			while (inc > 1.0/(2^20)){
				inc /= 2;
				if ((this->_center - line.vec(i - inc)).length() <= this->_radius) i -= inc;
			}

			in = Intersection(true, this, &line, i);
			return in;
		}
	}
	//STOP HERE
    return in;
}

const Vector Sphere::normal(const Vector& vec) const {
    Vector n = vec - _center;
    n.normalize();
    return n;
}
