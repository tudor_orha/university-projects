﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnOnCollision : MonoBehaviour {

    public int score;

    private Rigidbody rb;
    private Vector3 constantVelocity;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        score = 0;
        Vector3 constantVelocity = new Vector3(-1.0f, 1.0f, 1.0f);
        rb.AddForce(constantVelocity);
        //rb.MovePosition(rb.position + rb.velocity * Time.deltaTime);
    }

    private void Update()
    {
        rb.velocity = 10 * (rb.velocity.normalized);
        //if (rb.position.y < -1) Debug.Log("LUL");
    }

    void OnCollisionEnter(Collision col)
    {
        ContactPoint contact = col.contacts[0];
        rb.velocity = Vector3.Reflect(rb.velocity, contact.normal);
        if (col.gameObject.name == "Cube")
        {
            score++;
            Destroy(col.gameObject);
        }

        if (col.gameObject.name == "Player")
        {
            col.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            //rb.AddForce(new Vector3(0, 1000, 0));
        }
    }

    private void OnCollisionStay(Collision col)
    {

        if (col.gameObject.name == "Player")
        {
            col.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    private void OnCollisionExit(Collision col)
    {
        if (col.gameObject.name == "Player")
        {
            col.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

}
