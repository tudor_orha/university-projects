﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour {

    private GameObject sphere;

	// Use this for initialization
	void Start () {
        sphere = GameObject.Find("Sphere");
	}
	
	// Update is called once per frame
	void Update () {
        if( sphere.GetComponent<Rigidbody>().position.y < -1 || GameObject.Find("Sphere").GetComponent<DespawnOnCollision>().score == 15)
        {
            //Application.LoadLevel(Application.loadedLevel);
            SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
        }
    }
}
