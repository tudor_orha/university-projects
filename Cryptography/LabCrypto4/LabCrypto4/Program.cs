﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace LabCrypto4
{
    class Program
    {
        static List<long> trialDivision(long n)
        {
            List<long> primeFactors = new List<long>();
            if (n < 2) return primeFactors;

            while (n % 2 == 0)
            {
                primeFactors.Add(2);
                n /= 2;
            }

            for (int i = 3; i <= n; i += 2)
            {
                while (n % i == 0)
                {
                    primeFactors.Add(i);
                    n /= i;
                }
            }

            return primeFactors;
        }

        static List<long> FermatFactorisation(long n)
        {
            List<long> primeFactors = new List<long>();
            if (n < 2) return primeFactors;

            long t0 = Convert.ToInt64(Math.Sqrt(n));
            /*
            for (int t = t0 + 1; t < n; t++)
            {
                if (Convert.ToInt32(Math.Sqrt(t * t - n)) == Math.Sqrt(t * t - n))
                {
                    primeFactors.Add(t - Convert.ToInt32(Math.Sqrt(t*t- n)));
                    primeFactors.Add(t + Convert.ToInt32(Math.Sqrt(t*t- n)));
                    break;
                }
            }*/

            //4237894723

            for (int k = 2; k < 1500; k++)
            {
                bool ok = false;
                t0 = Convert.ToInt64(Math.Sqrt(k * n));
                for (long t = t0 + 1; t < t0 + 5000; t++)
                {
                    if (Convert.ToInt64(Math.Sqrt(t * t - k * n)) == Math.Sqrt(t * t - k * n))
                    {
                        if ((t - Convert.ToInt64(Math.Sqrt(t * t - k * n))) % k == 0)
                        {
                            primeFactors.Add((t - Convert.ToInt64(Math.Sqrt(t * t - k * n)))/ k);
                            primeFactors.Add(t + Convert.ToInt64(Math.Sqrt(t * t - k * n)));
                        }
                        else
                        {
                            primeFactors.Add(t - Convert.ToInt64(Math.Sqrt(t * t - k * n)));
                            primeFactors.Add((t + Convert.ToInt64(Math.Sqrt(t * t - k * n))) / k);
                        }
                        ok = true;
                        break;
                    }
                }
                if (ok == true) break;
            }

            return primeFactors;
        }

        private static Random random = new Random();
        static void Main(string[] args)
        {
            TimeSpan timeForSimple = TimeSpan.Zero;
            TimeSpan timeForFermats = TimeSpan.Zero;
            int iterations = 10000;
            while (iterations > 0)
            {
               Console.Write("n = "); String nString = Console.ReadLine();
                long n = Int64.Parse(nString);
               // long n = random.Next(1000000,10000000);
                //long n = 141467;

                Stopwatch sw = new Stopwatch();
                sw.Start();
                List<long> result = trialDivision(n);
                timeForSimple += sw.Elapsed;
                sw.Stop();
                Console.WriteLine("Time for basic: {0}", sw.Elapsed);

                sw = new Stopwatch();
                sw.Start();
                List<long> resultF = FermatFactorisation(n);
                sw.Stop();
                timeForFermats += sw.Elapsed;
                Console.WriteLine("Time for Fermat: {0}", sw.Elapsed);

                result.ForEach(Console.WriteLine);
                Console.WriteLine();
                resultF.ForEach(Console.WriteLine);
                iterations--;
            }
            Console.WriteLine("Total time for basic: {0}", timeForSimple);
            Console.WriteLine("Total time for fermat: {0}", timeForFermats);
        }
    }
}
