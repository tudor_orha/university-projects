﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabCrypto3
{
    class Program
    {

        static int gcd(int a,int b)
        {
            if (b == 0) return a;
            else return gcd(b, a % b);
        }

        static int checkIfCongruent(int a,int b,int n)
        {
            //int gcdVar = b % gcd(a, n);
            //if (gcdVar == 0) 
            for (int i = 0; i < n; i++)
            {
                if (((a * i) - b) % n == 0) return i;
            }
            //if ((a - b) % n == 0) return true;
            return -1;
        }

        static void Main(string[] args)
        {
            // ax cong b (mod n)
            int runtimes = 10;
            while (runtimes > 0)
            {
                Console.Write("a = "); String xString = Console.ReadLine();
                Console.Write("b = "); String yString = Console.ReadLine();
                Console.Write("n = "); String zString = Console.ReadLine();
                    int a = Int32.Parse(xString);
                    int b = Int32.Parse(yString);
                    int n = Int32.Parse(zString);
                int congruenceSolution = checkIfCongruent(a, b, n);
                if (congruenceSolution == -1) Console.WriteLine("{0}x = {1} (mod {2}) has no solution", a, b, n);
                else Console.WriteLine("{0}x = {1} (mod {2}) has {3} as a solution", a, b, n, congruenceSolution);
                runtimes--;
            }

        }
    }
}
