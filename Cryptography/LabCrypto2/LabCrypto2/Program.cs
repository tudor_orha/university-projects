﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Numerics;
using System.Globalization;

namespace LabCrypto2
{
    class Program
    {
        private static BigInteger euclidGCD(BigInteger first, BigInteger second)
        {
            /* Check For Proper Input */
            if (first == 0) return second;
            if (second == 0) return first;

            while (true)
            {
                BigInteger aux = first % second;
                if (aux == 0)
                    return second;
                first = second;
                second = aux;
            }
        }
     
        private static BigInteger recursiveLongGCD(BigInteger a, BigInteger b)
        {
            if (b == 0) return a;
            else return recursiveLongGCD(b, a % b);
        }

        private static BigInteger binaryGCD(BigInteger u, BigInteger v)
        {
            int shift;
          
            if (u == 0) return v;
            if (v == 0) return u;

            /* Let shift := lg K, where K is the greatest power of 2
                  dividing both u and v. */
            for (shift = 0; ((u | v) & 1) == 0; ++shift)
            {
                u >>= 1;
                v >>= 1;
            }

            while ((u & 1) == 0)
                u >>= 1;

            /* From here on, u is always odd. */
            do
            {
                /* remove all factors of 2 in v -- they are not common */
                /*   note: v is not zero, so while will terminate */
                while ((v & 1) == 0)  /* Loop X */
                    v >>= 1;

                /* Now u and v are both odd. Swap if necessary so u <= v,
                   then set v = v - u (which is even). For bignums, the
                   swapping is just pointer movement, and the subtraction
                   can be done in-place. */
                if (u > v)
                {
                    BigInteger t = v; v = u; u = t;
                }  // Swap u and v.
                v = v - u;                       // Here v >= u.
            } while (v != 0);

            /* restore common factors of 2 */
            return u << shift;
        }


        private static Random random = new Random();

        private static BigInteger generateNumberWithGivenDigits(int x)
        {
            String number = "";
            number += random.Next(1, 9);
            for (int i = 1; i < x; i++)
            {
                number += random.Next(0, 9);
            }
            return BigInteger.Parse(number, CultureInfo.InvariantCulture);
        }

        static void Main(string[] args)
        {
            Stopwatch sw;
            List<BigInteger> firstNumbers = new List<BigInteger>(), secondNumbers = new List<BigInteger>();
            String firstNumberString = "1234567891011121314151617181920";
            String secondNumberString = "9876543211011121314151617181920";
            BigInteger fstNr = BigInteger.Parse(firstNumberString, CultureInfo.InvariantCulture);
            BigInteger sndNr = BigInteger.Parse(secondNumberString, CultureInfo.InvariantCulture);

            sw = new Stopwatch();
            sw.Start();
            euclidGCD(fstNr,sndNr);
            sw.Stop();
            Console.WriteLine("Time for euclid: {0}", sw.Elapsed);
            Console.WriteLine("Gcd is {0}", euclidGCD(fstNr, sndNr));

            sw = new Stopwatch();
            sw.Start();
            recursiveLongGCD(fstNr, sndNr);
            sw.Stop();
            Console.WriteLine("Time for recursive: {0}", sw.Elapsed);
            Console.WriteLine("Gcd is {0}", recursiveLongGCD(fstNr, sndNr));

            sw = new Stopwatch();
            sw.Start();
            binaryGCD(fstNr, sndNr);
            sw.Stop();
            Console.WriteLine("Time for binary: {0}", sw.Elapsed);
            Console.WriteLine("Gcd is {0}", binaryGCD(fstNr, sndNr));


            for (int nrOfDigits = 1; nrOfDigits <= 50; nrOfDigits++)
            {
                firstNumbers.Clear();
                secondNumbers.Clear();
                for (int iters = 0; iters < 5000; iters++)
                {
                    firstNumbers.Add(generateNumberWithGivenDigits(nrOfDigits));
                    secondNumbers.Add(generateNumberWithGivenDigits(nrOfDigits));
                }
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < firstNumbers.Count; i++)
                {
                    euclidGCD(firstNumbers[i], secondNumbers[i]);
                }
                sw.Stop();
                //Console.WriteLine("Time for euclid with {0} digit(s): {1}", nrOfDigits, sw.Elapsed);

                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < firstNumbers.Count; i++)
                {
                    recursiveLongGCD(firstNumbers[i], secondNumbers[i]);
                }
                sw.Stop();
                //Console.WriteLine("Time for recursive with {0} digit(s): {1}", nrOfDigits, sw.Elapsed);

                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < firstNumbers.Count; i++)
                {
                    binaryGCD(firstNumbers[i], secondNumbers[i]);
                }
                sw.Stop();
                //Console.WriteLine("Time for binary with {0} digit(s): {1}", nrOfDigits, sw.Elapsed);
            }

            /*
            int x = 15, y = 10;
            List<BigInteger> firstNumbers = new List<BigInteger>(), secondNumbers = new List<BigInteger>();
            string[] lines = System.IO.File.ReadAllLines(@".\NumbersForGcd.txt");
            foreach (string line in lines)
            {
                string[] numbers = line.Split(' ');
                firstNumbers.Add(BigInteger.Parse(numbers[0], CultureInfo.InvariantCulture));
                //firstNumbers.Add(Int32.Parse(numbers[0]));
                secondNumbers.Add(Int32.Parse(numbers[1]));
            }

            // the code that you want to measure comes here


            //Console.WriteLine(euclidGCD(x, y));
            //Console.WriteLine(recursiveGCD(x, y));
            for (int iters = 0; iters < 3; iters++)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < firstNumbers.Count; i++)
                {
                    euclidGCD(firstNumbers[i], secondNumbers[i]);
                }
                sw.Stop();
                Console.WriteLine("Elapsed={0}", sw.Elapsed);

                sw.Start();
                for (int i = 0; i < firstNumbers.Count; i++)
                {
                    recursiveLongGCD(firstNumbers[i], secondNumbers[i]);
                }
                sw.Stop();
                
                Console.WriteLine("Elapsed={0}", sw.Elapsed);

            }
            */
        }
    }
}
