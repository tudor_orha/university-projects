﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Documents;
using System.Windows.Controls;

namespace Lab1Cripto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String plainText = richTextBox1.Text;
            String cypheredText = plainText;
            String errorMessage;
            int a = 0, b = 0;
            try
            {
                a = Int32.Parse(textBox1.Text);
                b = Int32.Parse(textBox2.Text);
            }
            catch (System.FormatException)
            {
                errorMessage = "a and b are supposed to be integers!";
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (a % 3 == 0)
            {
                errorMessage = "27 and a are supposed to be co-prime!";
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int[] numerical = new int[plainText.Length];
            int[] encryption = new int[plainText.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                if ((plainText[i] < 'a' || plainText[i] > 'z') && plainText[i] != ' ')
                {
                    errorMessage = "'" + plainText[i] + "' is not a part of the small letters from the english alphabet.";
                    MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (plainText[i] == ' ') numerical[i] = 0;
                else numerical[i] = plainText[i] - 'a' + 1;
                if (plainText[i] == ' ') numerical[i] = 0;
                encryption[i] = (numerical[i] * a + b) % 27;
                StringBuilder sb = new StringBuilder(cypheredText);
                if (encryption[i] == 0) sb[i] = ' ';
                else sb[i] = Convert.ToChar(64 + encryption[i]);
                cypheredText = sb.ToString();
            }
            richTextBox2.Text = cypheredText;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String cypheredText = richTextBox2.Text;
            String plainText = cypheredText;
            String errorMessage;
            int a = 0, b = 0;
            try
            {
                a = Int32.Parse(textBox1.Text);
                b = Int32.Parse(textBox2.Text);
            }
            catch (System.FormatException)
            {
                errorMessage = "a and b are supposed to be integers!";
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (a % 3 == 0)
            {
                errorMessage = "27 and a are supposed to be co-prime!";
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int decryptionA = 0, decryptionB;
            for (int i = 1; i <= 26; i++)
            {
                if (a * i % 27 == 1) decryptionA = i;
            }
            decryptionB = -b * decryptionA;
            do
            {
                decryptionB += 27;
            } while (decryptionB < 0);

            int[] numerical = new int[cypheredText.Length];
            int[] encryption = new int[cypheredText.Length];
            for (int i = 0; i < cypheredText.Length; i++)
            {
                if ((cypheredText[i] < 'A' || cypheredText[i] > 'Z') && cypheredText[i] != ' ')
                {
                    errorMessage = "'" + plainText[i] + "' is not a part of the uppercase letters from the english alphabet.";
                    MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (cypheredText[i] == ' ') encryption[i] = 0;
                else encryption[i] = cypheredText[i] - 'A' + 1;
                numerical[i] = (encryption[i] * decryptionA + decryptionB) % 27;
                StringBuilder sb = new StringBuilder(plainText);
                if (numerical[i] == 0) sb[i] = ' ';
                else sb[i] = Convert.ToChar(96 + numerical[i]);
                plainText = sb.ToString();
            }
            richTextBox1.Text = plainText;
        }
    }
}
