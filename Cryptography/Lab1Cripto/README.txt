Lab1 � Affine cypher

How to encrypt text:
�	Into the Plain Text Box, add a text containing only small letters and spaces ( abc�xyz, � �).
�	Into the small text box followed by �a:� you must add an integer that�s not a multiple of 3. ( ex: 1,2,4,5,7,8�).
�	Into the small text box followed by �b:� add any integer.
�	Click the button �Encrypt� and the result should be displayed in the Cyphered Text box with big letters and spaces ( ABC�XYZ, � �).

How to decrypt text:
�	Into the Cyphered Text Box, add a text containing only big letters and spaces ( ABC�XYZ , � �).
�	Into the small text box followed by �a:� you must add an integer that�s not a multiple of 3. (ex: 1,2,4,5,7,8�).
�	Into the small text box followed by �b:� add any integer.
�	Click the button �Decrypt� and the result should be displayed in the Plain Text box with small letter and spaces ( abc�xyz, � �).


 	
