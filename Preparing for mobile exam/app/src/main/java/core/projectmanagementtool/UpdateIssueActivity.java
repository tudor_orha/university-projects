package core.projectmanagementtool;

import core.projectmanagementtool.model.Seat;
import core.projectmanagementtool.model.Issue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;


public class UpdateIssueActivity extends AppCompatActivity {

    Seat currentSeat;
    Issue currentIssue;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_issue);
        String stringCar = getIntent().getStringExtra("Seat");
        try {
            JSONObject jsonCar = new JSONObject(stringCar);
            currentSeat = new Seat(
                    Integer.parseInt(jsonCar.get("id").toString()),
                    jsonCar.get("name").toString(),
                    jsonCar.get("type").toString(),
                    jsonCar.get("status").toString()
            );
            position = getIntent().getIntExtra("Position",0);
            final EditText nameField = (EditText) findViewById(R.id.editTextName);
            final EditText sprintField = (EditText) findViewById(R.id.editTextPassword);
            nameField.setText(currentSeat.getName());
            sprintField.setText(currentSeat.getType());
        }catch(org.json.JSONException e){}
    }

    public void updateIssue(View button){
        final EditText nameField = (EditText) findViewById(R.id.editTextName);
        final EditText sprintField = (EditText) findViewById(R.id.editTextPassword);
        String name = nameField.getText().toString();
        String sprint = sprintField.getText().toString();
        this.currentIssue.setName(name);
        this.currentIssue.setSprint(sprint);

        //FirebaseDatabase.getInstance().getReference().child("issues").child();

        String jsonIssue = currentIssue.toJSON();
        Intent intent = new Intent();
        intent.putExtra("Issue", jsonIssue).putExtra("Position",position);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void deleteIssue(View button){
        //final EditText nameField = (EditText) findViewById(R.id.editTextName);
        //final EditText sprintField = (EditText) findViewById(R.id.editTextPassword);
        //String name = nameField.getText().toString();
        //String sprint = sprintField.getText().toString();
        //this.currentSeat.setName(name);
        //this.currentSeat.setSprint(sprint);

        //FirebaseDatabase.getInstance().getReference().child("issues").child();

        currentIssue = new Issue("name","sprint");
        String jsonIssue = currentIssue.toJSON();
        Intent intent = new Intent();

        intent.putExtra("Operation", "Delete").putExtra("Position",position);
        setResult(RESULT_OK, intent);
        finish();
    }

}
