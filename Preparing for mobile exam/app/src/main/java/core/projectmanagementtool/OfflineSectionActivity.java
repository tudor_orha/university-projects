package core.projectmanagementtool;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class OfflineSectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_section);
    }

    public void reserveSeat(View view) {
        Intent intent = new Intent(OfflineSectionActivity.this,AddCarActivity.class);
        startActivity(intent);
    }

    public void goToOnlinePage(View view) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            Intent intent = new Intent(OfflineSectionActivity.this, ListOfflineActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Not connected to a network. Can't buy cars!",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void goToListPage(View view){
        Intent intent = new Intent(OfflineSectionActivity.this,ListOfflineActivity.class);
        startActivity(intent);
    }
}
