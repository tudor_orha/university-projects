package core.projectmanagementtool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AdminSectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_section);
    }

    public void goToListConfirmedPage(View view) {
        Intent intent = new Intent(AdminSectionActivity.this,ListConfirmedSeatsActivity.class);
        startActivity(intent);
    }

    public void goToListTakenPage(View view){
        Intent intent = new Intent(AdminSectionActivity.this,ListTakenSeatsActivity.class);
        startActivity(intent);
    }
}
