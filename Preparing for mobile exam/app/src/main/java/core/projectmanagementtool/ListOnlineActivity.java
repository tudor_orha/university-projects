package core.projectmanagementtool;

import core.projectmanagementtool.model.Issue;
import core.projectmanagementtool.model.Seat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ListOnlineActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    List<Seat> seatsArray;
    List<Issue> issuesArray;
    ArrayAdapter adapter;
    ListView listView;
    //Issue[] issuesArray = {new Issue("Issue1","Sprint1"),new Issue("Issue2","Sprint1")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_online);
        requestQueue = Volley.newRequestQueue(this);
        seatsArray = new ArrayList<>(5);
        issuesArray = new ArrayList<Issue>(5);
        adapter = new ArrayAdapter<Seat>(this,
                R.layout.activity_listview, seatsArray);
        listView = (ListView) findViewById(R.id.listOfIssues);
        this.loadData();

        ListView listView = (ListView) findViewById(R.id.listOfIssues);


        listView.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>adapter,View v, int position, long id){
                //Object item = adapter.getItemAtPosition(position);
                String jsonCar = seatsArray.get(position).toJSONString();
                Intent intent = new Intent(ListOnlineActivity.this,UpdateIssueActivity.class)
                        .putExtra("Seat", jsonCar)
                        .putExtra("Position", position);
                startActivityForResult(intent,1);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String operation = data.getStringExtra("Operation");
                if (operation.equals("Delete")) {
                    int position = data.getIntExtra("Position", 0);
                    JSONObject params = seatsArray.get(position).toJSONObject();

                    Log.v("Confirmin reserved seat", "pre confirm with values " + seatsArray.get(position).getId());
                    String url = "http://192.168.43.205:4021/confirm";
                    JsonObjectRequest removeCarRequest = new JsonObjectRequest(
                            Request.Method.POST,
                            url,
                            params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {
                                    Log.d("Got response", "at confirming seat!");
                                    Toast.makeText(ListOnlineActivity.this,
                                            "Seat has been confirmed successfully",
                                            Toast.LENGTH_LONG).show();

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    Log.d("Error", "At confirming seat!");
                                    Toast.makeText(ListOnlineActivity.this,
                                            "Cant confirm seat. It was not reserved!",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                    );
                    requestQueue.add(removeCarRequest);
                    Log.v("Confirmin reserved seat", "post confirm");

                    this.seatsArray.remove(position);
                    listView.setAdapter(adapter);
                }
            }
        }
    }

    public void loadData() {

        //issuesArray.add(new Issue("name1","sprint1"));
        //carsArray.add(new Seat(1,"name12",2,"type1","status1"));
        //carsArray.add(new Seat(2,"name2",2,"type1","status1"));

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.determinateBar);
        progressBar.setMax(100);

        String url = "http://192.168.43.205:4021/all";
        JsonArrayRequest arrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (response.length() > 0) {
                            Log.v("List all", "There are values from server.");
                            double progress = 0;
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObj = response.getJSONObject(i);
                                    int id = Integer.parseInt(jsonObj.get("id").toString());
                                    String name = jsonObj.get("name").toString();
                                    String type = jsonObj.get("type").toString();
                                    String status = jsonObj.get("status").toString();
                                    seatsArray.add(new Seat(id, name, type, status));
                                    //adapter.add(new Seat(id,name,quantity,type,status));
                                    progress += 100.0 / response.length();
                                    progressBar.setProgress((int) progress);
                                    Log.v("ProgressValue", "" + progressBar.getProgress());
                                } catch (JSONException e) {
                                    Log.e("Volley", "Invalid JSON Object.");
                                }
                            }
                            listView.setAdapter(adapter);
                        } else {
                            Log.v("List all", "No values from server to client.");
                            progressBar.setProgress(100);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("ERROR", "Response.ErrorListener in getall");
                    }
                }
        );
        requestQueue.add(arrayRequest);
    }
        // Fire base below
        /*
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child("issues");

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Count ", "" + dataSnapshot.getChildrenCount());
                issuesArray.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Issue post = postSnapshot.getValue(Issue.class);
                    Log.d("post",post.toString());
                    issuesArray.add(post);
                    listView.setAdapter(adapter);
                    //Log.e("Get Data", post.<YourMethod> ());
                }

                /*
                Map<String, Object> td = (HashMap<String, Object>) dataSnapshot.getValue();
                //ArrayList<Object> td = (ArrayList<Object>) dataSnapshot.getValue();
                //Log.d("ArrayDataSet",td.toString());

                //Object[] issuesArr = new Object[td.size()];
                //issuesArr = td.toArray(issuesArr);

                Collection<Object> values = td.values();
                Log.d("UpdatedValues", values.toString());

                for (Object e: values){
                    Log.d("UpdatedValues", e.toString());
                    Log.d("Class",e.getClass().toString());
                    //issuesArray[0] = new Issue(e.get(sprint));
                }

                //notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
                Log.e("The read failed: " ,firebaseError.getMessage());
            }
        });*/
}
