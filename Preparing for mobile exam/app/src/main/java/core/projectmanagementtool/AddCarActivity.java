package core.projectmanagementtool;

import core.projectmanagementtool.model.Seat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;


public class AddCarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
    }

    public void addIssue(View button){
        final EditText nameField = (EditText) findViewById(R.id.editTextName);
        String name = nameField.getText().toString();

        int id;
        try {
            id = Integer.parseInt(nameField.getText().toString());
        }
        catch(Exception e){
            Log.e("Error at int conversion","id not int");
            Toast.makeText(AddCarActivity.this, "Id should be an integer",
                    Toast.LENGTH_LONG).show();
            return;
        }
        Seat seatToBeAdded = new Seat(id,name,"","");
        JSONObject params = seatToBeAdded.toJSONObject();

        Log.v("Reserving Seat","pre reserve with values " + id);
        String url = "http://192.168.43.205:4021/reserve";
        JsonObjectRequest addCarRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d("Got response","at reserving seat!");
                        Toast.makeText(AddCarActivity.this, "Seat reserved successfully",
                                Toast.LENGTH_LONG).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("Error","At reserving seat!");
                        Toast.makeText(AddCarActivity.this, "Seat not available.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(addCarRequest);
        Log.v("Reserving seat","post reserve");
        finish();

        //DatabaseReference mDatabase;
        //mDatabase = FirebaseDatabase.getInstance().getReference();

        //mDatabase.child("issues").child("3").child("name").setValue(name);
        //mDatabase.child("issues").child("3").child("sprint").setValue(sprint);

        //Issue issue = new Issue(name,sprint);





        //mDatabase.child("issues").push().setValue(issue);
        //mDatabase.child("issues").push().child("name").setValue(name);
        //mDatabase.child("issues").push().child("sprint").setValue(sprint);


        /*
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        String[] emails = {"tudor_orha@yahoo.com"};
        String emailTitle = "Collected data";
        String emailContent = "Name: " + name + "\nSprint: " + sprint;
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emails);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, emailTitle);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailContent);
        startActivity(Intent.createChooser(emailIntent, "Send Email"));*/
    }
}
