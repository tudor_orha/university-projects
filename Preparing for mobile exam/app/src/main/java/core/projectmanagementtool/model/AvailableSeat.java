package core.projectmanagementtool.model;

import java.io.Serializable;
import org.json.JSONObject;
import org.json.JSONException;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by tudor on 04-Nov-17.
 */

@Entity
public class AvailableSeat implements Serializable {
    @PrimaryKey
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "status")
    private String status;

    public AvailableSeat(){
        this.id=0;
        this.name = "NoArgsConst";
        this.type = "yup";
        this.status = "not available";
    }

    public AvailableSeat(int id, String name, String type, String status){
        this.id = id;
        this.name = name;
        this.type = type;
        this.status = status;
    }

    public int getId() {return this.id;}

    public String getName(){
        return this.name;
    }

    public String getType(){
        return this.type;
    }

    public String getStatus() { return this.status;}

    public void setId(int id) {this.id = id;}

    public void setName(String name){
        this.name = name;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setStatus(String status) { this.status = status;}

    /*
    public void setName(String name){
        this.name = name;
    }

    public void setSprint(String sprint){
        this.sprint = sprint;
    }
    */
    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",getId());
            jsonObject.put("name", getName());
            jsonObject.put("type", getType());
            jsonObject.put("status",getStatus());

            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public String toString(){
        return this.name + "\n" + this.type;
    }

}
