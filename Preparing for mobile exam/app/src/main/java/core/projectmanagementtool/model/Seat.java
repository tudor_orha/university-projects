package core.projectmanagementtool.model;

import java.io.Serializable;
import org.json.JSONObject;
import org.json.JSONException;

/**
 * Created by tudor on 04-Nov-17.
 */

public class Seat implements Serializable {
    private int id;
    private String name;
    private String type;
    private String status;

    public Seat(){
        this.id=0;
        this.name = "NoArgsConst";
        this.type = "yup";
        this.status = "not available";
    }

    public Seat(int id, String name, String type, String status){
        this.id = id;
        this.name = name;
        this.type = type;
        this.status = status;
    }

    public int getId() {return this.id;}

    public String getName(){
        return this.name;
    }

    public String getType(){
        return this.type;
    }

    public String getStatus() { return this.status;}

    /*
    public void setName(String name){
        this.name = name;
    }

    public void setSprint(String sprint){
        this.sprint = sprint;
    }
    */
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",getId());
            jsonObject.put("name", getName());
            jsonObject.put("type", getType());
            jsonObject.put("status",getStatus());

            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",getId());
            jsonObject.put("name", getName());
            jsonObject.put("type", getType());
            jsonObject.put("status",getStatus());

            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new JSONObject();
        }
    }

    @Override
    public String toString(){
        return this.name + "\n" + this.type + "\n" + this.status;
    }

}
