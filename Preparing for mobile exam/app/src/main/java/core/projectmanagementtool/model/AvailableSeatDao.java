package core.projectmanagementtool.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by tudor on 02-Feb-18.
 */

@Dao
public interface AvailableSeatDao {

    @Query("SELECT * FROM AvailableSeat")
    List<AvailableSeat> getAll();

    @Insert
    void insert(AvailableSeat car);

    @Update
    void updateCars(AvailableSeat... cars);

    @Delete
    void delete(AvailableSeat car);
}
