package core.projectmanagementtool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import core.projectmanagementtool.model.Issue;
import core.projectmanagementtool.model.Seat;

public class ListTakenSeatsActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    List<Seat> seatsArray;
    List<Issue> issuesArray;
    ArrayAdapter adapter;
    ListView listView;
    //Issue[] issuesArray = {new Issue("Issue1","Sprint1"),new Issue("Issue2","Sprint1")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_taken_seats);
        requestQueue = Volley.newRequestQueue(this);
        seatsArray = new ArrayList<>(5);
        issuesArray = new ArrayList<Issue>(5);
        adapter = new ArrayAdapter<Seat>(this,
                R.layout.activity_listview, seatsArray);
        listView = (ListView) findViewById(R.id.listOfIssues);
        this.loadData();

        ListView listView = (ListView) findViewById(R.id.listOfIssues);
    }

    public void goToAddPage(View view){
        Intent intent = new Intent(ListTakenSeatsActivity.this,AddCarActivity.class);
        startActivity(intent);
    }

    public void loadData() {

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.determinateBar);
        progressBar.setMax(100);

        Log.v("GET TAKEN", "Pre request");

        String url = "http://192.168.43.205:4021/taken";
        JsonArrayRequest arrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (response.length() > 0) {
                            Log.v("GET TAKEN", "There are values from server.");
                            double progress = 0;
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObj = response.getJSONObject(i);
                                    int id = Integer.parseInt(jsonObj.get("id").toString());
                                    String name = jsonObj.get("name").toString();
                                    String type = jsonObj.get("type").toString();
                                    String status = jsonObj.get("status").toString();
                                    seatsArray.add(new Seat(id, name, type, status));
                                    //adapter.add(new Seat(id,name,quantity,type,status));
                                    progress += 100.0 / response.length();
                                    progressBar.setProgress((int) progress);
                                    Log.v("ProgressValue", "" + progressBar.getProgress());
                                } catch (JSONException e) {
                                    Log.e("Volley", "Invalid JSON Object.");
                                }
                            }
                            listView.setAdapter(adapter);
                        } else {
                            Log.v("no values", "No values from server to client.");
                            progressBar.setProgress(100);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("ERROR", "Response.ErrorListener in getall");
                    }
                }
        );
        requestQueue.add(arrayRequest);
        Log.v("GET TAKEN", "Post request.");

    }
}
