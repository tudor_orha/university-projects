package core.projectmanagementtool;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.content.Context;
import android.widget.Toast;

import core.projectmanagementtool.controller.AppDatabase;
import core.projectmanagementtool.model.Issue;

public class MainActivity extends AppCompatActivity {

    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToOnlinePage(View view) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            Intent intent = new Intent(MainActivity.this, OnlineSectionActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Not connected to a network. Can't access online mode!",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void goToAdminPage(View view) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            Intent intent = new Intent(MainActivity.this, AdminSectionActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Not connected to a network. Can't access admin mode!",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void goToOfflinePage(View view){
        Intent intent = new Intent(MainActivity.this,OfflineSectionActivity.class);
        startActivity(intent);
    }
}
