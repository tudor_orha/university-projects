package core.projectmanagementtool;

import core.projectmanagementtool.controller.AppDatabase;
import core.projectmanagementtool.model.AvailableSeat;
import core.projectmanagementtool.model.Issue;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ListOfflineActivity extends AppCompatActivity {

    List<AvailableSeat> seatsArray;
    List<Issue> issuesArray;
    ArrayAdapter adapter;
    ListView listView;
    AppDatabase appDatabase;
    //Issue[] issuesArray = {new Issue("Issue1","Sprint1"),new Issue("Issue2","Sprint1")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_offline);
        issuesArray = new ArrayList<Issue>(5);

        //carsArray = new ArrayList<>(5);
        //Intent mServiceIntent = new Intent(ListOfflineActivity.this, Controller.class);
        //ListOfflineActivity.this.startService(mServiceIntent);
        //carsArray = ctrl.getAvailableCars();
        /*
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,"database-name")
                .allowMainThreadQueries()
                //.fallbackToDestructiveMigration()
                .build();*/
        //carsArray = appDatabase.availableSeatDao().getAll();
        seatsArray = new ArrayList<>(5);

        adapter = new ArrayAdapter<AvailableSeat>(this,
                R.layout.activity_listview, seatsArray);
        listView = (ListView) findViewById(R.id.listOfIssues);
        this.loadData();

        /*
        listView.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>adapter,View v, int position, long id){
                //Object item = adapter.getItemAtPosition(position);
                String jsonIssue = issuesArray.get(position).toJSON();
                Intent intent = new Intent(ListOfflineActivity.this,UpdateIssueActivity.class)
                        .putExtra("Issue", jsonIssue)
                        .putExtra("Position", position);
                startActivityForResult(intent,1);
            }
        });*/
    }

    public void refresh(View view){
        loadData();
    }


    public void loadData(){

        //issuesArray.add(new Issue("name1","sprint1"));
        //carsArray.add(new Seat(1,"name12",2,"type1","status1"));
        //carsArray.add(new Seat(2,"name2",2,"type1","status1"));

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.determinateBar);
        progressBar.setMax(100);

        String url = "http://192.168.43.205:4021/seats";
        JsonArrayRequest arrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response){
                        if(response.length() > 0){
                            Log.v("List Available seats","There are values from server.");
                            seatsArray.clear();
                            double progress = 0;
                            for(int i = 0; i < response.length(); i++){
                                try{
                                    JSONObject jsonObj = response.getJSONObject(i);
                                    int id = Integer.parseInt(jsonObj.get("id").toString());
                                    String name = jsonObj.get("name").toString();
                                    String type = jsonObj.get("type").toString();
                                    String status = jsonObj.get("status").toString();
                                    if(status.equals("available"))
                                        seatsArray.add(new AvailableSeat(id,name,type,status));
                                    progress += 100.0/response.length();
                                    progressBar.setProgress((int) progress);
                                    Log.v("ProgressValue","" + progressBar.getProgress());
                                }
                                catch(JSONException e){
                                    Log.e("Volley","Invalid JSON Object.");
                                }
                            }
                            listView.setAdapter(adapter);
                        }
                        else{
                            Log.v("List Available seats","No values from server to client.");
                            progressBar.setProgress(100);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("ERROR", "Response.ErrorListener in getall");
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            requestQueue.add(arrayRequest);
        }
        else {
            Toast.makeText(this, "Cant refresh data, not connected to a network!",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String stringIssue = data.getStringExtra("Issue");
                int position = data.getIntExtra("Position",0);
                try {
                    JSONObject jsonIssue = new JSONObject(stringIssue);
                    issuesArray.set(position, new Issue(jsonIssue.get("name").toString(),jsonIssue.get("sprint").toString()));
                }catch(org.json.JSONException e){}
                this.loadData();
            }
        }
    }
}
