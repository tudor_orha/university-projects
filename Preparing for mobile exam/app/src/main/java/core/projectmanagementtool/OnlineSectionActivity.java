package core.projectmanagementtool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class OnlineSectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_section);
    }

    public void markAllSeatsAvailable(View view){

        Log.v("All seats to available","PRE CLEAN");
        String url = "http://192.168.43.205:4021/clean";
        JsonObjectRequest arrayRequest = new JsonObjectRequest(
                Request.Method.DELETE,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("All seats to available", "All seats are available now.");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("ERROR", "Response.ErrorListener in converting seats to available.");
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(arrayRequest);
        Log.v("All seats to available","POST CLEAN");
    }

    public void goToListPage(View view){
        Intent intent = new Intent(OnlineSectionActivity.this,ListOnlineActivity.class);
        startActivity(intent);
    }
}
