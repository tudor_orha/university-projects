package core.projectmanagementtool.controller;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import core.projectmanagementtool.model.AvailableSeat;
import core.projectmanagementtool.model.AvailableSeatDao;

/**
 * Created by tudor on 02-Feb-18.
 */

@Database(entities = {AvailableSeat.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{
    public abstract AvailableSeatDao availableSeatDao();
}
