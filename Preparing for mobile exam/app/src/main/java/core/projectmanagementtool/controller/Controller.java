package core.projectmanagementtool.controller;

import android.app.IntentService;
import android.content.Intent;

import core.projectmanagementtool.model.AvailableSeat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tudor on 12/6/2017.
 */

public class Controller extends IntentService {
    List<AvailableSeat> carsList;

    public Controller(){
        super("");
        carsList = new ArrayList<AvailableSeat>(5);
    }

    public void add(AvailableSeat car){
        carsList.add(car);
    }
    public void remove(int poz) { carsList.remove(poz);}

    public List<AvailableSeat> getAvailableCars(){
        return carsList;
    }

    @Override
    protected void onHandleIntent(Intent workIntent){
    }

}
