package ro.ubb.bookstore.web.dto;

import lombok.*;

/**
 * Created by Laura on 4/30/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class BookDto extends BaseDto{
    private String title;
    private String author;
    private String genre;
    private Float price;

    @Override
    public String toString() {
        return "BookDto{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }
}
