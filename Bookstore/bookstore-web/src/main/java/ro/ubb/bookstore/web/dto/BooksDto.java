package ro.ubb.bookstore.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by Laura on 4/9/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class BooksDto {
    private Set<BookDto> books;
}
