package ro.ubb.bookstore.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.model.Book;
import ro.ubb.bookstore.web.dto.BookDto;

/**
 * Created by Laura on 4/30/2017.
 */
@Component
public class BookConverter extends BaseConverter<Book, BookDto>{
    private static final Logger log = LoggerFactory.getLogger(BookConverter.class);

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto bookDto = BookDto.builder()
                .title(book.getTitle())
                .author(book.getAuthor())
                .genre(book.getGenre())
                .price(book.getPrice())
                .build();
        bookDto.setId(book.getId());
        return bookDto;
    }
}
