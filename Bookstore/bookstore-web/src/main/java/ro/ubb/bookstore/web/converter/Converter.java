package ro.ubb.bookstore.web.converter;

import ro.ubb.bookstore.core.model.BaseEntity;
import ro.ubb.bookstore.web.dto.BaseDto;

/**
 * Created by Laura on 4/30/2017.
 */
public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto> extends ConverterGeneric<Model, Dto> {
}
