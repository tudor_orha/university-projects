package ro.ubb.bookstore.web.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Created by Laura on 4/30/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class BaseDto implements Serializable{
    private Long id;
}
