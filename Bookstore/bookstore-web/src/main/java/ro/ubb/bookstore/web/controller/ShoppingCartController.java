package ro.ubb.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.core.model.Client;
import ro.ubb.bookstore.core.model.ClientBook;
import ro.ubb.bookstore.core.service.ClientService;
import ro.ubb.bookstore.web.converter.ClientBookConverter;
import ro.ubb.bookstore.web.dto.ClientBookDto;
import ro.ubb.bookstore.web.dto.ClientBooksDto;

import java.util.Map;
import java.util.Set;

/**
 * Created by Laura on 5/6/2017.
 */
@RestController
public class ShoppingCartController {
    private static final Logger log = LoggerFactory.getLogger(ShoppingCartController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientBookConverter clientBookConverter;

    @RequestMapping(value = "/shopping-cart/{clientId}", method = RequestMethod.GET)
    public ClientBooksDto getClientBooks(
            @PathVariable final Long clientId) {
        log.trace("getClientBooks: clientId={}", clientId);
        Client client = clientService.findClient(clientId);
        Set<ClientBook> clientBooks = client.getClientBooks();
        Set<ClientBookDto> clientBookDtos = clientBookConverter
                .convertModelsToDtos(clientBooks);
        ClientBooksDto result = new ClientBooksDto(clientBookDtos);
        log.trace("getClientBooks: result={}", result);
        return result;
    }

    @RequestMapping(value = "/shopping-cart/{clientId}", method = RequestMethod.PUT)
    public ClientBooksDto updateClientBooks(
            @PathVariable final Long clientId,
            @RequestBody final ClientBooksDto clientBooksDto) {
        log.trace("updateClientBooks: clientId={}, clientBooksDto={}", clientId, clientBooksDto);
        Map<Long, Integer> items = clientBookConverter.convertDtoToMap(clientBooksDto);
        Client client = clientService.updateClientShoppingCart(clientId, items);
        Set<ClientBookDto> clientBookDtos = clientBookConverter.
                convertModelsToDtos(client.getClientBooks());
        ClientBooksDto result = new ClientBooksDto(clientBookDtos);
        log.trace("getClientBooks: result={}", result);
        return result;
    }
}
