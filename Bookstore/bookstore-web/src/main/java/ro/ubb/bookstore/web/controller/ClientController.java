package ro.ubb.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.core.model.Client;
import ro.ubb.bookstore.core.service.ClientService;
import ro.ubb.bookstore.web.dto.ClientDto;
import ro.ubb.bookstore.web.dto.ClientsDto;
import ro.ubb.bookstore.web.converter.ClientConverter;
import ro.ubb.bookstore.web.dto.EmptyJsonResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tudor on 4/13/2017.
 */
@RestController
public class ClientController {
    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientConverter clientConverter;

    /*
    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public Map<String, ClientDto> createClient(
            @RequestBody final Map<String, ClientDto> clientDtoMap) {
        log.trace("createClient: clientDtoMap={}", clientDtoMap);
        ClientDto clientDto = clientDtoMap.get("clients");
        Client client = clientService.createClient(
                clientDto.getFirstName(), clientDto.getLastName(), clientDto.getEmail(), clientDto.getAddress());
        Map<String, ClientDto> result = new HashMap<>();
        result.put("client", clientConverter.convertModelToDto(client));
        log.trace("createClient: result={}", result);
        return result;
    }*/

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public ResponseEntity createClient(@RequestBody final Map<String, ClientDto> clientDtoMap) {
        log.trace("createClient: clientDtoMap={}", clientDtoMap);

        ResponseEntity response;
        ClientDto clientDto = clientDtoMap.get("clients");
        try {
            clientService.createClient(
                    clientDto.getFirstName(), clientDto.getLastName(), clientDto.getEmail(), clientDto.getAddress());
            response = new ResponseEntity("success", HttpStatus.CREATED);
            log.trace("createClient: successful");
        } catch (RuntimeException e) {
            response = new ResponseEntity(e.getMessage(), HttpStatus.IM_USED);
            log.trace("createClient: failed");
        }
        return response;
    }

    @RequestMapping(value = "/clients/{clientId}", method = RequestMethod.PUT)
    public Map<String, ClientDto> updateClient(
            @PathVariable final Long clientId,
            @RequestBody final Map<String, ClientDto> clientDtoMap) {
        log.trace("updateClient: clientId={}, clientDtoMap={}",clientId, clientDtoMap);
        ClientDto clientDto = clientDtoMap.get("client");
        Client client = clientService.updateClient(
                clientId, clientDto.getFirstName(), clientDto.getLastName(), clientDto.getEmail(), clientDto.getAddress(), clientDto.getBooks());
        Map<String, ClientDto> result = new HashMap<>();
        result.put("client", clientConverter.convertModelToDto(client));
        log.trace("updateClient: result={}", result);
        return result;
    }

    @RequestMapping(value = "clients/{clientId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteClient(@PathVariable final Long clientId) {
        log.trace("deleteClient: clientId={}", clientId);
        clientService.deleteClient(clientId);
        log.trace("deleteClient - method end");
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public ClientsDto getClients() {
        log.trace("getClients");
        List<Client> clients = clientService.findAll();
        log.trace("getClients: clients={}", clients);
        return new ClientsDto(clientConverter.convertModelsToDtos(clients));
    }


}
