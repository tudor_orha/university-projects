package ro.ubb.bookstore.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by tudor on 5/1/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ClientDto extends BaseDto{
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private Set<Long> books;

    @Override
    public String toString() {
        return "ClientDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", books=" + books +
                "} " + super.toString();
    }
}