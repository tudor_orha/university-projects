package ro.ubb.bookstore.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.model.ClientBook;
import ro.ubb.bookstore.web.dto.ClientBookDto;
import ro.ubb.bookstore.web.dto.ClientBooksDto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Laura on 5/6/2017.
 */
@Component
public class ClientBookConverter extends BaseConverterGeneric<ClientBook, ClientBookDto>{
    @Override
    public ClientBook convertDtoToModel(ClientBookDto clientBookDto) {
        throw new RuntimeException("not yet implemented.");
    }

    @Override
    public ClientBookDto convertModelToDto(ClientBook clientBook) {
        ClientBookDto clientBookDto = ClientBookDto.builder()
                .clientId(clientBook.getClient().getId())
                .bookId(clientBook.getBook().getId())
                .bookTitle(clientBook.getBook().getTitle())
                .quantity(clientBook.getQuantity())
                .build();
        return clientBookDto;
    }

    public Map<Long, Integer> convertDtoToMap(ClientBooksDto clientBooksDto) {
        Map<Long, Integer> items = new HashMap<>();
        clientBooksDto.getClientBooks()
                .forEach(cb -> items.put(cb.getBookId(), cb.getQuantity()));
        return items;
    }
}
