package ro.ubb.bookstore.web.dto;

import lombok.*;

/**
 * Created by Laura on 5/6/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ClientBookDto {
    private Long clientId;
    private Long bookId;
    private String bookTitle;
    private Integer quantity;
}
