package ro.ubb.bookstore.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by tudor on 4/13/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ClientsDto {
    private Set<ClientDto> clients;
}
