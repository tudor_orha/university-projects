package ro.ubb.bookstore.web.converter;

/**
 * Created by Laura on 5/6/2017.
 */
public interface ConverterGeneric<Model, Dto> {
    Model convertDtoToModel(Dto dto);
    Dto convertModelToDto(Model model);
}
