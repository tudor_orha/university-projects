package ro.ubb.bookstore.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.model.Client;
import ro.ubb.bookstore.web.dto.ClientDto;

import java.util.stream.Collectors;

/**
 * Created by tudor on 5/1/2017.
 */
@Component
public class ClientConverter extends BaseConverter<Client, ClientDto>{
    private static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto clientDto = ClientDto.builder()
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .address(client.getAddress())
                .build();
        clientDto.setId(client.getId());
        clientDto.setBooks(client.getBooks().stream()
                .map(b -> b.getId())
                .collect(Collectors.toSet()));
        return clientDto;
    }
}