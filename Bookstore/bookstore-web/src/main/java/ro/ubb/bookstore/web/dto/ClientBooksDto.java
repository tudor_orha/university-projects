package ro.ubb.bookstore.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by Laura on 5/6/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ClientBooksDto {
    private Set<ClientBookDto> clientBooks;
}
