package ro.ubb.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.core.model.Book;
import ro.ubb.bookstore.core.service.BookService;
import ro.ubb.bookstore.web.converter.BookConverter;
import ro.ubb.bookstore.web.dto.BookDto;
import ro.ubb.bookstore.web.dto.BooksDto;
import ro.ubb.bookstore.web.dto.EmptyJsonResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Laura on 4/9/2017.
 */
@RestController
public class BookController {
    private static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private BookConverter bookConverter;

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public ResponseEntity createBook(
            @RequestBody final Map<String, BookDto> bookDtoMap) {
        log.trace("createBook: bookDtoMap={}", bookDtoMap);
        BookDto bookDto = bookDtoMap.get("book");
        ResponseEntity response;
        try{
            bookService.createBook(bookDto.getTitle(), bookDto.getAuthor(), bookDto.getGenre(), bookDto.getPrice());
            response = new ResponseEntity("success",HttpStatus.OK);
            log.trace("createBook success");
        }catch(RuntimeException e){
            response = new ResponseEntity(e.getMessage(),HttpStatus.PRECONDITION_FAILED);
            log.trace("createBook failure");
        }
        return response;
    }

    @RequestMapping(value = "/books/{bookId}", method = RequestMethod.PUT)
    public ResponseEntity updateBook(
            @PathVariable final Long bookId,
            @RequestBody final Map<String, BookDto> bookDtoMap) {
        log.trace("updateBook: bookId={}, bookDtoMap={}",bookId, bookDtoMap);
        BookDto bookDto = bookDtoMap.get("book");
        ResponseEntity response;
        try{
            bookService.updateBook(bookId, bookDto.getTitle(), bookDto.getAuthor(), bookDto.getGenre(), bookDto.getPrice());
            response = new ResponseEntity("success",HttpStatus.OK);
            log.trace("updateBook success");
        }catch(RuntimeException e){
            response = new ResponseEntity(e.getMessage(),HttpStatus.PRECONDITION_FAILED);
            log.trace("updateBook failure");
        }
        return response;
    }

    @RequestMapping(value = "books/{bookId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteBook(
            @PathVariable final Long bookId) {
        log.trace("deleteBook: bookId={}", bookId);
        bookService.deleteBook(bookId);
        log.trace("deleteBook - method end");
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public BooksDto getBooks() {
        log.trace("getBooks");
        List<Book> books = bookService.findAll();
        log.trace("getBooks: books={}", books);
        return new BooksDto(bookConverter.convertModelsToDtos(books));
    }
}
