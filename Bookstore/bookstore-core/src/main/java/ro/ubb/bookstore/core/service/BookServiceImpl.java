package ro.ubb.bookstore.core.service;

import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.ubb.bookstore.core.model.Book;
import ro.ubb.bookstore.core.repository.BookRepository;

import javax.validation.Validation;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

/**
 * Created by Laura on 4/9/2017.
 */
@Service
public class BookServiceImpl implements BookService {

    private static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    private final Validator validator = Validation
            .byProvider(HibernateValidator.class)
            .configure()
            .buildValidatorFactory()
            .getValidator();

    @Override
    @Transactional
    public Book createBook(String title, String author, String genre, Float price) throws RuntimeException {
        log.trace("createBook: title={}, author={}, genre={}, price={}", title, author, genre, price);
        validateBook(title, author, genre, price);
        Book book = Book.builder()
                .title(title)
                .author(author)
                .genre(genre)
                .price(price)
                .build();
        book = bookRepository.save(book);
        log.trace("createBook: book={}", book);
        return book;
    }

    @Override
    @Transactional
    public Book updateBook(Long id, String title, String author, String genre, Float price) throws RuntimeException{
        log.trace("updateBook: id={}, title={}, author={}, genre={}, price={}", id, title, author, genre, price);
        validateBook(title, author, genre, price);
        Book book = bookRepository.findOne(id);
        book.setTitle(title);
        book.setAuthor(author);
        book.setGenre(genre);
        book.setPrice(price);
        log.trace("updateBook: book={}", book);
        return book;
    }

    @Override
    @Transactional
    public void deleteBook(Long id) {
        log.trace("deleteBook: id={}", id);
        bookRepository.delete(id);
        log.trace("deleteBook - method end");
    }

    @Override
    public List<Book> findAll() {
        log.trace("findAll");
//        List<Book> books = bookRepository.findAll();
//        List<Book> books = bookRepository.findAllWithClientsGraph();
//        List<Book> books = bookRepository.findAllWithClientsSqlQuery();
//        List<Book> books = bookRepository.findAllWithClientsJpql();
        List<Book> books = bookRepository.findAllWithClientsJpaCriteria();
        log.trace("findAll: books={}", books);
        return books;
    }

    @Override
    public Book findBook(Long bookId) {
        log.trace("findBook: bookId={}", bookId);
//        Book book = bookRepository.findOne(bookId);
        Book book = bookRepository.findOneWithClients(bookId);
        log.trace("findBook: book={}", book);
        return book;
    }

    private void validateBook(String title, String author, String genre, Float price) throws RuntimeException{
        Book book = Book.builder()
                .title(title)
                .author(author)
                .genre(genre)
                .price(price)
                .build();
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);
        if(constraintViolations.size() > 0){
            constraintViolations.forEach(c -> log.error(c.getMessage()));
            String violations = constraintViolations.stream().map(c -> c.getMessage()).reduce("", (c1,c2) -> c1 + "\n" + c2);
            throw new RuntimeException(violations);
        }
    }


}
