package ro.ubb.bookstore.core.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Laura on 4/9/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> implements Serializable{
    @Id
    @TableGenerator(name = "TABLE_GENERATOR",initialValue = 0, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_GENERATOR")
    @Column(unique = true, nullable = false)
    private ID id;
}
