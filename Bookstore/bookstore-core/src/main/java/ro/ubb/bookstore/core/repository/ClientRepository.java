package ro.ubb.bookstore.core.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.ubb.bookstore.core.model.Client;

import java.util.List;

/**
 * Created by tudor on 4/13/2017.
 */
public interface ClientRepository extends BookstoreRepository<Client,Long>, ClientRepositoryCustom {
    @Query("select distinct c from Client c")
    @EntityGraph(value = "clientWithBooks", type = EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithBooksGraph();

    @Query("select distinct c from Client c where c.id=?1")
    @EntityGraph(value = "clientWithBooks", type = EntityGraph.EntityGraphType.LOAD)
    Client findOneWithBooks(Long clientId);
}
