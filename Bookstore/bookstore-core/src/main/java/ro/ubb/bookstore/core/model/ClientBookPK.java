package ro.ubb.bookstore.core.model;

import lombok.*;

import java.io.Serializable;

/**
 * Created by Laura on 5/4/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ClientBookPK implements Serializable{
    private Client client;
    private Book book;
}
