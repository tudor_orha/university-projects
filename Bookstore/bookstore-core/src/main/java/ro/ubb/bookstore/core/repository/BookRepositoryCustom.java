package ro.ubb.bookstore.core.repository;

import ro.ubb.bookstore.core.model.Book;

import java.util.List;

/**
 * Created by Laura on 4/9/2017.
 */
public interface BookRepositoryCustom {
    List<Book> findAllWithClientsSqlQuery();
    List<Book> findAllWithClientsJpql();
    List<Book> findAllWithClientsJpaCriteria();
}
