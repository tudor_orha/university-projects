package ro.ubb.bookstore.core.model;

import lombok.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tudor on 4/13/2017.
 */
@Entity
@Table(name = "client")
@NamedEntityGraphs({
        @NamedEntityGraph(name = "clientWithBooks", attributeNodes = {
                @NamedAttributeNode(value = "clientBooks", subgraph = "clientBooksGraph")
        }, subgraphs = {
                @NamedSubgraph(name = "clientBooksGraph", attributeNodes = {
                        @NamedAttributeNode(value = "client")
                })
        }
        )
})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Client extends BaseEntity<Long> {

    @NotBlank
    @Column(name = "firstname", nullable = false)
    private String firstName;

    @NotBlank
    @Column(name = "lastname", nullable = false)
    private String lastName;

    @NotBlank @Email
    @Column(name = "email", nullable = false , unique = true)
    private String email;

    @NotBlank
    @Column(name = "address", nullable = false)
    private String address;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private Set<ClientBook> clientBooks = new HashSet<>();

    public Set<Book> getBooks() {
        return Collections.unmodifiableSet(
                this.clientBooks.stream().
                        map(sd -> sd.getBook()).
                        collect(Collectors.toSet()));
    }

    public void addBook(Book book) {
        ClientBook clientBook = new ClientBook();
        clientBook.setBook(book);
        clientBook.setClient(this);
        clientBooks.add(clientBook);
    }

    public void addBooks(Set<Book> books) {
        books.forEach(book -> addBook(book));
    }

    public void addItem(Book book, Integer quantity) {
        ClientBook clientBook = new ClientBook();
        clientBook.setBook(book);
        clientBook.setQuantity(quantity);
        clientBook.setClient(this);
        clientBooks.add(clientBook);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return email.equals(client.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", clientBooks=" + clientBooks +
                "} " + super.toString();
    }
}
