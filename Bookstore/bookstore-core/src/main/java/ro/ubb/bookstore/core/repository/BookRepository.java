package ro.ubb.bookstore.core.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import ro.ubb.bookstore.core.model.Book;

import java.util.List;

/**
 * Created by Laura on 4/9/2017.
 */
public interface BookRepository extends BookstoreRepository<Book, Long>, BookRepositoryCustom {

    @Query("select distinct b from Book b")
    @EntityGraph(value = "bookWithClients", type = EntityGraph.EntityGraphType.LOAD)
    List<Book> findAllWithClientsGraph();

    @Query("select distinct b from Book b where b.id=?1")
    @EntityGraph(value = "bookWithClients", type = EntityGraph.EntityGraphType.LOAD)
    Book findOneWithClients(Long bookId);
}
