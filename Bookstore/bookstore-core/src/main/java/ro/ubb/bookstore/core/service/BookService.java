package ro.ubb.bookstore.core.service;

import ro.ubb.bookstore.core.model.Book;

import java.util.List;

/**
 * Created by Laura on 4/9/2017.
 */
public interface BookService {
    Book createBook(String title, String author, String genre, Float price) throws RuntimeException;
    Book updateBook(Long id, String title, String author, String genre, Float price) throws RuntimeException;
    void deleteBook(Long id);
    List<Book> findAll();
    Book findBook(Long bookId);
}
