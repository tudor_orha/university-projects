package ro.ubb.bookstore.core.repository;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.bookstore.core.model.Client;
import ro.ubb.bookstore.core.model.ClientBook;
import ro.ubb.bookstore.core.model.ClientBook_;
import ro.ubb.bookstore.core.model.Client_;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created by tudor on 24-May-17.
 */
public class ClientRepositoryImpl extends CustomRepositorySupport<Client, Long> implements ClientRepositoryCustom {

    private static final Logger log = LoggerFactory.getLogger(ClientRepositoryImpl.class);

    @Override
    @Transactional
    public List<Client> findAllWithBooksSqlQuery() {
        log.trace("findAllWithDisciplinesSqlQuery: method entered");

        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        Query query = session.createSQLQuery("select distinct {c.*}, {cb.*}, {b.*}" +
                " from client c" +
                " left join client_book cb on cb.client_id = c.id" +
                " left join book b on b.id = cb.book_id")
                .addEntity("c", Client.class)
                .addJoin("cb", "c.clientBooks")
                .addJoin("b", "cb.book")
                .addEntity("c", Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Client> clients = query.list();

        log.trace("findAllWithBooksSqlQuery: clients={}", clients);
        return clients;
    }

    @Override
    @Transactional
    public List<Client> findAllWithBooksJpql() {
        log.trace("findAllWithBooksJpql: method entered");

        javax.persistence.Query query = getEntityManager().createQuery("select distinct c from Client c" +
                " left join fetch c.clientBooks cb" +
                " left join fetch cb.book b");

        List<Client> clients = query.getResultList();

        log.trace("findAllWithBooksJpql: clients={}", clients);
        return clients;
    }



    @Override
    @Transactional
    public List<Client> findAllWithBooksJpaCriteria() {
        log.trace("findAllWithBooksJpaCriteria: method entered");

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);

        query.distinct(Boolean.TRUE);

        Root<Client> from = query.from(Client.class);

        Fetch<Client, ClientBook> clientBookFetch = from.fetch(Client_.clientBooks, JoinType.LEFT);
        clientBookFetch.fetch(ClientBook_.book, JoinType.LEFT);

        List<Client> clients = getEntityManager().createQuery(query).getResultList();

        log.trace("findAllWithBooksJpaCriteria: clients={}", clients);

        return clients;
    }
}
