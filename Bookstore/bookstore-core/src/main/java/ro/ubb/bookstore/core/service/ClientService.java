package ro.ubb.bookstore.core.service;

import ro.ubb.bookstore.core.model.Client;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by tudor on 4/13/2017.
 */
public interface ClientService {
    Client createClient(String firstName, String lastName, String email, String address) throws RuntimeException;
    Client updateClient(Long id, String firstName, String lastName, String email, String address, Set<Long> books);
    void deleteClient(Long id);
    List<Client> findAll();
    Client findClient(Long id);
    Client updateClientShoppingCart(Long clientId, Map<Long, Integer> items);
}
