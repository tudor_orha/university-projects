package ro.ubb.bookstore.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.bookstore.core.model.BaseEntity;

import java.io.Serializable;

/**
 * Created by Laura on 4/9/2017.
 */
@NoRepositoryBean
@Transactional
public interface BookstoreRepository<T extends BaseEntity<ID>, ID extends Serializable> extends JpaRepository<T,ID>{
}
