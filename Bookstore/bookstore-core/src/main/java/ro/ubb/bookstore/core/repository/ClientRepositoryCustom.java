package ro.ubb.bookstore.core.repository;

import ro.ubb.bookstore.core.model.Client;

import java.util.List;
/**
 * Created by tudor on 24-May-17.
 */
public interface ClientRepositoryCustom {
    List<Client> findAllWithBooksSqlQuery();
    List<Client> findAllWithBooksJpql();
    List<Client> findAllWithBooksJpaCriteria();
}
