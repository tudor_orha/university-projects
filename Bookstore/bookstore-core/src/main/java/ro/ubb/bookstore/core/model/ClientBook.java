package ro.ubb.bookstore.core.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Laura on 5/4/2017.
 */
@Entity
@Table(name = "client_book")
@IdClass(ClientBookPK.class)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ClientBook implements Serializable {
    @Id
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;

    @Id
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "quantity")
    private Integer quantity;
}