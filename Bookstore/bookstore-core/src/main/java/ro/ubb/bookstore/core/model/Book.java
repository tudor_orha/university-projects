package ro.ubb.bookstore.core.model;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Laura on 4/9/2017.
 */
@Entity
@Table(name = "book")
@NamedEntityGraphs({
        @NamedEntityGraph(name = "bookWithClients", attributeNodes = {
                @NamedAttributeNode(value = "clientBooks", subgraph = "bookClientsGraph")
        }, subgraphs = {
                @NamedSubgraph(name = "bookClientsGraph", attributeNodes = {
                        @NamedAttributeNode(value = "client")
                })
        }
        )
})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Book extends BaseEntity<Long>{
    @NotBlank
    @Column(name = "title", nullable = false)
    private String title;

    @NotBlank
    @Column(name = "author", nullable = false)
    private String author;

    @NotBlank
    @Column(name = "genre", nullable = false)
    private String genre;

    @NotNull
    @Min(0)
    @Column(name = "price", nullable = false)
    private Float price;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private Set<ClientBook> clientBooks = new HashSet<>();

    public Set<Client> getClients() {
        return Collections.unmodifiableSet(
                clientBooks.stream()
                        .map(sd -> sd.getClient())
                        .collect(Collectors.toSet())
        );
    }

    public void addClient(Client client) {
        ClientBook clientBook = new ClientBook();
        clientBook.setClient(client);
        clientBook.setBook(this);
        clientBooks.add(clientBook);
    }

    public void addQuantity(Client client, Integer quantity) {
        ClientBook clientBook = new ClientBook();
        clientBook.setClient(client);
        clientBook.setQuantity(quantity);
        clientBook.setBook(this);
        clientBooks.add(clientBook);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return title.equals(book.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", price=" + price +
                ", clientBooks=" + clientBooks +
                "} " + super.toString();
    }
}
