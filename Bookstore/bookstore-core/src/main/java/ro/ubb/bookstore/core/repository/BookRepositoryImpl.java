package ro.ubb.bookstore.core.repository;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.bookstore.core.model.Book;
import ro.ubb.bookstore.core.model.ClientBook;
import ro.ubb.bookstore.core.model.ClientBook_;
import ro.ubb.bookstore.core.model.Book_;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created by Laura on 5/23/2017.
 */
public class BookRepositoryImpl extends CustomRepositorySupport<Book, Long> implements BookRepositoryCustom {
    private static final Logger log = LoggerFactory.getLogger(BookRepositoryImpl.class);

    @Override
    @Transactional
    public List<Book> findAllWithClientsSqlQuery() {
        log.trace("findAllWithClientsSqlQuery: method entered");

        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        Query query = session.createSQLQuery("select distinct {b.*}, {cb.*}, {c.*}" +
                " from book b" +
                " left join client_book cb on cb.book_id = b.id" +
                " left join client c on c.id = cb.client_id")
                .addEntity("b", Book.class)
                .addJoin("cb", "b.clientBooks")
                .addJoin("c", "cb.client")
                .addEntity("b", Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Book> books = query.list();

        log.trace("findAllWithClientsSqlQuery: books={}", books);
        return books;
    }

    @Override
    @Transactional
    public List<Book> findAllWithClientsJpql() {
        log.trace("findAllWithClientsJpql: method entered");

        javax.persistence.Query query = getEntityManager().createQuery("select distinct b from Book b" +
                " left join fetch b.clientBooks cb" +
                " left join fetch cb.client c");

        List<Book> books = query.getResultList();

        log.trace("findAllWithClientsJpql: books={}", books);
        return books;
    }

    @Override
    @Transactional
    public List<Book> findAllWithClientsJpaCriteria() {
        log.trace("findAllWithCleintsJpaCriteria: method entered");

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);

        query.distinct(Boolean.TRUE);

        Root<Book> from = query.from(Book.class);

        Fetch<Book, ClientBook> clientBookFetch = from.fetch(Book_.clientBooks, JoinType.LEFT);
        clientBookFetch.fetch(ClientBook_.client, JoinType.LEFT);

        List<Book> books = getEntityManager().createQuery(query).getResultList();

        log.trace("findAllWithClientsJpaCriteria: books={}", books);

        return books;
    }
}
