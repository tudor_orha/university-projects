package ro.ubb.bookstore.core.service;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import ro.ubb.bookstore.core.model.Book;
import ro.ubb.bookstore.core.model.Client;
import ro.ubb.bookstore.core.repository.BookRepository;
import ro.ubb.bookstore.core.repository.ClientRepository;

import java.util.Set;

/**
 * Created by tudor on 4/13/2017.
 */
@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BookRepository bookRepository;

    private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private Validator validator = this.factory.getValidator();

    @Override
    @Transactional
    public Client createClient(String firstName, String lastName, String email, String address) throws RuntimeException {
        log.trace("createClient: firstName={}, lastName={}, email={}, address={}", firstName, lastName, email, address);
        Client client = Client.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .address(address)
                .build();

        Set<ConstraintViolation<Client>> constraintViolations = validator.validate(client);
        if(constraintViolations.size() > 0){
            constraintViolations.forEach(c -> log.error("Field in client error: " + c.getMessage()));
            String violations = constraintViolations.stream().map(c -> c.getMessage()).reduce("", (c1,c2) -> c1 + "\n" + c2);
            throw new RuntimeException(violations);
        }
        clientRepository.save(client);
        //clientRepository.saveOneWithBooks(client);
        //clientRepository.saveOneWithBooks(client.getId(),client.getAddress(),client.getEmail(),client.getFirstName(),client.getLastName());
        log.trace("createClient: client={}", client);
        return client;
    }

    @Override
    @Transactional
    public Client updateClient(Long id, String firstName, String lastName, String email, String address, Set<Long> books) {
        log.trace("updateClient: id={}, firstname={}, lastname={}, email={}, address={}, books={}",
                id, firstName, lastName, email, address, books);
        Client client = clientRepository.findOne(id);
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setEmail(email);
        client.setAddress(address);
        client.getBooks().stream()
                .map(b -> b.getId())
                .forEach(bId -> {
                    if (books.contains(bId)) {
                        books.remove(bId);
                    }
                });
        List<Book> bookList = bookRepository.findAll(books);
        bookList.forEach(b -> client.addBook(b));
        log.trace("updateClient: client={}", client);
        return client;
    }

    @Override
    @Transactional
    public void deleteClient(Long id) {
        log.trace("deleteClient: id={}", id);
        clientRepository.delete(id);
        log.trace("deleteClient - method end");
    }

    @Override
    public List<Client> findAll() {
        log.trace("findAll");
        //Used for debugging
        //List<Client> clients = new ArrayList<Client>();
        //The one from below is not working with lazy
        //List<Client> clients = clientRepository.findAll();
        List<Client> clients = clientRepository.findAllWithBooksGraph();
        //List<Client> clients = clientRepository.findAllWithBooksSqlQuery();
        //List<Client> clients = clientRepository.findAllWithBooksJpql();
        //List<Client> clients = clientRepository.findAllWithBooksJpaCriteria();
        log.trace("findAll: clients={}", clients);
        return clients;
    }

    @Override
    public Client findClient(Long id){
        log.trace("find client: id={}", id);
        //Client client = clientRepository.findOne(id);
        Client client = clientRepository.findOneWithBooks(id);
        log.trace("find client: client={}", client);
        return client;
    }

    @Override
    @Transactional
    public Client updateClientShoppingCart(Long clientId, Map<Long, Integer> items) {
        log.trace("updateClientShoppingCart: clientId={}, items={}", clientId, items);
        Client client = clientRepository.findOne(clientId);
        client.getClientBooks()
                .forEach(cb -> cb.setQuantity(items.get(cb.getBook().getId())));
        log.trace("updateClientShoppingCart: client={}", client);
        return client;
    }
}