<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="/labPhpAjax/lab8.css" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/labPhpAjax/lab8.js" type="text/javascript"></script>
<script src="/labPhpAjax/delete.php" type="text/php"></script>

</head>

<body>
<form class="search-bar" action="test.php?searching=true" method="POST">
	<label for="contain">Contains the word:</label>
	<input class="form-control" type="text" name="search" value=""/>
	<button type="submit" value="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
</form>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "multimedia_file_collection";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if(!isset($_POST['search'])) $_POST['search']="";	

$sql = "SELECT * FROM files WHERE (Title LIKE '%".$_POST['search']."%' OR
								   genre LIKE '%".$_POST['search']."%' OR
								   format_type LIKE '%".$_POST['search']."%' OR	
								   path LIKE '%".$_POST['search']."%')";
								   
								   
$result = $conn->query($sql);
if($result->num_rows > 0){
	echo '<table class="table"><tr><th>Title</th><th>Genre</th><th>Format</th><th>Path</th><th>Delete</th></tr>';
	while($row = $result->fetch_assoc()){
		echo "<tr><td>".$row["Title"]."</td><td>".$row["genre"]."</td><td>".$row["format_type"]."</td><td>".$row["path"].'</td><td><button onclick="deleteFile(';
		echo "'".$row["Title"]."'";
		echo ')">x</button></td></tr>';
	}
}
?>
<tr><td><input class="form-control" type="text" id="Title" value=""/>
<td><input class="form-control" type="text" id="genre" value=""/></td>
<td><input class="form-control" type="text" id="format_type" value=""/></td>
<td><input class="form-control" type="text" id="path" value=""/></td>
<td><button onclick="myAjax()">add</button></td></tr>
</table>
</body>
</html>





