<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="lab5.css"></link>
</head>
<body>
  <h1>The 4 classical elements</h1>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th style="text-align:center">Element</th>
      <th style="text-align:center">Image</th>
	  <th style="text-align:center">Link</th>
    </tr>
    <xsl:for-each select="entities/entity">
    <tr>
      <td class="text"><xsl:value-of select="text"/></td>
      <td> <xsl:variable name="imglink"><xsl:value-of select="img" /></xsl:variable>
		   <img src="{$imglink}"></img></td>
	  <td><xsl:variable name="hyperlink"><xsl:value-of select="link" /></xsl:variable>
		  <a href="{$hyperlink}"> <xsl:value-of select="link" /></a></td>
    </tr>
    </xsl:for-each>
  </table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>