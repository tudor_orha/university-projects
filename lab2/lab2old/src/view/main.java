package view;

import java.util.stream.*;
import model.*;
import model.statements.*;
import repository.MyRepository;
import controller.MyController;
import model.expressions.ConstExp;
import model.expressions.VarExp;
import model.expressions.*;

/**
 * Created by Tudor on 11/17/2016.
 */
public class main {
    public static void main(String[] args){
        MyRepository repo = new MyRepository();
        MyController ctrl = new MyController(repo);

        // v=2;Print(v)
        IStmt ex1= new CompStmt(new AssignStmt("v",new ConstExp(2)), new PrintStmt(new VarExp("x")));
        //IStmt ex2 = new CompStmt(new AssignStmt("v",new ConstExp(10)), new CompStmt( new newStmt("a",new ConstExp(22)),new CompStmt(
        //new forkStmt(new CompStmt(new wHStmt("a",new ConstExp(30)),new CompStmt(new AssignStmt("v",ConstExp(32)),new CompStmt(new PrintStmt(new VarExp("v")),new PrintStmt(new rHExp("a")))))), new CompStmt()
        //)));

        PrgState prg = new PrgState(new MyStack(), new MyDictionary(), new MyList(), new MyDictionary(), new MyDictionary<>(),
                new MyDictionary<>(), ex1);
        //MyIntDictionary<String, Integer> first = prg.getSymTable();
        //MyIntDictionary<String, Integer> second = prg.getSymTable();
        //second.put("3",5);
        //System.out.print(first);
        //System.out.print(second);

        ctrl.add(prg);
        ctrl.allStep();
    }
}
