package view;

import controller.MyController;
import javafx.application.Application;
import javafx.beans.property.SimpleListProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import model.MyDictionary;
import model.MyList;
import model.MyStack;
import model.PrgState;
import model.expressions.ConstExp;
import model.expressions.VarExp;
import model.statements.AssignStmt;
import model.statements.CompStmt;
import model.statements.IStmt;
import model.statements.PrintStmt;
import model.statements.*;
import model.expressions.*;
import repository.MyRepository;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.util.Pair;

import java.util.*;
import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.stream.*;

/**
 * Created by Tudor on 1/20/2017.
 */
public class GUI extends Application {

    public class TripleEntry{
        Integer index;
        Integer value;
        List<Integer> listOfValues;

        public TripleEntry(Integer ind,Integer val,List<Integer> list){
            this.index = ind;
            this.value = val;
            this.listOfValues = list;
        }

        public Integer getIndex(){return index;}

        public Integer getValue() {return value;}

        public List<Integer> getListOfValues() {return listOfValues;}
    }

    @Override
    public void start(Stage primaryStage) {
        MyRepository repo = new MyRepository();
        MyController ctrl = new MyController(repo);

        // v=2;Print(v)
        IStmt ex2= new CompStmt(new AssignStmt("v",new ConstExp(2)), new PrintStmt(new VarExp("x")));


        // v=10,new(a,22),fork(wH(a,30);v=32;print(v);print(rH(a)));print(v);
        IStmt ex3 = new CompStmt(new AssignStmt("v",new ConstExp(10)), new CompStmt( new newStmt("a",new ConstExp(22)),new CompStmt(
        new forkStmt(new CompStmt(new wHStmt("a",new ConstExp(30)),new CompStmt(new AssignStmt("v",new ConstExp(32)),new CompStmt(
                new PrintStmt(new VarExp("v")),new PrintStmt(new rHExp("a")))))),new PrintStmt(new VarExp("v")))));//, new CompStmt()


        IStmt ex4 = new CompStmt(new AssignStmt("v",new ConstExp(10)),new CompStmt(new WhileStmt(
                new ArithExp(new rHExp("v"),new ConstExp(4),2),new AssignStmt("v",new ArithExp(new rHExp("v"),new ConstExp(1),2))),
                new PrintStmt(new VarExp("v"))));

        IStmt ex5 = new CompStmt(new CompStmt(new CompStmt(new AssignStmt("v",new ConstExp(0)),
                new AssignStmt("v",new ConstExp(1))),new AssignStmt("v",new ConstExp(3))),new AssignStmt("v",new ConstExp(5)));

        IStmt ex1 = new CompStmt(new CompStmt(new CompStmt(new CompStmt(new CompStmt(new CompStmt(new AssignStmt("v",new ConstExp(0)),
                new repeat(new CompStmt(new PrintStmt(new VarExp("v")),
                        new AssignStmt("v",new ArithExp(new VarExp("v"),new ConstExp(1),1))),new equalExp(new VarExp("v"),new ConstExp(3))))
                ,new AssignStmt("x",new ConstExp(1))),new AssignStmt("y",new ConstExp(2))),new AssignStmt("z",new ConstExp(3))),new AssignStmt("w",new ConstExp(4))),new PrintStmt(new ArithExp(new VarExp("v"),new ConstExp(10),3)));

        IStmt ex6 = new CompStmt(new CompStmt(new CompStmt(new CompStmt(new CompStmt(new CompStmt(new AssignStmt("v",new ConstExp(0)),
                new repeat(new CompStmt(new forkStmt(new CompStmt(new PrintStmt(new VarExp("v")),new AssignStmt("v",new ArithExp(new VarExp("v"),new ConstExp(1),2)))),
                        new AssignStmt("v",new ArithExp(new VarExp("v"),new ConstExp(2),1))),new equalExp(new VarExp("v"),new ConstExp(3))))
                ,new AssignStmt("x",new ConstExp(1))),new AssignStmt("y",new ConstExp(2))),new AssignStmt("z",new ConstExp(3))),new AssignStmt("w",new ConstExp(4))),new PrintStmt(new ArithExp(new VarExp("v"),new ConstExp(10),3)));


        IStmt ex7 =
                new CompStmt(
                new CompStmt(
                new CompStmt(
                new CompStmt(
                new CompStmt(
                        new AssignStmt("a",new ConstExp(1)), new AssignStmt("b",new ConstExp(2))),
                        new IfAssignStmt("c",new VarExp("a"),new ConstExp(100),new ConstExp(200))),
                        new PrintStmt(new VarExp("c"))),
                        new IfAssignStmt("c",new ArithExp(new VarExp("b"),new ConstExp(2),2)
                                ,new ConstExp(100),new ConstExp(200))),
                        new PrintStmt(new VarExp("c"))
                );

        IStmt ex8 =
                new CompStmt(
                new CompStmt(
                new CompStmt(
                        new newStmt("v1",new ConstExp(1)),
                        new newSemaphore("cnt",new rHExp("v1"))),
                        new newSemaphore("cnt",new ConstExp(10))),
                        new acquire("lel")
                );

        IStmt ex9 =
                new CompStmt(
                        new CompStmt(
                        new CompStmt(
                        new CompStmt(
                                new CompStmt(
                                new CompStmt(
                                        new newStmt("v1",new ConstExp(1)),
                                        new newSemaphore("cnt",new rHExp("v1"))),
                                new forkStmt(
                                        new CompStmt(
                                        new CompStmt(
                                        new CompStmt(
                                                new acquire("cnt"),
                                                new wHStmt("v1",new ArithExp(new rHExp("v1"),new ConstExp(10),3))),
                                                new PrintStmt(new rHExp("v1"))),
                                                new release("cnt")
                                        ))),
                                new forkStmt(
                                        new CompStmt(
                                        new CompStmt(
                                        new CompStmt(
                                        new CompStmt(
                                                new acquire("cnt"),
                                                new wHStmt("v1",new ArithExp(new rHExp("v1"),new ConstExp(10),3))),
                                                new wHStmt("v1",new ArithExp(new rHExp("v1"),new ConstExp(2),3))),
                                                new PrintStmt(new rHExp("v1"))),
                                                new release("cnt")
                                        )
                                )),
                        new acquire("cnt")),
                        new PrintStmt(new ArithExp(new rHExp("v1"),new ConstExp(1),2))),
                        new release("cnt")
                );

        PrgState prg = new PrgState(new MyStack(), new MyDictionary(), new MyList(), new MyDictionary(), new MyDictionary(),
                new MyDictionary(), ex9);
        //MyIntDictionary<String, Integer> first = prg.getSymTable();
        //MyIntDictionary<String, Integer> second = prg.getSymTable();
        //second.put("3",5);
        //System.out.print(first);
        //System.out.print(second);
        ctrl.add(prg);
        //ctrl.allStep();

        ListView<String> ExeStack = new ListView<String>();
        ObservableList<String> StkItems = FXCollections.observableArrayList();
        //StkItems.add(prg.getStk().toString());
        prg.getStk().getStack().stream().forEach(s -> StkItems.add(s.toString()));
        //FXCollections.observableArrayList ("Single", "Double", "Suite", "Family App");
        ExeStack.setItems(StkItems);
        ExeStack.setPrefWidth(162);
        ExeStack.setPrefHeight(200);

        ListView<String> SymTbl = new ListView<String>();
        ObservableList<String> SymTblItems = FXCollections.observableArrayList();
        SymTblItems.add(prg.getSymTable().toString());
        //prg.getSymTable().getDict().stream().forEach(s -> StkItems.add(s.toString()));
        //FXCollections.observableArrayList ("Single", "Double", "Suite", "Family App");
        SymTbl.setItems(SymTblItems);
        SymTbl.setPrefWidth(100);
        SymTbl.setPrefHeight(200);

        ListView<String> Heapp = new ListView<String>();
        ObservableList<Entry<Integer,Integer>> HeapItems = FXCollections.observableArrayList();
        //HeapItems.add(prg.getHeap().toString());
        //FXCollections.observableArrayList ("Single", "Double", "Suite", "Family App");
        //Heapp.setItems(HeapItems);
        Heapp.setPrefWidth(100);
        Heapp.setPrefHeight(200);

        TableView<Entry<Integer,Integer>> Heap = new TableView<Entry<Integer,Integer>>();
        Heap.setEditable(true);
        TableColumn address = new TableColumn("Address");
        address.setCellValueFactory(new PropertyValueFactory<Entry,String>("key"));
        TableColumn value = new TableColumn("Value");
        value.setCellValueFactory(new PropertyValueFactory<Entry,String>("value"));
        HashMap<Integer,Integer> heap = prg.getHeap().getDict();
        for (Integer key : heap.keySet())
            HeapItems.add(new Entry(key,heap.get((key))));
        Heap.setItems(HeapItems);
        Heap.setPrefWidth(162);
        Heap.setPrefHeight(200);
        Heap.getColumns().addAll(address,value);


        ListView<String> Out = new ListView<String>();
        ObservableList<String> OutItems = FXCollections.observableArrayList();
        OutItems.add(prg.getOut().toString());
                //FXCollections.observableArrayList ("Single", "Double", "Suite", "Family App");
        Out.setItems(OutItems);
        Out.setPrefWidth(100);
        Out.setPrefHeight(200);

        TableView<Entry<String, Integer>> LatchTable = new TableView<>();
        LatchTable.setEditable(true);
        ObservableList<Entry<String ,Integer>> LatchItems = FXCollections.observableArrayList();
        TableColumn location = new TableColumn("Location");
        location.setCellValueFactory(new PropertyValueFactory<Entry,String>("key"));
        TableColumn valueLatch = new TableColumn("Value");
        valueLatch.setCellValueFactory(new PropertyValueFactory<Entry,String>("value"));
        HashMap<String,Integer> latch = prg.getLatch().getDict();
        LatchItems.add(new Entry("Location1",123));
        for (String key : latch.keySet())
            LatchItems.add(new Entry(key,heap.get((key))));
        LatchTable.setItems(LatchItems);
        LatchTable.setPrefWidth(162);
        LatchTable.setPrefHeight(200);
        LatchTable.getColumns().addAll(location,valueLatch);


        TableView<TripleEntry> SemaphoreTable = new TableView<>();
        SemaphoreTable.setEditable(true);
        ObservableList<TripleEntry> SemaphoreItems = FXCollections.observableArrayList();
        TableColumn semaphoreIndex = new TableColumn("Index");
        semaphoreIndex.setCellValueFactory(new PropertyValueFactory<TripleEntry,String>("index"));
        TableColumn semaphoreValue = new TableColumn("Value");
        semaphoreValue.setCellValueFactory(new PropertyValueFactory<TripleEntry,String>("value"));
        TableColumn semaphoreListOfValues = new TableColumn("List of values");
        semaphoreListOfValues.setCellValueFactory(new PropertyValueFactory<TripleEntry,String>("listOfValues"));
        HashMap<Integer,Pair<Integer,List<Integer>>> semaphore = prg.getSemaphore().getDict();
        List<Integer> example = Arrays.asList(1,2,3,4);
        SemaphoreItems.add(new TripleEntry(1,1,example));
        for (Integer key : semaphore.keySet())
            SemaphoreItems.add( new TripleEntry(key,semaphore.get(key).getKey(),semaphore.get(key).getValue()));
        SemaphoreTable.setItems(SemaphoreItems);
        SemaphoreTable.setPrefWidth(189);
        SemaphoreTable.setPrefHeight(200);
        SemaphoreTable.getColumns().addAll(semaphoreIndex, semaphoreValue, semaphoreListOfValues);



        Label stkLabel = new Label("ExeStack:");
        Label symLabel = new Label("SymTable:");
        Label heapLabel = new Label("Heap:");
        Label outLabel = new Label("Out:");
        Label latchLabel = new Label("LatchTable:");
        Label semaphoreLabel = new Label("Semaphore Table:");

        Button button = new Button("Accept");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                ctrl.allStepGUI();
                //repo.logPrgStateExec(prg);
                StkItems.clear();//StkItems.add(prg.getStk().toString());
                prg.getStk().getStack().stream().forEach(s -> StkItems.add(s.toString()));
                SymTblItems.clear();SymTblItems.add(prg.getSymTable().toString());

                HeapItems.clear();//HeapItems.add(prg.getHeap().toString());
                for (Integer key : heap.keySet()) HeapItems.add(new Entry(key,heap.get(key)));

                OutItems.clear();OutItems.add(prg.getOut().toString());

                SemaphoreItems.clear();
                for (Integer key: semaphore.keySet()) SemaphoreItems.add( new TripleEntry(key,semaphore.get(key).getKey(),semaphore.get(key).getValue()));
            }
        });

        Pane root = new Pane();
        Scene scene = new Scene(root, 555, 550);
        stkLabel.setLayoutX(25);
        stkLabel.setLayoutY(35);
        ExeStack.setLayoutX(25);
        ExeStack.setLayoutY(50);
        symLabel.setLayoutX(212);
        symLabel.setLayoutY(35);
        SymTbl.setLayoutX(212);
        SymTbl.setLayoutY(50);
        heapLabel.setLayoutX(337);
        heapLabel.setLayoutY(35);
        Heap.setLayoutX(337);
        Heap.setLayoutY(50);
        outLabel.setLayoutX(150);
        outLabel.setLayoutY(285);
        Out.setLayoutX(150);
        Out.setLayoutY(300);
        latchLabel.setLayoutX(337);
        latchLabel.setLayoutY(285);
        LatchTable.setLayoutX(337);
        LatchTable.setLayoutY(300);
        semaphoreLabel.setLayoutX(287);
        semaphoreLabel.setLayoutY(285);
        SemaphoreTable.setLayoutX(287);
        SemaphoreTable.setLayoutY(300);
        root.getChildren().addAll(button,stkLabel,symLabel,heapLabel,outLabel,semaphoreLabel,ExeStack,SymTbl,Heap,Out,SemaphoreTable);


        primaryStage.setTitle("JavaFxProg");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public class Entry<K,V>{
        K key;V value;
        public Entry(K k,V v){
            key = k;
            value = v;
        }
        public String getKey(){return key.toString();}
        public String getValue() {return value.toString();}
    }

    public static void main(String[] args) {
        launch(args);
    }
}