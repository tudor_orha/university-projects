package model;

import java.util.ArrayList;
/**
 * Created by Tudor on 11/17/2016.
 */
public class MyList<T> implements MyIntList<T> {
    ArrayList<T> l;

    public MyList() {
        this.l = new ArrayList();
    }

    public void add(T var){
        this.l.add(var);
    }
    public int size(){
        return this.l.size();
    }

    public T get(int index){
        return l.get(index);
    }
    public String toString(){ return l.toString();}
    public ArrayList<T> getList(){return l;}

}
