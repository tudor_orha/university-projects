package model;
import java.util.ArrayList;

/**
 * Created by Tudor on 11/17/2016.
 */
public interface MyIntList<T> {
    void add(T var);
    int size();
    T get(int index);
    String toString();
    ArrayList<T> getList();
}
