package model;

/**
 * Created by Tudor on 11/17/2016.
 */

import java.util.Stack;

public class MyStack<T> implements MyIntStack<T> {
    Stack<T> d = new Stack();

    public MyStack() {
    }

    public T pop(){
        return this.d.pop();
    }

    public void push(T elem){
        this.d.push(elem);
    }

    public Boolean isEmpty(){ return this.d.isEmpty();}

    public String toString(){
        return this.d.toString();
    }
    public Stack<T> getStack(){return d;};

}
