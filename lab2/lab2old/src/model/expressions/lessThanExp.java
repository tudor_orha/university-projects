package model.expressions;

import model.MyIntDictionary;
import model.expressions.Exp;

/**
 * Created by Tudor on 12/16/2016.
 */
public class lessThanExp extends Exp {
    Exp exp1;
    Exp exp2;

    public lessThanExp(Exp ex1,Exp ex2){
        this.exp1 = ex1;
        this.exp2 = ex2;
    }

    public int eval(MyIntDictionary<String, Integer> symTbl, MyIntDictionary<Integer, Integer> hp){
        if (this.exp1.eval(symTbl,hp) < this.exp2.eval(symTbl,hp)) return 1;
        else return 0;
    }

    public String toString(){
        return "lt";
    }
}
