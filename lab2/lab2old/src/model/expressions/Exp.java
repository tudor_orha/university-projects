package model.expressions;

import model.MyIntDictionary;

/**
 * Created by Tudor on 12/9/2016.
 */
public abstract class Exp {
    public Exp(){}
    public abstract int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp);
    public abstract String toString();
}
