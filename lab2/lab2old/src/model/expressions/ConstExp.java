package model.expressions;

import model.MyIntDictionary;

/**
 * Created by Tudor on 11/17/2016.
 */
public class ConstExp extends Exp {
    int number;

    public ConstExp(int nr) {
        this.number = nr;
    }

    public int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp) {
        return number;
    }

    public String toString() {
        return Integer.toString(number);
    }

}
