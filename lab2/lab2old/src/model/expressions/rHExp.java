package model.expressions;

import model.MyIntDictionary;
import model.expressions.Exp;

/**
 * Created by Tudor on 12/16/2016.
 */
public class rHExp extends Exp {
    String VarName;

    public rHExp(String var){
        this.VarName = var;
    }

    public int eval(MyIntDictionary<String, Integer> symTbl, MyIntDictionary<Integer, Integer> hp){
        int address = symTbl.get(VarName);
        try {
            return hp.get(address);
        }
        catch(Throwable t){
            return address;
        }
    }

    public String toString(){
        return "rHExp("+VarName+")";
    }
}
