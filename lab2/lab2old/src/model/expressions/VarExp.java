package model.expressions;

import model.MyIntDictionary;
import model.expressions.Exp;

/**
 * Created by Tudor on 11/17/2016.
 */
public class VarExp extends Exp {
    String id;

    public VarExp(String ID) {
        this.id = ID;
    }

    public int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp) {
        return ((Integer)tbl.get(this.id)).intValue();
    }

    public String toString(){
        return id;
    }
}
