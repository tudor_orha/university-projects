package model.statements;

import javafx.util.Pair;
import model.MyIntDictionary;
import model.MyIntStack;
import model.PrgState;
import model.expressions.Exp;

import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;

/**
 * Created by Tudor on 1/26/2018.
 */
public class acquire implements IStmt {
    String id;

    public acquire(String id) {
        this.id = id;
    }

    public String toString() {
        return "acquire(" + id + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        MyIntDictionary<Integer, Pair<Integer,List<Integer>>> semaphore = state.getSemaphore();
        if (symTbl.get(id) == null) {
            System.out.println("Error at acquire, variable is not in symTable");
            return null;
        }
        if (semaphore.get(symTbl.get(id)) == null){
            System.out.println("Error at acquire, variable is not in symTable");
            return null;
        }


        Pair<Integer,List<Integer>> pair = semaphore.get(symTbl.get(id));
        Integer N1 = pair.getKey();
        Integer NL = pair.getValue().size();
        if(N1 > NL){
            if(pair.getValue().contains(state.getID())){}
            else{
                pair.getValue().add(state.getID());
            }
        }
        else {
            stk.push(this);
        }

        return null;
    }


}
