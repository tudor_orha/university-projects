package model.statements;

import model.MyIntDictionary;
import model.expressions.*;
import model.PrgState;
import model.MyIntStack;
/**
 * Created by Tudor on 1/26/2017.
 */
public class WhileStmt implements IStmt {
    Exp e;
    IStmt stmt;

    public WhileStmt(Exp ex,IStmt st) {
        this.e = ex;
        this.stmt = st;
    }

    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        if(e.eval(symTbl,Heap) != 0) {
            stk.push(this);
            stk.push(stmt);
        }
        return null;
    }

    public String toString(){
        return "while(" + e.toString() + ") " + stmt.toString();
    }
}
/*
    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        int val = exp.eval(symTbl,Heap);
        //if (symTbl.isDefined(id)) symTbl.update(id, val);
        //else symTbl.add(id, val);
        symTbl.put(this.id, Integer.valueOf(val));
        return null;
    }

}
        MyIntStack<IStmt> stk = state.getStk();
        stk.push(snd);
        stk.push(first);
        return null;
        */
/*
4.Define and integrate the following While statement:
 while (expression) statement
The statement evaluation rule is as follows:
Stack1={while (exp1) Stmt1 | Stmt2|...}
 SymTable1
Out1
HeapTable1
FileTable1
 ==>
If exp1 is evaluated to 0 then Stack2={Stmt2|...}
Else Stack2={Stmt1 | while (exp1) Stmt1 | Stmt2|...}
SymTable2=SymTable1
Out2=Out1
HeapTable2=HeapTable1
FileTable2=FileTable1
Example: v=6; (while (v-4) print(v);v=v-1);print(v)
 */
