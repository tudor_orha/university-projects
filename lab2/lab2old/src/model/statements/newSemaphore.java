package model.statements;

import model.MyIntDictionary;
import model.PrgState;
import model.expressions.Exp;

import java.util.List;
import java.util.ArrayList;

import javafx.util.Pair;

/**
 * Created by Tudor on 1/26/2018.
 */
public class newSemaphore implements IStmt {
    String id;
    Exp exp;

    static int newFreeLocation = 10;

    public newSemaphore(String id, Exp exp) {
        this.id = id;
        this.exp = exp;
    }

    public String toString() {
        return "newSemaphore(" + id + ", " + exp.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        MyIntDictionary<Integer, Pair<Integer,List<Integer>>> semaphore = state.getSemaphore();
        int number = exp.eval(symTbl,Heap);
        semaphore.put(newFreeLocation,new Pair(number,new ArrayList<Integer>()));
        symTbl.put(id,newFreeLocation);
        newFreeLocation ++;
        return null;
    }

}
