package model.statements;

import model.MyIntDictionary;
import model.MyIntList;
import model.PrgState;
import java.util.HashMap;

/**
 * Created by Tudor on 1/26/2017.
 */
public class await implements IStmt {
    String id;

    public await(String var){
        id = var;
    }

    public PrgState execute(PrgState state){
        String foundIndex = lookup(state.getSymTable(),id);
        MyIntDictionary<String, Integer> latch = state.getLatch();
        if (foundIndex==null) {System.out.println("Var not in symtable!"); return null;}
        else{
            String inLatch = lookup(state.getSymTable(),id);
            if(inLatch==null) {System.out.println("Var is not in LatchTable!");return null;}
            else if (latch.getDict().get(id) == 0);
            else state.getStk().push(this);
        }
        return null;
    }

    public String lookup(MyIntDictionary<String,Integer> dic,String id){
        for (String key : dic.getDict().keySet())
            if(key == id) return id;
        return null;
    }
}
