package model.statements;

import model.expressions.Exp;
import model.MyIntDictionary;
import model.MyIntStack;
import model.PrgState;

/**
 * Created by Tudor on 11/17/2016.
 */
public class AssignStmt implements IStmt {
    String id;
    Exp exp;

    public AssignStmt(String ID, Exp e) {
        this.id = ID;
        this.exp = e;
    }

    public String toString() {
        return id + "=" + exp.toString();
    }

    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        int val = exp.eval(symTbl,Heap);
        //if (symTbl.isDefined(id)) symTbl.update(id, val);
        //else symTbl.add(id, val);
        symTbl.put(this.id, Integer.valueOf(val));
        return null;
    }

}

