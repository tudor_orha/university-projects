package model.statements;


import model.MyIntStack;
import model.PrgState;
import model.statements.IStmt;

/**
 * Created by Tudor on 11/17/2016.
 */
public class CompStmt implements IStmt {
    IStmt first;
    IStmt snd;

    public CompStmt(IStmt f, IStmt s) {
        this.first = f;
        this.snd = s;
    }

    public String toString() {
        return "(" + first.toString() + ";" + snd.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        stk.push(snd);
        stk.push(first);
        return null;
    }

}
