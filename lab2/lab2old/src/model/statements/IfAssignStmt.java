package model.statements;

import model.MyIntDictionary;
import model.MyIntStack;
import model.PrgState;
import model.expressions.Exp;

/**
 * Created by Tudor on 1/26/2018.
 */
public class IfAssignStmt implements IStmt {
    String id;
    Exp boolExp;
    Exp ifExp;
    Exp elseExp;

    public IfAssignStmt(String ID, Exp boolEx, Exp ifEx, Exp elseEx) {
        this.id = ID;
        this.boolExp = boolEx;
        this.ifExp = ifEx;
        this.elseExp = elseEx;
    }

    public String toString() {
        return id + "=" + boolExp.toString() + "?" + ifExp.toString() + ":" + elseExp.toString();

    }

    public PrgState execute(PrgState state) {

        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        IStmt assignStmt;

        if(boolExp.eval(symTbl,Heap) != 0)
            assignStmt = new AssignStmt(id,ifExp);
        else
            assignStmt = new AssignStmt(id,elseExp);
        stk.push(assignStmt);

        return null;
    }
}
