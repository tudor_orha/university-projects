package model.statements;
import javafx.util.Pair;
import model.PrgState;
import model.MyIntStack;
import model.MyIntDictionary;
import model.MyIntList;

import java.util.List;

/**
 * Created by Tudor on 12/20/2016.
 */
public class forkStmt implements IStmt {

    IStmt state;

    public forkStmt(IStmt st){
        this.state = st;
    }

    public PrgState execute(PrgState prg){
        MyIntStack stk = prg.getStk();
        MyIntDictionary<String, Integer> sym = prg.getSymTable();
        MyIntList out = prg.getOut();
        MyIntDictionary<Integer, Integer> heap = prg.getHeap();
        MyIntDictionary<Integer, Integer> latch = prg.getLatch();
        MyIntDictionary<String, Integer> NewSym = sym;
        MyIntDictionary<Integer, Pair<Integer, List<Integer>>> semaphore = prg.getSemaphore();
        PrgState newPrg = new PrgState(stk,NewSym,out,heap,latch,semaphore, this.state);
        newPrg.setID(prg.getID()*10);
        return newPrg;
    }

    public String toString(){
        return "fork(" + state.toString() + ")" ;
    }
}
