package model.statements;

import model.MyIntDictionary;
import model.MyIntStack;
import model.PrgState;
import model.expressions.*;
/**
 * Created by Tudor on 1/26/2017.
 */
public class repeat implements IStmt {
    IStmt stmt;
    Exp e;

    public repeat(IStmt st,Exp ex){
        stmt = st;
        e = ex;
    }

    public PrgState execute(PrgState state){
        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        //if(e.eval(symTbl,Heap) != 0) {
        //    stk.push(this);
        //    stk.push(stmt);
        //}
        if (e.eval(symTbl,Heap) == 0) {
            if (e instanceof equalExp) stk.push(new WhileStmt(new notEqualExp(((equalExp) e).exp1,((equalExp) e).exp2), stmt));

        }
        stk.push(stmt);
        return null;
    }

    public String toString(){
        return "repeat(" + stmt.toString() +") until(" + e.toString() +")";
    }
}
