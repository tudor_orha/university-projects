package model.statements;

import model.MyIntDictionary;
import model.MyIntList;
import model.expressions.Exp;
import model.PrgState;

/**
 * Created by Tudor on 11/17/2016.
 */
public class PrintStmt implements IStmt {
    Exp exp;

    public PrintStmt(Exp e) {
        this.exp = e;
    }

    public String toString() {
        return "print(" + exp.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntList out = state.getOut();
        MyIntDictionary symTbl = state.getSymTable();
        MyIntDictionary heap = state.getHeap();
        Integer val = exp.eval(symTbl,heap);
        out.add(val);
        //out.add(exp.eval(
        //out.add(Integer.valueOf(this.exp.eval(state.getSymTable(),state.getHeap())));
        return null;
    }
}
