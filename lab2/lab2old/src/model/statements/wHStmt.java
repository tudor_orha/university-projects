package model.statements;

import model.expressions.Exp;
import model.MyIntDictionary;
import model.PrgState;
/**
 * Created by Tudor on 12/16/2016.
 */
public class wHStmt implements IStmt {
    String VarName;
    Exp exp;

    public wHStmt(String var,Exp ex){
        this.VarName = var;
        this.exp = ex;
    }

    public PrgState execute(PrgState state){
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        int address = symTbl.get(VarName);
        int v = exp.eval(symTbl,Heap);
        if(symTbl.get(VarName) != null){
            Heap.remove(address);
            Heap.put(address,v);
        }
        //else throw "invalid address exception";
        return null;
    }

    public String toString(){ return VarName + "=" + exp.toString();}

}
