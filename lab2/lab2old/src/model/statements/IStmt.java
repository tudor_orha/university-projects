package model.statements;

import model.PrgState;

/**
 * Created by Tudor on 11/18/2016.
 */
public interface IStmt {
    String toString();
    PrgState execute(PrgState state);
}


