package model.statements;

import model.MyIntDictionary;
import model.PrgState;
import model.expressions.*;
/**
 * Created by Tudor on 1/26/2017.
 */
public class newLatch implements IStmt {
    String id;
    Exp exp;

    public newLatch(String var,Exp ex){
        id = var;
        exp = ex;
    }

    public PrgState execute(PrgState state){
        MyIntDictionary symTbl = state.getSymTable();
        MyIntDictionary heap = state.getHeap();
        exp.eval(symTbl,heap);

        return null;
    }
}
