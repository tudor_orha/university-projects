package model.statements;

import javafx.util.Pair;
import model.MyIntDictionary;
import model.PrgState;

import java.util.List;

/**
 * Created by Tudor on 1/26/2018.
 */
public class release implements IStmt {
    String id;

    public release(String id) {
        this.id = id;
    }

    public String toString() {
        return "release(" + id + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        MyIntDictionary<Integer, Pair<Integer,List<Integer>>> semaphore = state.getSemaphore();
        if (symTbl.get(id) == null) {
            System.out.println("Error at acquire, variable is not in symTable");
            return null;
        }
        if (semaphore.get(symTbl.get(id)) == null){
            System.out.println("Error at acquire, variable is not in symTable");
            return null;
        }
        Pair<Integer,List<Integer>> pair = semaphore.get(symTbl.get(id));

        if(pair.getValue().contains(state.getID())){
            pair.getValue().remove(new Integer(state.getID()));
        }


        //int number = exp.eval(symTbl,Heap);
        //semaphore.put(newFreeLocation,new Pair(number,new ArrayList<Integer>()));
        //symTbl.put(id,newFreeLocation);
        //newFreeLocation ++;
        return null;
    }


}
