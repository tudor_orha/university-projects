package model.statements;

import model.expressions.Exp;
import model.PrgState;

/**
 * Created by Tudor on 11/17/2016.
 */
public class IfStmt implements IStmt {
    Exp exp;
    IStmt thenS;
    IStmt elseS;


    public IfStmt(Exp e, IStmt t, IStmt el) {
        this.exp = e;
        this.thenS = t;
        this.elseS = el;
    }

    public String toString() {
        return "IF(" + exp.toString() + ") THEN(" + thenS.toString() + ") ELSE(" + elseS.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        return null;
    }
}
