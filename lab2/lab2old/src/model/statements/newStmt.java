package model.statements;

import model.PrgState;
import model.expressions.Exp;
import model.MyIntDictionary;
/**
 * Created by Tudor on 12/16/2016.
 */
public class newStmt implements IStmt {
    String id;
    Exp exp;


    public newStmt(String var_name, Exp expression) {
        id = var_name;
        exp = expression;
    }

    public String toString() {
        return "new(" + id + "," + exp.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        int val = exp.eval(symTbl,Heap);
        int freeLocation = state.getNewFreeLocation();
        //if var is in symTbl
        if (symTbl.get(this.id) != null) {
            symTbl.remove(this.id);
        }
        Heap.put(freeLocation, val);
        symTbl.put(this.id, freeLocation);
        state.setNewFreeLocation(freeLocation + 1);
        return null;
    }
}
