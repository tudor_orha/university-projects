package model;

/**
 * Created by Tudor on 11/17/2016.
 */

import java.util.HashMap;

public class MyDictionary<K,V> implements MyIntDictionary<K,V> {
    HashMap<K, V> d = new HashMap();

    public MyDictionary() {
    }



    public V get(K key) {
        return this.d.get(key);
    }

    public V put(K key, V val) {
        return this.d.put(key, val);
    }

    public void remove(K key) { this.d.remove(key);}

    public String toString() {
        return this.d.toString();
    }

    public HashMap<K,V> getDict() { return d;}
}
