package model;

import model.statements.IStmt;

import java.util.List;
import javafx.util.Pair;

/**
 * Created by Tudor on 11/18/2016.
 */
public class PrgState {
    MyIntStack<IStmt> exeStack;
    MyIntDictionary<String, Integer> symTable;
    MyIntList<Integer> out;
    MyIntDictionary<Integer, Integer> Heap;
    MyIntDictionary<Integer, Integer> LatchTable;
    MyIntDictionary<Integer, Pair<Integer,List<Integer>>> SemaphoreTable;
    IStmt originalProgram;
    int newFreeLocation;
    int ID;

    public PrgState(MyIntStack<IStmt> stk, MyIntDictionary<String, Integer> symTbl, MyIntList<Integer> ot,
                    MyIntDictionary<Integer,Integer> hp,MyIntDictionary<Integer, Integer> lt,
                    MyIntDictionary<Integer, Pair<Integer,List<Integer>>> semTbl, IStmt prg) {
        exeStack = stk;
        symTable = symTbl;
        out = ot;
        Heap = hp;
        LatchTable = lt;
        SemaphoreTable = semTbl;
        originalProgram = prg;
        newFreeLocation = 0;
        stk.push(prg);
        ID = 1;
    }

    public MyIntStack getStk() {
        return this.exeStack;
    }

    public MyIntDictionary getSymTable() {
        return this.symTable;
    }

    public MyIntList getOut() {
        return this.out;
    }

    public MyIntDictionary getHeap() { return this.Heap;}

    public MyIntDictionary getLatch() {return this.LatchTable;}

    public MyIntDictionary getSemaphore() { return this.SemaphoreTable;}

    public int getNewFreeLocation() { return newFreeLocation;}

    public void setNewFreeLocation(int new_val) { newFreeLocation = new_val;}

    public Boolean isNotCompleted(){
        return !(this.exeStack.isEmpty());
    }

    public PrgState oneStep(){
        if (exeStack.isEmpty()) ;//throws MyStmtExecException;
        IStmt crtStmt = exeStack.pop();
        return crtStmt.execute(this);
    }

    public int getID(){
        return this.ID;
    }

    public void setID(int newID) {
        this.ID = newID;
    }

    public String toString(){
        String s = new String();
        s = s + "You must finish PrgState toString.";
        return s;
    }
}
