package model;

import java.util.HashMap;

/**
 * Created by Tudor on 11/17/2016.
 */
public interface MyIntDictionary<K,V>{
    V get(K key);
    V put(K key, V val);
    void remove(K key);
    String toString();
    HashMap<K,V> getDict();
}
