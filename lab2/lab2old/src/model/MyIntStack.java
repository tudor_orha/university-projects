package model;

/**
 * Created by Tudor on 11/17/2016.
 */
import java.util.Stack;

public interface MyIntStack<T> {
    T pop();
    void push(T elem);
    Boolean isEmpty();
    Stack<T> getStack();
}
