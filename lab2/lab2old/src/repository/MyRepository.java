package repository;

import java.io.*;
import model.PrgState;
import model.MyIntDictionary;
import model.MyIntList;
import model.statements.IStmt;
import model.MyIntStack;
import java.util.List;
import java.util.ArrayList;
/**
 * Created by Tudor on 11/18/2016.
 */
public class MyRepository implements MyIntRepository {
    List<PrgState> d;
    String logFilePath;

    public MyRepository() {
        this.d = new ArrayList();
        //logFilePath = "C:/Users/Tudor/IdeaProjects/lab2/src/view/logFile.txt";
        //logFilePath = "C:/Users/Tudor/Desktop/lab2/src/view/logFile.txt";
        //logFilePath = "C:/Users/Tudor/Desktop/lab2/out/production/lab2gui/view/logFile.txt";
        logFilePath = "E:/Programare/university-projects/lab2/lab2old/logFile.txt";
    }

    public void add(PrgState prg) {
        this.d.add(prg);
    }

    public List<PrgState> getPrgList(){
        return this.d;
    }

    public void setPrgList(List<PrgState> lst){
        this.d = new ArrayList();
        for(int i=0;i<lst.size();i++)
            d.add(lst.get(i));
    }

    public void logPrgStateExec(PrgState prg){
        try {
            PrintWriter logFile = new PrintWriter(new BufferedWriter(new FileWriter(logFilePath, true)));
            MyIntStack<IStmt> stk = prg.getStk();
            MyIntDictionary<String, Integer> sym = prg.getSymTable();
            MyIntList<Integer> out = prg.getOut();
            logFile.println("ExeStack:\n");
            logFile.println(stk.toString());
            logFile.println("\nSymTable:\n");
            logFile.println(sym.toString());
            logFile.println("\nOut:\n");
            logFile.println(out.toString());
            logFile.println("\nFileTable:\n");
            logFile.println();
            logFile.println("----------------------------------------------------------");
            logFile.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }


    }


}
