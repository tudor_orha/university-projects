package repository;

import model.PrgState;
import java.util.List;
/**
 * Created by Tudor on 11/18/2016.
 */
public interface MyIntRepository {
    void add(PrgState prg);
    void logPrgStateExec(PrgState prg);
    List<PrgState> getPrgList();
    void setPrgList(List<PrgState> lst);
}
