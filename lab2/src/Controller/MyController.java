package Controller;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import Model.PrgState;
import Model.statements.IStmt;
import Model.utils.MyDictionary;
import Model.utils.MyIntDictionary;
import Model.utils.MyIntStack;
import Repository.MyIntRepository;

/**
 * Created by Tudor on 10/12/2017.
 */
public class MyController implements MyIntController {
    MyIntRepository repo;
    ExecutorService executor;

    public MyController(MyIntRepository Repo) {
        this.repo = Repo;
    }

    public void add(PrgState prg) {
        this.repo.add(prg);
    }


    PrgState oneStep(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        if (stk.isEmpty()) throw new ControllerException("Stack is empty, no statements left!");
        IStmt crtStmt = stk.pop();
        System.out.print(crtStmt.toString() + ";\n");
        return crtStmt.execute(state);
    }

    public void allStep() {
        PrgState prg = repo.getCrtPrg();
        while (!prg.getStk().isEmpty()) {
            oneStep(prg);
            garbageCollect();
            repo.logPrgStateExec(prg);
        }
    }

    private void garbageCollect(){
        PrgState prg = repo.getCrtPrg();
        MyIntDictionary<Integer,Integer> garbageCollected = prg.getHeap();
        for(Map.Entry<Integer,Integer> entry : garbageCollected.getDict().entrySet()){
            if ( prg.getSymTable().values().contains(entry.getKey()) == false ) garbageCollected.remove(entry.getKey());
        }
        prg.setHeap(garbageCollected);
    }

    public List<PrgState> removeCompletedProgram(List<PrgState> inPrgList) {
        return inPrgList.stream().filter(p -> p.isNotCompleted()).collect(Collectors.toList());
    }
    /*
    public void oneStepForAllPrg(List<PrgState> prgList) {
        prgList.forEach(prg -> repo.logPrgStateExec(prg));
        List<Callable<PrgState>> callList = prgList.stream()
                .map(p -> (Callable<PrgState>) () -> p.oneStep())
                .collect(Collectors.toList());
        try {
            List<PrgState> newPrgList = executor.invokeAll(callList).stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return null;
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                            return null;
                        }
                    })
                    .filter(p -> p != null)
                    .collect(Collectors.toList());

            prgList.addAll(newPrgList);
            prgList.forEach(prg -> repo.logPrgStateExec(prg));
            repo.setPrgList(prgList);
        } catch (InterruptedException e) {
        }

    }

    public void allStep() {
        executor = Executors.newFixedThreadPool(2);
        while (true) {
            //remove the completed programs
            List<PrgState> prgList = removeCompletedProgram(repo.getPrgList());
            if (prgList.size() == 0) break; //complete the execution of all threads
            oneStepForAllPrg(prgList);
        }
        executor.shutdownNow();
    }
    */
}
