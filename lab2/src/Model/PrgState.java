package Model;

import Model.statements.*;
import Model.utils.*;

import java.io.BufferedReader;

/**
 * Created by Tudor on 10/12/2017.
 */
public class PrgState {
    MyIntStack<IStmt> exeStack;
    MyIntDictionary<String, Integer> symTable;
    MyIntList<Integer> out;
    MyIntDictionary<Integer,Tuple<String, BufferedReader>> fileTable;
    MyIntDictionary<Integer, Integer> Heap;
    MyIntDictionary<Integer, Integer> LatchTable;
    IStmt originalProgram;
    int newFreeLocation;
    int ID;

    public PrgState(MyIntStack<IStmt> stk, MyIntDictionary<String, Integer> symTbl, MyIntList<Integer> ot,
                    MyIntDictionary<Integer,Tuple<String, BufferedReader>> fileTbl,
                    MyIntDictionary<Integer,Integer> hp,MyIntDictionary<Integer, Integer> lt,IStmt prg) {
        exeStack = stk;
        symTable = symTbl;
        out = ot;
        fileTable = fileTbl;
        Heap = hp;
        LatchTable = lt;
        originalProgram = prg;
        newFreeLocation = 1;
        stk.push(prg);
        ID = 1;
    }

    public MyIntStack getStk() {
        return this.exeStack;
    }

    public MyIntDictionary getSymTable() {
        return this.symTable;
    }

    public MyIntList getOut() {
        return this.out;
    }

    public MyIntDictionary getFileTable() { return this.fileTable;}

    public MyIntDictionary getHeap() { return this.Heap;}

    public void setHeap(MyIntDictionary dic){ this.Heap = dic;}

    public MyIntDictionary getLatch() {return this.LatchTable;}

    public int getNewFreeLocation() { return newFreeLocation;}

    public void setNewFreeLocation(int new_val) { newFreeLocation = new_val;}

    public Boolean isNotCompleted(){
        return !(this.exeStack.isEmpty());
    }

    public PrgState oneStep(){
        if (exeStack.isEmpty()) ;//throws MyStmtExecException;
        IStmt crtStmt = exeStack.pop();
        return crtStmt.execute(this);
    }

    public int getID(){
        return this.ID;
    }

    public void setID(int newID) {
        this.ID = newID;
    }

    public String toString(){
        String s = new String();
        s = s + "You must finish PrgState toString.";
        return s;
    }
}
