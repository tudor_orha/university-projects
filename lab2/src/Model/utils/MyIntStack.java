package Model.utils;

import java.util.Stack;

/**
 * Created by Tudor on 10/12/2017.
 */

public interface MyIntStack<T> {
    T pop();
    void push(T elem);
    Boolean isEmpty();
    Stack<T> getStack();
}
