package Model.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Tudor on 10/12/2017.
 */

public interface MyIntDictionary<K,V>{
    V get(K key);
    V put(K key, V val);
    Collection<V> values();
    void remove(K key);
    boolean containsKey(K key);
    String toString();
    HashMap<K,V> getDict();
    void setContent(HashMap<K,V> content);
    Set entrySet();
}
