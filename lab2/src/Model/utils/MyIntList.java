package Model.utils;

import java.util.ArrayList;

/**
 * Created by Tudor on 10/12/2017.
 */

public interface MyIntList<T> {
    void add(T var);
    int size();
    T get(int index);
    String toString();
    ArrayList<T> getList();
}
