package Model.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Tudor on 10/12/2017.
 */

public class MyDictionary<K,V> implements MyIntDictionary<K,V> {
    HashMap<K, V> d = new HashMap();

    public MyDictionary() {}

    public V get(K key) {
        return this.d.get(key);
    }

    public V put(K key, V val) {
        return this.d.put(key, val);
    }

    public Collection<V> values() { return this.d.values(); }

    public void remove(K key) { this.d.remove(key); }

    public boolean containsKey(K key) { return this.d.containsKey(key);}

    public String toString() {
        return this.d.toString();
    }

    public HashMap<K,V> getDict() { return d;}

    public void setContent(HashMap<K,V> content){ d = content;}

    public Set entrySet(){return d.entrySet();}
}
