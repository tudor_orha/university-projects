package Model.expressions;

import Model.utils.MyIntDictionary;

/**
 * Created by Tudor on 10/12/2017.
 */
public abstract class Exp {
    public Exp(){}
    public abstract int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp);
    public abstract String toString();
}
