package Model.expressions;

import Model.utils.MyIntDictionary;

/**
 * Created by Tudor on 10/12/2017.
 */
public class ArithExp extends Exp {
    Exp e1;
    Exp e2;
    int op;

    public ArithExp(int opt, Exp exp1, Exp exp2) {
        this.e1 = exp1;
        this.e2 = exp2;
        this.op = opt;
    }

    public int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp) {
        if (op == 1) return (e1.eval(tbl,hp) + e2.eval(tbl,hp));
        else if(op==2) return (e1.eval(tbl,hp) - e2.eval(tbl,hp));
        else if(op==3) return (e1.eval(tbl,hp) * e2.eval(tbl,hp));
        else if(op==4 && e2.eval(tbl,hp) != 0) return (e1.eval(tbl,hp) / e2.eval(tbl,hp));
        else if(op==4) throw new ExpressionException("Division by 0 in ArithExp!");
        else return -99999999;
    }

    public String toString(){
        if (op == 1) return e1.toString() + "+" + e2.toString();
        else if(op==2) return e1.toString() + "-"+ e2.toString();
        else if(op==3) return e1.toString() + "*"+ e2.toString();
        else if(op==4) return e1.toString() + "/"+ e2.toString();
        return "Error at ArithExp";
    }
}
