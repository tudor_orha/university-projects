package Model.expressions;

import Model.utils.MyIntDictionary;

/**
 * Created by Tudor on 10/12/2017.
 */
public class ConstExp extends Exp {
    int number;

    public ConstExp(int nr) {
        this.number = nr;
    }

    public int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp) {
        return number;
    }

    public String toString() {
        return Integer.toString(number);
    }

}