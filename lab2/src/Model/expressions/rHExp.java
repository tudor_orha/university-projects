package Model.expressions;

import Model.utils.MyIntDictionary;

/**
 * Created by Tudor on 11/23/2017.
 */
public class rHExp extends Exp {
    String VarName;

    public rHExp(String var){
        this.VarName = var;
    }

    public int eval(MyIntDictionary<String, Integer> symTbl, MyIntDictionary<Integer, Integer> hp){
        try {
            int address = symTbl.get(VarName);
        }
        catch( Throwable t) {
            throw new ExpressionException("Variable '" + VarName + "' can not be found in the symTable.");
        }
        try{
            return hp.get(symTbl.get(VarName));
        }catch (Throwable t){
            throw new ExpressionException("The heap location of '" + VarName + "' is not allocated.");
        }
    }

    public String toString(){
        return "rHExp("+VarName+")";
    }
}
