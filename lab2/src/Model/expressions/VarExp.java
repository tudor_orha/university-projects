package Model.expressions;

import Model.utils.MyIntDictionary;

/**
 * Created by Tudor on 10/12/2017.
 */
public class VarExp extends Exp {
    String id;

    public VarExp(String ID) {
        this.id = ID;
    }

    public int eval(MyIntDictionary<String, Integer> tbl, MyIntDictionary<Integer, Integer> hp) {
        if (tbl.containsKey(this.id) == false) throw new ExpressionException("Variable is not in the symTable.");
        return ((Integer) tbl.get(this.id)).intValue();

    }

    public String toString(){
        return id;
    }
}
