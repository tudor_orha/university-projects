package Model.statements;

import Model.PrgState;
import Model.expressions.Exp;
import Model.utils.MyIntDictionary;
import Model.utils.Tuple;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by Tudor on 10/24/2017.
 */
public class closeRFile implements IStmt {
    Exp exp_file_id;
    private static int id = 1;

    public closeRFile(Exp exp_file_id){
        this.exp_file_id = exp_file_id;
    }

    public PrgState execute(PrgState state) {
        MyIntDictionary<Integer,Tuple<String,BufferedReader>> fileTbl = state.getFileTable();
        MyIntDictionary<String,Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> hp = state.getHeap();
        int value = exp_file_id.eval(symTbl,hp);
        BufferedReader br = fileTbl.get(value).getY();
        try{
            br.close();
            fileTbl.remove(value);
        }catch (java.io.IOException e){}

        return state;
    }

    public String toString() {
        return "closeRFile(" + exp_file_id + ")";
    }
}
