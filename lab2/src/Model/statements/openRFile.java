package Model.statements;

import Model.PrgState;
import Model.utils.MyIntDictionary;
import Model.utils.MyIntStack;
import Model.utils.Tuple;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by Tudor on 10/24/2017.
 */
public class openRFile implements IStmt {
    String var_file_id;
    String filename;
    private static int id = 1;

    public openRFile(String var_file_id, String filename){
        this.var_file_id = var_file_id;
        this.filename = filename;
    }

    public PrgState execute(PrgState state) {
        MyIntDictionary<Integer,Tuple<String,BufferedReader>> fileTbl = state.getFileTable();
        MyIntDictionary<String,Integer> symTbl = state.getSymTable();
        for (Tuple<String,BufferedReader> value : fileTbl.values()) {
            if (value.getX() == filename)
                throw new RuntimeException("Error at openRFile(). FileName already in fileTable.");
        }
        try{
            FileReader in = new FileReader(filename);
            BufferedReader br = new BufferedReader(in);
            fileTbl.put(id,new Tuple<String,BufferedReader>(filename,br));
            symTbl.put(var_file_id,id);
            id++;
        }catch (java.io.FileNotFoundException e){ throw new RuntimeException("Error at openRFile(). File with given FileName not found!");}

        return state;
    }

    public String toString() {
        return "openRFile(" + var_file_id + ", " + filename + ")";
    }
}
