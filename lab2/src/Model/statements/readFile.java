package Model.statements;

import Model.PrgState;
import Model.utils.MyIntDictionary;
import Model.utils.Tuple;
import Model.expressions.Exp;

import java.beans.Expression;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Tudor on 10/24/2017.
 */
public class readFile implements IStmt {
    Exp exp_file_id;
    String var_name;

    public readFile(Exp exp_file_id, String var){
        this.exp_file_id = exp_file_id;
        this.var_name = var;
    }

    public PrgState execute(PrgState state) {
        MyIntDictionary<Integer,Tuple<String,BufferedReader>> fileTbl = state.getFileTable();
        MyIntDictionary<String,Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer,Integer> hp = state.getHeap();
        int value = exp_file_id.eval(symTbl,hp);
        BufferedReader br = fileTbl.get(value).getY();
        try {
            String line = br.readLine();
            int val;
            if (line == null) val = 0;
            else val = Integer.parseInt(line);
            symTbl.put(var_name,val);
        }catch(java.io.IOException e){}
        return state;
    }

    public String toString() {
        return "readFile(" + exp_file_id + ", " + var_name + ")";
    }
}
