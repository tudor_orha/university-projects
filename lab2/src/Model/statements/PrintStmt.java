package Model.statements;

import Model.expressions.Exp;
import Model.utils.*;
import Model.PrgState;

/**
 * Created by Tudor on 10/12/2017.
 */
public class PrintStmt implements IStmt {
    Exp exp;

    public PrintStmt(Exp e) {
        this.exp = e;
    }

    public String toString() {
        return "print(" + exp.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntList out = state.getOut();
        MyIntDictionary symTbl = state.getSymTable();
        MyIntDictionary heap = state.getHeap();
        Integer val = exp.eval(symTbl,heap);
        out.add(val);
        //out.add(exp.eval(
        //out.add(Integer.valueOf(this.exp.eval(state.getSymTable(),state.getHeap())));
        return state;
    }
}
