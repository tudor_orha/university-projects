package Model.statements;

import Model.expressions.Exp;
import Model.utils.MyIntDictionary;
import Model.PrgState;

/**
 * Created by Tudor on 11/23/2017.
 */
public class wHStmt implements IStmt {
    String VarName;
    Exp exp;

    public wHStmt(String var,Exp ex){
        this.VarName = var;
        this.exp = ex;
    }

    public PrgState execute(PrgState state){
        MyIntDictionary<String, Integer> symTbl = state.getSymTable();
        MyIntDictionary<Integer, Integer> Heap = state.getHeap();
        int address = symTbl.get(VarName);
        int v = exp.eval(symTbl,Heap);
        if(symTbl.get(VarName) != null){
            Heap.remove(address);
            Heap.put(address,v);
        }
        else throw new RuntimeException("invalid address exception at wHStmt, Variable not in SymTable");
        return null;
    }

    public String toString(){ return "wHStmt(" + VarName + "," + exp.toString() + ")";}

}