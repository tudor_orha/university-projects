package Model.statements;

import Model.PrgState;

/**
 * Created by Tudor on 10/12/2017.
 */
public interface IStmt {
    String toString();
    PrgState execute(PrgState state);
}
