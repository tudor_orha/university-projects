package Model.statements;

import Model.utils.MyIntStack;
import Model.PrgState;

/**
 * Created by Tudor on 10/12/2017.
 */
public class CompStmt implements IStmt {
    IStmt first;
    IStmt snd;

    public CompStmt(IStmt f, IStmt s) {
        this.first = f;
        this.snd = s;
    }

    public String toString() {
        return "(" + first.toString() + ";" + snd.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        stk.push(snd);
        stk.push(first);
        return state;
    }

}
