package Model.statements;

import Model.expressions.Exp;
import Model.PrgState;
import Model.utils.MyIntDictionary;
import Model.utils.MyIntStack;

import java.util.IntSummaryStatistics;

/**
 * Created by Tudor on 10/12/2017.
 */
public class IfStmt implements IStmt {
    Exp exp;
    IStmt thenS;
    IStmt elseS;


    public IfStmt(Exp e, IStmt t, IStmt el) {
        this.exp = e;
        this.thenS = t;
        this.elseS = el;
    }

    public String toString() {
        return "IF(" + exp.toString() + ") THEN(" + thenS.toString() + ") ELSE(" + elseS.toString() + ")";
    }

    public PrgState execute(PrgState state) {
        MyIntStack<IStmt> stk = state.getStk();
        MyIntDictionary<String, Integer> dic = state.getSymTable();
        MyIntDictionary<Integer, Integer> hp = state.getHeap();
        int result = exp.eval(dic,hp);
        if(result != 0)
            stk.push(thenS);
        else
            stk.push(elseS);
        return state;
    }
}
