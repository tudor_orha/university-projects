package Repository;

import java.util.List;
import Model.PrgState;

/**
 * Created by Tudor on 10/12/2017.
 */
public interface MyIntRepository {
    void add(PrgState prg);
    PrgState getCrtPrg();
    void logPrgStateExec(PrgState prg);
    List<PrgState> getPrgList();
    void setPrgList(List<PrgState> lst);
}
