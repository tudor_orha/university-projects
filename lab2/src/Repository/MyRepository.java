package Repository;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import Model.PrgState;
import Model.utils.*;
import Model.statements.IStmt;

/**
 * Created by Tudor on 10/12/2017.
 */
public class MyRepository implements MyIntRepository {
    List<PrgState> d;
    String logFilePath;

    public MyRepository() {
        this.d = new ArrayList();
        //logFilePath = "C:/Users/Tudor/IdeaProjects/lab2/src/view/logFile.txt";
        //logFilePath = "E:/programare/university-projects/lab2/src/View/logFile.txt";
        //logFilePath = "C:/Users/Tudor/Desktop/lab2/out/production/lab2gui/view/logFile.txt";
        logFilePath = "./src/View/logFile.txt";
    }

    public void add(PrgState prg) {
        this.d.add(prg);
    }

    public PrgState getCrtPrg() { return this.d.get(0); }

    public List<PrgState> getPrgList(){
        return this.d;
    }

    public void setPrgList(List<PrgState> lst){
        this.d = new ArrayList();
        for(int i=0;i<lst.size();i++)
            d.add(lst.get(i));
    }

    public void logPrgStateExec(PrgState prg){
        try {
            PrintWriter logFile = new PrintWriter(new BufferedWriter(new FileWriter(logFilePath, true)));
            logFile.println("ExeStack:\n");
            logFile.println(prg.getStk());
            logFile.println("\nSymTable:\n");
            logFile.println(prg.getSymTable());
            logFile.println("\nHeap:\n");
            logFile.println(prg.getHeap());
            logFile.println("\nOut:\n");
            logFile.println(prg.getOut());
            logFile.println("\nFileTable:\n");
            logFile.println(prg.getFileTable());
            logFile.println();
            logFile.println("----------------------------------------------------------");
            logFile.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }
}
