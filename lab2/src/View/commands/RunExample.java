package View.commands;

import Controller.MyController;

/**
 * Created by Tudor on 10/24/2017.
 */
public class RunExample extends Command {
    private MyController ctr;
    public RunExample(String key, String desc,MyController ctr){
        super(key, desc);
        this.ctr=ctr;
    }
    @Override
    public void execute() {
        //try{
            ctr.allStep();
        //catch ( E) {} //here you must treat the exceptions that can not be solved in the controller
    }
}
