package View.commands;

/**
 * Created by Tudor on 10/24/2017.
 */
public class ExitCommand extends Command {
    public ExitCommand(String key, String desc) {
        super(key, desc);
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
