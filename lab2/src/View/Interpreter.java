package View;

import Repository.MyRepository;
import Controller.MyController;
import Model.utils.*;
import Model.statements.*;
import Model.expressions.*;
import Model.PrgState;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by Tudor on 10/11/2017.
 */
public class Interpreter {
    public static void main(String[] args){
        MyRepository repo = new MyRepository();
        MyController ctrl = new MyController(repo);

        // v=2;Print(v)
        IStmt ex1= new CompStmt(new AssignStmt("v", new ConstExp(2)), new PrintStmt(new VarExp("v")));

        //a=2+3*5; (b=a+1;print(b));
        IStmt ex2 = new CompStmt(new AssignStmt("a", new ArithExp(1, new ConstExp(2),
                new ArithExp(4,new ConstExp(3), new ConstExp(0)))),
                new CompStmt(new AssignStmt("b",new ArithExp(1,new VarExp("a"), new ConstExp(1))),
                        new PrintStmt(new VarExp("b"))));

        IStmt ex3 = new CompStmt(new AssignStmt("a", new ArithExp(2,new ConstExp(2),
                new ConstExp(2))), new CompStmt(new IfStmt(new VarExp("a"),
                new AssignStmt("v",new ConstExp(2)), new AssignStmt("v", new ConstExp(3))),
                new PrintStmt(new VarExp("v"))));

        //Opening 2 files
        IStmt ex4 = new CompStmt(new openRFile("file1","./file1.txt"), new openRFile("file2","./file2.txt"));

        //v=10;new(v,20);new(a,22);wH(a,30);print(a);print(rH(a));a=0
        IStmt ex5 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
                new CompStmt( new newStmt("v",new ConstExp(20)),
                new CompStmt( new newStmt("a", new ConstExp(22)),
                new CompStmt( new wHStmt("a",new ConstExp(30)),
                        new CompStmt( new PrintStmt(new VarExp("a")),
                new CompStmt(new PrintStmt(new rHExp("a")),new AssignStmt("a",new ConstExp(0))))))));

        //ex5 = new PrintStmt(new rHExp("a"));
        //ex5 = new CompStmt(new AssignStmt("a",new ConstExp(50)),new PrintStmt(new rHExp("a")));

        //IStmt ex2 = new CompStmt(new AssignStmt("v",new ConstExp(10)), new CompStmt( new newStmt("a",new ConstExp(22)),new CompStmt(
        //new forkStmt(new CompStmt(new wHStmt("a",new ConstExp(30)),new CompStmt(new AssignStmt("v",ConstExp(32)),new CompStmt(new PrintStmt(new VarExp("v")),new PrintStmt(new rHExp("a")))))), new CompStmt()
        //)));
        PrgState prg1 = new PrgState(new MyStack(), new MyDictionary(), new MyList(), new MyDictionary<>(), new MyDictionary(),
                new MyDictionary<>(), ex5);
        PrgState prg2 = new PrgState(new MyStack(), new MyDictionary(), new MyList(), new MyDictionary<>(), new MyDictionary(),
                new MyDictionary<>(), ex2);
        PrgState prg3 = new PrgState(new MyStack(), new MyDictionary(), new MyList(), new MyDictionary<>(), new MyDictionary(),
                new MyDictionary<>(), ex3);

        //MyIntDictionary<String, Integer> first = prg.getSymTable();
        //MyIntDictionary<String, Integer> second = prg.getSymTable();
        //second.put("3",5);
        //System.out.print(first);
        //System.out.print(second);
        ctrl.add(prg1);
        ctrl.allStep();
    }
}
